@extends('employee.partials.main')

@section('container')
    <style>
        button.btn.btn-outline-success.search{
            margin-top: 10%;
            margin-left: 10%;
            border: none;
            background-color: #4107BB;
            color: white;  
        }
    </style>

    <div style="background-color: #F7F9FF; width: 100%;">
        <div class="container" style="display: flex;">
            <p style="font-weight: 400; color:#4107BB; padding-top: 15px;">Select</p>
            <select class="form-select" aria-label="Default select example" style="width: 160px; height: 35px; border: none; position: relative; margin-top: 1%; margin-left: 2%;">
                <option selected>Month</option>
                <option value="1">January</option>
                <option value="2">February</option>
                <option value="3">March</option>
            </select>
            <select class="form-select" aria-label="Default select example" style="width: 160px; height: 35px; border: none; position: relative; margin-top: 1%; margin-left: 1%;">
                <option selected>Year</option>
                <option value="1">2021</option>
                <option value="2">2022</option>
                <option value="3">2023</option>
            </select>
            <form>
                <button class="btn btn-outline-success search" type="submit">Search</button>
            </form>
            <p style="font-weight: 600; color:#4107BB; padding-top: 15px; margin-left:44%;">December 2021</p>
        </div>
    </div>

    <br>

    <div class="container">
        <div class="accordion w-100" id="basicAccordion">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse"
                        data-mdb-target="#basicAccordionCollapseOne" aria-expanded="false"
                        aria-controls="collapseOne">
                        Question #1
                    </button>
                </h2>
                <div id="basicAccordionCollapseOne" class="accordion-collapse collapse"
                aria-labelledby="headingOne" data-mdb-parent="#basicAccordion" style="">
                    <div class="accordion-body">
                        <strong>This is the first item's accordion body.</strong> It is shown by default,
                        until the collapse plugin adds the appropriate classes that we use to style each
                        element. These classes control the overall appearance, as well as the showing and
                        hiding via CSS transitions. You can modify any of this with custom CSS or overriding
                        our default variables. It's also worth noting that just about any HTML can go within
                        the <code>.accordion-body</code>, though the transition does limit overflow.
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection