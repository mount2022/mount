<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EmployeeTaskController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\NewTaskController;
use App\Http\Controllers\AddNewTaskController;
use App\Http\Controllers\ApprovalTaskController;
use App\Http\Controllers\DashboardAdminController;
use App\Http\Controllers\DatesController;
use App\Http\Controllers\FilterController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProfileSettingsController;
use App\Http\Controllers\VueAPIController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\MoodController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*====================HOME====================*/
Route::get('/', function () {
     return view('Home.home');
});
/*====================AUTH====================*/
Route::get('/login', [LoginController::class, "index"])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, "authenticate"]);
Route::post('/logout', [LoginController::class, "logout"]);

/*=================DAHSBOARD=================*/
Route::get('/dashboard', [DashboardController::class, "index"])->middleware('auth');
Route::get('/dashboard/filter', [FilterController::class, "index"])->middleware('auth');

/*====================TASK====================*/
Route::resource('/dashboard/task', EmployeeTaskController::class)->middleware('auth');
Route::post('/dashboard/task/dependentDropdown', [EmployeeTaskController::class, 'dependentDropdown'])->name('dependentDropdown.store');
Route::get('/dashboard/task/off/{id}', [EmployeeTaskController::class, 'showDayOff'])->middleware('auth');
Route::get('/dashboard/task/delete', [EmployeeTaskController::class, 'destroy'])->middleware('auth');
Route::post('/dashboard/task/dismiss', [EmployeeTaskController::class, 'dismiss'])->middleware('auth');

/*====================Profile====================*/
Route::get('/dashboard/profile', [ProfileController::class, "index"])->middleware('auth');
Route::get('/dashboard/profile/change-password', [ProfileController::class, "edit"])->name('user.password.edit')->middleware('auth');
Route::patch('/dashboard/profile/change-password', [ProfileController::class, "update"])->name('user.password.update')->middleware('auth');
Route::get('/dashboard/profile/notification', [ProfileController::class, "notification"])->middleware('auth');
Route::post('/dashboard/profile/clear-notification', [ProfileController::class, "clearNotification"])->middleware('auth');
Route::get('/dashboard/profile/kpi', [ProfileController::class, "KeyPerformanceIndicator"])->middleware('auth');
Route::get('/dashboard/profile/jobdesk', [ProfileController::class, "JobDescription"])->middleware('auth');
Route::get('/dashboard/profile/training', [ProfileController::class, "TrainingPlan"])->middleware('auth');

Route::get('/dashboard-admin/profile/{id}', [ProfileController::class, "ProfileSA"])->middleware('auth');
Route::get('/dashboard-admin/profile/change-password/{id}', [ProfileController::class, "ChangePasswordSA"])->name('user.password.edit')->middleware('auth');
Route::get('/dashboard-admin/profile/kpi/{id}', [ProfileController::class, "KeyPerformanceIndicatorSA"])->middleware('auth');
Route::get('/dashboard-admin/profile/jobdesk/{id}', [ProfileController::class, "JobDescriptionSA"])->middleware('auth');
Route::get('/dashboard-admin/profile/training/{id}', [ProfileController::class, "TrainingPlanSA"])->middleware('auth');
Route::post('/dashboard-admin/profile/reset', [ProfileController::class, "ResetPassword"])->middleware('is_admin');

Route::post('/dashboard-admin/profile/training/action', [ProfileController::class, "ActionTraining"])->name('training.action')->middleware('is_admin');
Route::post('/dashboard-admin/profile/training/title', [ProfileController::class, "TitleTraining"])->middleware('is_admin');

Route::post('/dashboard-admin/profile/kpi/action', [ProfileController::class, "ActionKPI"])->name('kpi.action')->middleware('is_admin');
Route::post('/dashboard-admin/profile/kpi/title', [ProfileController::class, "TitleKPI"])->middleware('is_admin');

Route::post('/dashboard-admin/profile/jobdesc/action', [ProfileController::class, "ActionJobdesc"])->middleware('is_admin');

Route::post('crop',[ProfileController::class, 'crop'])->name('crop');

/*=================DAHSBOARD=================*/
Route::get('/dashboard-admin', [DashboardAdminController::class, "index"])->middleware('is_admin');
Route::get('/dashboard-managefield', [DashboardAdminController::class, "managefield"])->middleware('is_admin');
Route::get('/dashboard-managelist', [DashboardAdminController::class, "managelist"])->middleware('is_admin');
Route::get('/dashboard-averageHours', [DashboardAdminController::class, "averageHours"])->middleware('is_admin');
Route::post('/dashboard-averageHours/export', [DashboardAdminController::class, "export"])->middleware('is_admin');
Route::post('/dashboard-managelist/create', [DashboardAdminController::class, 'create'])->middleware('is_admin');
Route::post('/dashboard-managelist/delete', [DashboardAdminController::class, 'delete'])->middleware('is_admin');

/*=================Approval=================*/
Route::get('/dashboard-admin/approval', [ApprovalTaskController::class, 'index'])->middleware('is_admin')->name('dashboard.admin');
Route::get('/dashboard/task-detail', [ApprovalTaskController::class, 'detail'])->middleware('is_admin');
Route::post('/dashboard-admin/approval/approve', [ApprovalTaskController::class, 'approve'])->middleware('is_admin');
Route::post('/dashboard-admin/approval/reject', [ApprovalTaskController::class, 'reject'])->middleware('is_admin');
Route::post('/dashboard-admin/approval/undo', [ApprovalTaskController::class, 'undo'])->middleware('is_admin');
Route::post('/dashboard-admin/export', [ApprovalTaskController::class, 'export'])->middleware('is_admin');
Route::post('/dashboard-admin/unlock', [ApprovalTaskController::class, 'unlock'])->middleware('is_admin');
Route::get('/dashboard/task/admin/{id}', [ApprovalTaskController::class, 'show'])->middleware('is_admin');
Route::get('/dashboard/task/admin/off/{id}', [ApprovalTaskController::class, 'showDayOff'])->middleware('is_admin');
Route::post('/dashboard-admin/approval-task/approve', [ApprovalTaskController::class, 'approveSingle'])->middleware('is_admin');
Route::post('/dashboard-admin/approval-task/reject', [ApprovalTaskController::class, 'rejectSingle'])->middleware('is_admin');
Route::post('/dashboard-admin/approval-task/undo', [ApprovalTaskController::class, 'undoSingle'])->middleware('is_admin');

/*=================Selected Approval=================*/
Route::post('/dashboard-admin/approval/approve-selected', [ApprovalTaskController::class, 'approveSelected'])->middleware('is_admin')->name('approval.selected');
Route::post('/dashboard-admin/approval/reject-selected', [ApprovalTaskController::class, 'rejectSelected'])->middleware('is_admin')->name('reject.selected');
Route::post('/dashboard-admin/export-selected', [ApprovalTaskController::class, 'exportSelected'])->middleware('is_admin')->name('export.selected');

/*=================Vue API=================*/
// Route::get('/user', [VueAPIController::class, 'index'])->middleware('is_admin');
Route::get('/notification', [VueAPIController::class, 'notification']);
Route::get('/markAsRead', [VueAPIController::class, 'markAsRead']);

/*=================Mood=================*/
Route::post('/dashboard/mood', [MoodController::class, 'index'])->middleware('auth')->name('mood.create');

/*=================Employee=================*/
Route::resource('/dashboard-admin/employee', EmployeeController::class)->middleware('is_admin');
Route::post('/dashboard-admin/employee/delete', [EmployeeController::class, 'destroy'])->middleware('is_admin');
Route::post('/dashboard-admin/employee/update/{id}', [EmployeeController::class, 'update'])->middleware('is_admin');
Route::post('/dashboard-admin/employee/import', [EmployeeController::class, 'import'])->middleware('is_admin');