<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\DailyTask;
use App\Models\TypeOfWork;
use App\Models\Site;
use App\Models\Transportation;
use App\Models\Mood;
use App\Models\Task;
use App\Models\User;
use App\Models\Absence;
use App\Models\Department;
use App\Exports\AverageExport;
use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class DashboardAdminController extends Controller
{
    public function index(){
        // Mood
        $Month = date('m');
        $Year = date('Y');
        $Date = date('d');
        $startDate = Carbon::createFromDate($Year, $Month, $Date);

        if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
            $CW = Mood::whereDate('created_date', $startDate)->where('mood', "Couldn't be worse")->count();
            $SC = Mood::whereDate('created_date', $startDate)->where('mood', 'Sick')->count();
            $UW = Mood::whereDate('created_date', $startDate)->where('mood', 'Unwell')->count();
            $MH = Mood::whereDate('created_date', $startDate)->where('mood', 'Meh')->count();
            $SO = Mood::whereDate('created_date', $startDate)->where('mood', 'So-so')->count();
            $OK = Mood::whereDate('created_date', $startDate)->where('mood', 'Okay')->count();
            $GD = Mood::whereDate('created_date', $startDate)->where('mood', 'Good')->count();
            $VG = Mood::whereDate('created_date', $startDate)->where('mood', 'Very good')->count();
            $GR = Mood::whereDate('created_date', $startDate)->where('mood', 'Great')->count();
            $RG = Mood::whereDate('created_date', $startDate)->where('mood', 'Really great')->count();

            $CWdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Couldn't be worse");
            })->get();
            foreach($CWdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $SCdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Sick");
            })->get();
            foreach($SCdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $UWdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Unwell");
            })->get();
            foreach($UWdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $MHdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Meh");
            })->get();
            foreach($MHdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $SOdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "So-so");
            })->get();
            foreach($SOdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $OKdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Okay");
            })->get();
            foreach($OKdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $GDdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Good");
            })->get();
            foreach($GDdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $VGdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Very good");
            })->get();
            foreach($VGdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $GRdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Great");
            })->get();
            foreach($GRdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $RGdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Really great");
            })->get();
            foreach($RGdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }
        } elseif(auth()->user()->role_id == 2) {
            $CW = Mood::whereDate('created_date', $startDate)->where('mood', "Couldn't be worse")->where('department_id', auth()->user()->department_id)->count();
            $SC = Mood::whereDate('created_date', $startDate)->where('mood', 'Sick')->where('department_id', auth()->user()->department_id)->count();
            $UW = Mood::whereDate('created_date', $startDate)->where('mood', 'Unwell')->where('department_id', auth()->user()->department_id)->count();
            $MH = Mood::whereDate('created_date', $startDate)->where('mood', 'Meh')->where('department_id', auth()->user()->department_id)->count();
            $SO = Mood::whereDate('created_date', $startDate)->where('mood', 'So-so')->where('department_id', auth()->user()->department_id)->count();
            $OK = Mood::whereDate('created_date', $startDate)->where('mood', 'Okay')->where('department_id', auth()->user()->department_id)->count();
            $GD = Mood::whereDate('created_date', $startDate)->where('mood', 'Good')->where('department_id', auth()->user()->department_id)->count();
            $VG = Mood::whereDate('created_date', $startDate)->where('mood', 'Very good')->where('department_id', auth()->user()->department_id)->count();
            $GR = Mood::whereDate('created_date', $startDate)->where('mood', 'Great')->where('department_id', auth()->user()->department_id)->count();
            $RG = Mood::whereDate('created_date', $startDate)->where('mood', 'Really great')->where('department_id', auth()->user()->department_id)->count();

            $CWdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Couldn't be worse")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($CWdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $SCdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Sick")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($SCdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $UWdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Unwell")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($UWdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $MHdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Meh")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($MHdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $SOdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "So-so")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($SOdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $OKdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Okay")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($OKdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $GDdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Good")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($GDdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $VGdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Very good")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($VGdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $GRdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Great")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($GRdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }

            $RGdata = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('moods')->whereDate('created_date', $startDate)->where('mood', "Really great")->where('department_id', auth()->user()->department_id);
            })->get();
            foreach($RGdata as $data) {
                $data->department_id = Department::where('id', $data->department_id)->value('name');
            }
        }
        
        $this->fragment['CW'] = $CW;
        $this->fragment['SC'] = $SC;
        $this->fragment['UW'] = $UW;
        $this->fragment['MH'] = $MH;
        $this->fragment['SO'] = $SO;
        $this->fragment['OK'] = $OK;
        $this->fragment['GD'] = $GD;
        $this->fragment['VG'] = $VG;
        $this->fragment['GR'] = $GR;
        $this->fragment['RG'] = $RG;

        $this->fragment['CWdata'] = $CWdata;
        $this->fragment['SCdata'] = $SCdata;
        $this->fragment['UWdata'] = $UWdata;
        $this->fragment['MHdata'] = $MHdata;
        $this->fragment['SOdata'] = $SOdata;
        $this->fragment['OKdata'] = $OKdata;
        $this->fragment['GDdata'] = $GDdata;
        $this->fragment['VGdata'] = $VGdata;
        $this->fragment['GRdata'] = $GRdata;
        $this->fragment['RGdata'] = $RGdata;

        // Tasks completed & still in progress
        if (auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
            $PS = Task::distinct('user_id')->whereDate('created_at', $startDate)->count();
        }

        $TP = 0;
        $TC = 0;
        $PeopleSubmitted = Task::whereDate('created_at', $startDate)->get();
        foreach($PeopleSubmitted as $task) {
            $task->department_id = User::where('id', $task->user_id)->value('department_id');
        }

        foreach($PeopleSubmitted as $task) {
            if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
                if ($task->task_status == 'In Progress') {
                    $TP++;
                } else if ($task->task_status == 'Completed') {
                    $TC++;
                }
            } elseif(auth()->user()->role_id == 2) {
                if ($task->department_id == auth()->user()->department_id) {
                    if ($task->task_status == 'In Progress') {
                        $TP++;
                    } else if ($task->task_status == 'Completed') {
                        $TC++;
                    }
                }
            }
        }

        $this->fragment['TP'] = $TP; // Tasks in Progress
        $this->fragment['TC'] = $TC; // Tasks Completed

        // All Tasks
        $Task = Task::whereDate('created_at', $startDate)->latest();

        if($Task != null) {
            $Task = $Task->get();

            foreach($Task as $task) {
                $task->department_id = User::where('id', $task->user_id)->value('department_id');
                $task->department = Department::where('id', $task->department_id)->value('name');
                $task->user_name = User::where('id', $task->user_id)->value('name');
                $task->picture = User::where('id', $task->user_id)->value('picture');
    
                $totalMinutes = 0;
    
                if ($task->absence_id == null) {
                    $start = new \Carbon\Carbon($task->start_time);
                    $end   = new \Carbon\Carbon($task->finish_time);
                    $minutesToday  = $start->diffInMinutes($end);
                    $totalMinutes += $minutesToday;
    
                    $now = \Carbon\Carbon::now();
                    $then = $now->copy()->addMinutes($totalMinutes);
                    $hours = $now->diffInHours($then);
                    $totalMinutes = $now->diffInMinutes($then) - ($hours * 60);
    
                    $task->hour = $hours;
                    $task->minute = $totalMinutes;
                }
    
                $task->start_time = strtotime($task->start_time);
                $task->start_time = date("H:i", $task->start_time);
                $task->task_date = Carbon::createFromFormat('Y-m-d H:i:s', $task->created_at)->format('l, d F Y');
            }
        }

        $this->fragment['Task'] = $Task; // Today Submitted Tasks

        // People not submitted
        if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
            $HF = User::whereNotIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('tasks')->whereDate('created_at', $startDate);
            })->count();
    
            $notSubmitted = User::whereNotIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('tasks')->whereDate('created_at', $startDate);
            })->get();
        } elseif(auth()->user()->role_id == 2) {
            $HF = User::whereNotIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('tasks')->whereDate('created_at', $startDate);
            })->where('department_id', auth()->user()->department_id)->count();
    
            $notSubmitted = User::whereNotIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('tasks')->whereDate('created_at', $startDate);
            })->where('department_id', auth()->user()->department_id)->get();
        }
        
        foreach($notSubmitted as $s) {
            $s->department_id = Department::where('id', $s->department_id)->value('name');
        }

        $this->fragment['HF'] = $HF;
        $this->fragment['notSubmitted'] = $notSubmitted;

        // People submitted
        if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
            $submitted = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('tasks')->whereDate('created_at', $startDate);
            })->get();
        } elseif(auth()->user()->role_id == 2) {
            $submitted = User::whereIn('id', function($query)use($startDate) {
                $query->select('user_id')->from('tasks')->whereDate('created_at', $startDate);
            })->where('department_id', auth()->user()->department_id)->get();

            $PS = 0;
        }
        
        foreach($submitted as $s) {
            $s->department_id = Department::where('id', $s->department_id)->value('name');
            if(auth()->user()->role_id == 2) {
                $PS++; 
            }
        }

        $this->fragment['submitted'] = $submitted;
        $this->fragment['PS'] = $PS; // People Submitted
        
        /*** View ***/
        return view("admin-superadmin.dashboard-admin", $this->fragment);
    }

    public function managefield() {
        // $tables = DB::select('SHOW TABLES');
        // foreach($tables as $table) {
        //     echo $table->Tables_in_mount . " | ";
        // }

        $tables= array('Project Name', 'Task Name', 'Type Of Work', 'Site Name',  'Kind Of Transportation', 'Day Off');

        $this->fragment['tables'] = $tables;

        /*** View ***/
        return view("admin-superadmin.managefield-superadmin", $this->fragment);
    }

    public function managelist() {
        $name = request()->get('name');
        $data = null;
        $Departments = Department::all();
        $Projects = Project::all();

        if($name == 'ProjectName') {
            $data = Project::all();
            $name = 'Project Name';
        } else if($name == 'TaskName') {
            if(request()->get('project') != null && request()->get('department') != null) {
                $data = DailyTask::where('project_id', request()->get('project'))->where('department_id', request()->get('department'))->get();
                $name = 'Task Name';
            } else {
                $data = null;
                $name = 'Task Name';
            }
        } else if($name == 'TypeOfWork') {
            $data = TypeOfWork::all();
            $name = 'Type Of Work';
        } else if($name == 'SiteName') {
            $data = Site::all();
            $name = 'Site Name';
        } else if($name == 'KindOfTransportation') {
            $data = Transportation::all();
            $name = 'Kind Of Transportation';
        } else if($name == 'DayOff') {
            $data = Absence::all();
            $name = 'Day Off';
        }

        $this->fragment['datas'] = $data;
        $this->fragment['name'] = $name;
        $this->fragment['Departments'] = $Departments;
        $this->fragment['Projects'] = $Projects;

        /*** View ***/
        return view("admin-superadmin.managelist", $this->fragment);
    }

    public function averageHours() {
        $departmentRequest = request()->get('department');
        $isSearch = false;

        $Department = Department::all();
        if (auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
            if($departmentRequest != null) {
                $User = User::where('department_id', $departmentRequest)->get();
                $userCountGet = User::where('department_id', $departmentRequest)->get();
                $COUNT_TEST = User::where('department_id', $departmentRequest)->count();
                $isSearch = true;
            } else {
                $User = User::where('department_id', auth()->user()->department_id)->get();
                $userCountGet = User::where('department_id', auth()->user()->department_id)->get();
                $COUNT_TEST = User::where('department_id', auth()->user()->department_id)->count();
            }
        } elseif (auth()->user()->role_id == 2) {
            $User = User::where('department_id', auth()->user()->department_id)->get();
            $userCountGet = User::where('department_id', auth()->user()->department_id)->get();
            $COUNT_TEST = User::where('department_id', auth()->user()->department_id)->count();
        }
        

        $month = request()->get('month');
        $year = request()->get('year');

        foreach($User as $user) {
            $user->department_id = Department::where('id', $user->department_id)->value('name');

            if($month != null && $year != null) {
                $user->task = Task::where('user_id', $user->id)->whereMonth('created_at', $month)->whereYear('created_at', $year)->whereNull('absence_id')->get();
                $workingDays = Task::distinct('created_at')->whereMonth('created_at', $month)->whereYear('created_at', $year)->where('user_id', $user->id)->whereNull('absence_id')->count();
            } else {
                $user->task = Task::where('user_id', $user->id)->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->whereNull('absence_id')->get();
                $workingDays = Task::distinct('created_at')->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->where('user_id', $user->id)->whereNull('absence_id')->count();
            }

            $totalMinutes = 0;

            foreach($user->task as $task) {
                if ($task->absence_id == null) {
                    $start = new \Carbon\Carbon($task->start_time);
                    $end   = new \Carbon\Carbon($task->finish_time);
                    $minute  = $start->diffInMinutes($end);
                    $totalMinutes += $minute;
                }
            }

            $user->hour = floor($totalMinutes / 60);
            $user->days = $workingDays;
            if($user->days != 0 && $user->hour != 0) {
                $user->average = floor($user->hour / $user->days);
            } else {
                $user->average = 0;
            }
        }

        $averageHour = 0;
        foreach($User as $user) {
            $averageHour += $user->average;
        }

        $userCount = 0;
        foreach($userCountGet as $DATA) {
            // $workingDaysGet = Task::distinct('created_at')->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->where('user_id', $DATA->id)->whereNull('absence_id')->count();
            
            if($month != null && $year != null) {
                $workingDaysGet = Task::distinct('created_at')->whereMonth('created_at', $month)->whereYear('created_at', $year)->where('user_id', $DATA->id)->whereNull('absence_id')->count();
            } else {
                $workingDaysGet = Task::distinct('created_at')->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->where('user_id', $DATA->id)->whereNull('absence_id')->count();
            }

            if($workingDaysGet != 0) {
                $userCount++;
            }
        }

        // Debug
        // print_r('Total All User: ' . $COUNT_TEST . ' | Average All: ' . $averageHour / $COUNT_TEST . ' | Total User Without Working Hours 0: ' . $userCount . ' | Average Without Working Hours 0: ' . $averageHour / $userCount);

        if($userCount != 0) {
            $totalAverage = $averageHour / $userCount;
        } else {
            $totalAverage = 0;
        }
        
        $this->fragment['Department'] = $Department;
        $this->fragment['User'] = $User;
        $this->fragment['totalAverage'] = $totalAverage;
        $this->fragment['isSearch'] = $isSearch;

        return view("admin-superadmin.average-hours", $this->fragment);
    }

    public function export() {
        $Month = request()->post('month');
        $Year = request()->post('year');
        $Department = request()->post('department');
        $DepartmentName = Department::where('id', $Department)->value('name');

        $timezone = time() + (60 * 60 * 7);
        $dateNow  = date("YmdHi", $timezone);
        $excelName = "Task-" . $DepartmentName . "-" . $Year . $Month . "-" . $dateNow . ".xlsx";

        return Excel::download(new AverageExport($Month, $Year, $Department), $excelName);
    }

    public function create(Request $request) {
        $fieldName = $request->fieldName;

        if($fieldName == 'ProjectName') {
            $validatedData = $request->validate([
                'name' => 'required',
            ]);

            Project::create($validatedData);
        } else if($fieldName == 'TaskName') {
            $validatedData = $request->validate([
                'name' => 'required',
                'project_id' => 'required',
                'department_id' => 'required'
            ]);

            DailyTask::create($validatedData);
        } else if($fieldName == 'TypeOfWork') {
            $validatedData = $request->validate([
                'name' => 'required',
            ]);

            TypeOfWork::create($validatedData);
        } else if($fieldName == 'SiteName') {
            $validatedData = $request->validate([
                'name' => 'required',
            ]);

            Site::create($validatedData);
        } else if($fieldName == 'KindOfTransportation') {
            $validatedData = $request->validate([
                'name' => 'required',
            ]);

            Transportation::create($validatedData);
        } else if($fieldName == 'DayOff') {
            $validatedData = $request->validate([
                'name' => 'required',
            ]);

            Absence::create($validatedData);
        }

        return redirect()->back();
    }

    public function delete(Request $request) {
        $fieldName = $request->fieldName;

        if($fieldName == 'ProjectName') {
            Project::destroy($request->id);
        } else if($fieldName == 'TaskName') {
            DailyTask::destroy($request->id);
        } else if($fieldName == 'TypeOfWork') {
            TypeOfWork::destroy($request->id);
        } else if($fieldName == 'SiteName') {
            Site::destroy($request->id);
        } else if($fieldName == 'KindOfTransportation') {
            Transportation::destroy($request->id);
        } else if($fieldName == 'DayOff') {
            Absence::destroy($request->id);
        }

        return redirect()->back();
    }
}