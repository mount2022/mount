<?php

namespace App\Http\Controllers;

use App\Models\Mood;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MoodController extends Controller
{
    public function index(Request $request) {
        $validatedData = [
            'user_id' => auth()->user()->id,
            'department_id' => auth()->user()->department_id,
            'mood' => $request->rating,
            'created_date' => $request->date
        ];
        
        Mood::create($validatedData);
        
        return redirect()->back()->with('error_code', 5);
    }
}
