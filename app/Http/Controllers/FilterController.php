<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function index(){
        $today = today(); 
        $dates = []; 
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i);
        }

        if($date = request()->get('date')){
            for($i=1; $i < $date->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($date->year, $date->month, $i);
            }
        }
        

        $this->fragment['dates'] = $dates;

        return view('employee.tasks.filter',$this->fragment);
    }
}
