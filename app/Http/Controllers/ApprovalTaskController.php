<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\User;
use App\Models\Task;
use App\Models\Project;
use App\Models\Site;
use App\Models\Transportation;
use App\Models\TypeOfWork;
use App\Models\Notification;
use App\Models\Mood;
use App\Models\Approval;
use App\Models\Unlock;
use App\Exports\TaskExport;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;

use App\Events\MessageCreated;

use Carbon\Carbon;
use Illuminate\Http\Request;

class ApprovalTaskController extends Controller
{
    public function index() {
        $today = today(); 
        $startDate = Carbon::createFromDate($today->year, $today->month, 1);

        //logic filter
        $month = request()->get('month');
        $year = request()->get('year');
        if($month != null && $year != null ){
            $startDate = Carbon::createFromDate($year, $month, 1);
        }

        if(auth()->user()->role_id == 2) {
            if(request()->get('search') != null) {
                // Admin - search
                $keyword = '%' . request()->get('search') . '%';
                $users = User::whereHas('tasks', function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                })->where('department_id', auth()->user()->department_id)->where('name', 'like', $keyword)->with(['department','tasks' => function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                },'tasks.approvals']);
            } else {
                // Admin - select all 
                $users = User::whereHas('tasks', function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                })->where('department_id', auth()->user()->department_id)->with(['department','tasks' => function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                },'tasks.approvals']);
            }
        }

        if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
            if(request()->get('search') != null) {
                // Super Admin - search
                $keyword = '%' . request()->get('search') . '%';
                $users = User::whereHas('tasks', function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                })->where('name', 'like', $keyword)->with(['department','tasks' => function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                },'tasks.approvals']);
    
            } else {
                // Super Admin - select all
                $users = User::whereHas('tasks', function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                })->with(['department','tasks' => function($query)use($startDate){
                    $query->whereYear('created_at', $startDate->year)
                    ->whereMonth('created_at', $startDate->month);
                },'tasks.approvals']);
            }
        }

        $users = $users->get();
    
        foreach($users as $user){
            $tasks = $user->tasks;
            $user->status = "All Approved";

            foreach($tasks as $task){
                $task->status = 'All Approved';
                $approvals = $task->approvals;
                
                foreach($approvals as $approval){
                    if($approval->approval_status != 'Approved'){
                        $task->status = "Pending";
                        $user->trueStatus = $approval->approval_status;
                    }
                }
                if($task->status == 'Pending'){
                    $user->status = "Pending";
                }
            }
        }

        $this->fragment['users'] = $users;
        $this->fragment['today'] = $today;

        if($month != null && $year != null ) {
            $DATE = Carbon::parse($startDate)->format('F Y');
            $MONTH = Carbon::parse($startDate)->format('m');
            $YEAR = Carbon::parse($startDate)->format('Y');
            session()->put( 'DATE', $DATE);
            session()->put( 'MONTH', $MONTH);
            session()->put( 'YEAR', $YEAR);
        }

        return view('admin-superadmin.approval-task', $this->fragment);

        // if (auth()->user()->role_id != 3 ) {
        //     $users = User::whereHas('tasks', function($query)use($startDate){
        //         $query->whereYear('created_at', $startDate->year)
        //         ->whereMonth('created_at', $startDate->month);
        //     })->where('department_id', auth()->user()->department_id)->with(['department','tasks' => function($query)use($startDate){
        //         $query->whereYear('created_at', $startDate->year)
        //         ->whereMonth('created_at', $startDate->month);
        //     },'tasks.approvals']);
        // }
        // else {
        //     $users = User::whereHas('tasks', function($query)use($startDate){
        //         $query->whereYear('created_at', $startDate->year)
        //         ->whereMonth('created_at', $startDate->month);
        //     })->with(['department','tasks' => function($query)use($startDate){
        //         $query->whereYear('created_at', $startDate->year)
        //         ->whereMonth('created_at', $startDate->month);
        //     },'tasks.approvals']);
        // }
    }

    public function approve(Request $request){
        $userId = $request->userId;
        $today = today(); 
        
        $startDate = Carbon::createFromDate($today->year, $today->month, 1);

        //logic filter
        $month = request()->post('month');
        $year = request()->post('year');
        if($month != null && $year != null){
            $startDate = Carbon::createFromDate($year, $month, 1);
        }

        // dd($startDate->month, $startDate->year);

        $user = User::whereHas('tasks', function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
                ->whereMonth('created_at', $startDate->month);
        })->with(['department','tasks' => function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
                ->whereMonth('created_at', $startDate->month);
        },'tasks.approvals' => function($query){
            $query->where('user_id', auth()->id());
        }]);

        $user = $user->findOrFail($userId);
        // dd($user->tasks);
        
        foreach($user->tasks as $task) {
            
            if(request()->post('date') != null) {

                // -- Approve per tanggal -- //
                if($task->created_at->format('Y-m-d') == request()->post('date')) {
                    foreach($task->approvals as $approval) {
                        if($approval->approval_status != 'Approved') {
                            $approval->approval_status = 'Approved';
                            $approval->update();

                            // -- Notification -- //
                            $time = Carbon::parse($task->created_at)->format('l, d F');
                            $timezone = time() + (60 * 60 * 7);
                            
                            if($task->absence_id == null) {
                                if($task->name == "Other, please mention in Task Remark") {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Task Submission Approval',
                                        'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Approved by Super Admin",
                                        'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Approved",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                } else {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Task Submission Approval',
                                        'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                                        'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Approved",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                }
                                
                                $id = Notification::create($message)->id;
                                $message['id'] = $id;
                            } else {
                                if($task->name == "Other, please mention in Absence Remark") {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Day Off Permisson Approval',
                                        'body' => "Your record for day off \"" . $task->absence_remark . "\" in " . $time . " Approved by Super Admin",
                                        'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Approved",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                } else {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Day Off Permisson Approval',
                                        'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                                        'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Approved",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                }
                                
        
                                $id = Notification::create($message)->id;
                                $message['id'] = $id;
                            }
        
                            MessageCreated::dispatch($message);
                            // -- Notification -- //
                        }
                    }
                }
                // -- Approve per tanggal -- //

            } else {

                // -- Approve All Bulan Ini -- //

                foreach($task->approvals as $approval){
                    if($approval->approval_status != 'Approved') {
                        $approval->approval_status = 'Approved';
                        $approval->update();

                        // -- Notification -- //
                        $time = Carbon::parse($task->created_at)->format('l, d F');
                        $timezone = time() + (60 * 60 * 7);
        
                        if($task->absence_id == null) {
                            if($task->name == "Other, please mention in Task Remark") {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $request->userId,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Approval',
                                    'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Approved by Super Admin",
                                    'optional_body' => "Your task \"" . $task->daily_task_remark . "\" in " . $time . " is Approved",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            } else {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $request->userId,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Approval',
                                    'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                                    'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Approved",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            }
        
                            $id = Notification::create($message)->id;
                            $message['id'] = $id;
                        } else {
                            
                            $message = [
                                'sender_id' => auth()->id(),
                                'receiver_id' => $request->userId,
                                'task_id' => $task->id,
                                'title' => 'Day Off Permisson Approval',
                                'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                                'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Approved",
                                'time_created' => date("l, d F H:i", $timezone),
                                'status' => 'unread'
                            ];
        
                            $id = Notification::create($message)->id;
                            $message['id'] = $id;
                        }
        
                        MessageCreated::dispatch($message);
                        // -- Notification -- //
                    }
                }
                // -- Approva All Bulan Ini -- //

            }
        }

        if($month != null && $year != null ) {
            $DATE = Carbon::parse($startDate)->format('F Y');
            return redirect()->route('dashboard.admin', ['month' => $month, 'year' => $year])->with( [ 'DATE' => $DATE ] );
        }

        if(request()->post('date') != null) {
            return redirect('/dashboard/task-detail?date=' . request()->post('date'));
        }

        if(request()->post('id') != null){
            return redirect('/dashboard/task-detail?id=' . request()->post('id'));
        }

        return redirect('/dashboard-admin/approval');

    }

    public function reject(Request $request){
        $userId = $request->userId;
        $today = today(); 
        
        $startDate = Carbon::createFromDate($today->year, $today->month, 1);

        //logic filter
        $month = request()->post('month');
        $year = request()->post('year');
        if($month != null && $year != null ){
            $startDate = Carbon::createFromDate($year, $month, 1);

        }

        $user = User::whereHas('tasks', function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
             ->whereMonth('created_at', $startDate->month);
        })->with(['department','tasks' => function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
             ->whereMonth('created_at', $startDate->month);
        },'tasks.approvals' => function($query){
            $query->where('user_id', auth()->id());
        }]);

        $user = $user->findOrFail($userId);
        foreach($user->tasks as $task){

            if(request()->post('date') != null) {

                // -- Rejection per tanggal -- //
                if($task->created_at->format('Y-m-d') == request()->post('date')) {
                    foreach($task->approvals as $approval){
                        if($approval->approval_status != 'Rejected') {
                            $approval->approval_status = 'Rejected';
                            $approval->update();

                            // -- Notification -- //
                            $time = Carbon::parse($task->created_a)->format('l, d F');
                            $timezone = time() + (60 * 60 * 7);
                
                            if($task->absence_id == null) { 
                                if($task->name == "Other, please mention in Task Remark") {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Task Submission Rejection',
                                        'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Rejected by Super Admin",
                                        'optional_body' => "Your task \"" . $task->daily_task_remark . "\" in " . $time . " is Rejected",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                } else {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Task Submission Rejection',
                                        'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                                        'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Rejected",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                }
                                
                
                                $id = Notification::create($message)->id;
                                $message['id'] = $id;
                            } else {
                                if($task->name == "Other, please mention in Absence Remark") {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Day Off Permisson Rejection',
                                        'body' => "Your record for day off \"" . $task->absence_remark . "\" in " . $time . " Rejected by Super Admin",
                                        'optional_body' => "Your day off \"" . $task->absence_remark . "\" in " . $time . " is Rejected",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                } else {
                                    $message = [
                                        'sender_id' => auth()->id(),
                                        'receiver_id' => $request->userId,
                                        'task_id' => $task->id,
                                        'title' => 'Day Off Permisson Rejection',
                                        'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                                        'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Rejected",
                                        'time_created' => date("l, d F H:i", $timezone),
                                        'status' => 'unread'
                                    ];
                                }
                                
                
                                $id = Notification::create($message)->id;
                                $message['id'] = $id;
                            }
                
                            MessageCreated::dispatch($message);
                            // -- Notification -- //\
                        }
                    }
                }
                // -- Rejection per tanggal -- //

            } else {

                // -- Rejection All -- //
                foreach($task->approvals as $approval){
                    if($approval->approval_status != 'Rejected') {
                        $approval->approval_status = 'Rejected';
                        $approval->update();

                        // -- Notification -- //
                        $time = Carbon::parse($task->created_a)->format('l, d F');
                        $timezone = time() + (60 * 60 * 7);
            
                        if($task->absence_id == null) { 
                            if($task->name == "Other, please mention in Task Remark") {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $request->userId,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Rejection',
                                    'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your task \"" . $task->daily_task_remark . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            } else {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $request->userId,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Rejection',
                                    'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            }

                            $id = Notification::create($message)->id;
                            $message['id'] = $id;
                        } else {
                            if($task->name == "Other, please mention in Absence Remark") {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $request->userId,
                                    'task_id' => $task->id,
                                    'title' => 'Day Off Permisson Rejection',
                                    'body' => "Your record for day off \"" . $task->absence_remark . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your day off \"" . $task->absence_remark . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            } else {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $request->userId,
                                    'task_id' => $task->id,
                                    'title' => 'Day Off Permisson Rejection',
                                    'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            }
                            
                            $id = Notification::create($message)->id;
                            $message['id'] = $id;
                        }
            
                        MessageCreated::dispatch($message);
                        // -- Notification -- //
                    }
                }
                // -- Rejection All -- //

            }
        }

        if($month != null && $year != null ) {
            $DATE = Carbon::parse($startDate)->format('F Y');
            return redirect()->route('dashboard.admin', ['month' => $month, 'year' => $year])->with( [ 'DATE' => $DATE ] );
        }

        if(request()->post('date') != null) {
            return redirect('/dashboard/task-detail?date=' . request()->post('date'));
        }

        if(request()->post('id') != null){
            return redirect('/dashboard/task-detail?id=' . request()->post('id'));
        }

        return redirect('/dashboard-admin/approval');
    }
    
    public function approveSelected(Request $request) {
        $userId = request()->post('ids');
        $today = today(); 
        $startDate = Carbon::createFromDate($today->year, $today->month, 1);

        //logic filter
        $month = request()->post('month');
        $year = request()->post('year');
        if($month != null && $year != null ){
            $startDate = Carbon::createFromDate($year, $month, 1);
        }

        $user = User::whereHas('tasks', function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
                ->whereMonth('created_at', $startDate->month);
        })->with(['department','tasks' => function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
                ->whereMonth('created_at', $startDate->month);
        },'tasks.approvals' => function($query){
            $query->where('user_id', auth()->id());
        }]);

        $user = $user->findOrFail($userId);
        foreach($user as $users) {
            foreach($users->tasks as $task) {
                foreach($task->approvals as $approval) {
                    if($approval->approval_status != 'Approved') {
                        $approval->approval_status = 'Approved';
                        $approval->update();

                        $time = Carbon::parse($task->created_at)->format('l, d F');
                        $timezone = time() + (60 * 60 * 7);
            
                        if($task->absence_id == null) { 
                            if($task->name == "Other, please mention in Task Remark") {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Approval',
                                    'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Approved by Super Admin",
                                    'optional_body' => "Your task \"" . $task->daily_task_remark . "\" in " . $time . " is Approved",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            } else {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Approval',
                                    'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                                    'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Approved",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            }

                            $id = Notification::create($message)->id;
                            $message['id'] = $id;

                            MessageCreated::dispatch($message);
                        } else {
                            if($task->name == "Other, please mention in Absence Remark") {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Day Off Permisson Approval',
                                    'body' => "Your record for day off \"" . $task->absence_remark . "\" in " . $time . " Approved by Super Admin",
                                    'optional_body' => "Your day off \"" . $task->absence_remark . "\" in " . $time . " is Approved",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            } else {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Day Off Permisson Approval',
                                    'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                                    'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Approved",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            }

                            $id = Notification::create($message)->id;
                            $message['id'] = $id;         
                            
                            MessageCreated::dispatch($message);
                        }
                    }
                }
            }
        }

        return response()->json(['success' => "Success"]);
    }

    public function rejectSelected(Request $request) {
        $userId = request()->post('ids');
        $today = today();
        $startDate = Carbon::createFromDate($today->year, $today->month, 1);

        //logic filter
        $month = request()->post('month');
        $year = request()->post('year');
        if($month != null && $year != null ){
            $startDate = Carbon::createFromDate($year, $month, 1);
        }

        $user = User::whereHas('tasks', function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
                ->whereMonth('created_at', $startDate->month);
        })->with(['department','tasks' => function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
                ->whereMonth('created_at', $startDate->month);
        },'tasks.approvals' => function($query){
            $query->where('user_id', auth()->id());
        }]);

        $user = $user->findOrFail($userId);
        foreach($user as $users) {
            foreach($users->tasks as $task){
                foreach($task->approvals as $approval){
                    if($approval->approval_status != 'Rejected') { 
                        $approval->approval_status = 'Rejected';
                        $approval->update();

                        $time = Carbon::parse($task->created_at)->format('l, d F');
                        $timezone = time() + (60 * 60 * 7);
        
                        if($task->absence_id == null) {
                            if($task->name == "Other, please mention in Task Remark") {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Rejection',
                                    'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your task \"" . $task->daily_task_remark . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            } else {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Task Submission Rejection',
                                    'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            }
                            
                            $id = Notification::create($message)->id;
                            $message['id'] = $id;
                            MessageCreated::dispatch($message);
                        } else {
                            if($task->name == "Other, please mention in Absence Remark") {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Day Off Permisson Rejection',
                                    'body' => "Your record for day off \"" . $task->absence_remark . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your day off \"" . $task->absence_remark . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            } else {
                                $message = [
                                    'sender_id' => auth()->id(),
                                    'receiver_id' => $users->id,
                                    'task_id' => $task->id,
                                    'title' => 'Day Off Permisson Rejection',
                                    'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                                    'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Rejected",
                                    'time_created' => date("l, d F H:i", $timezone),
                                    'status' => 'unread'
                                ];
                            }
        
                            $id = Notification::create($message)->id;
                            $message['id'] = $id;
                            MessageCreated::dispatch($message);
                        }
                    }
                }
            }
        }

        return response()->json(['success' => "Success"]);
    }

    public function undo(Request $request){
        $userId = $request->userId;
        $today = today(); 
        
        $startDate = Carbon::createFromDate($today->year, $today->month, 1);

        //logic filter
        $month = request()->post('month');
        $year = request()->post('year');
        if($month != null && $year != null ){
            $startDate = Carbon::createFromDate($year, $month, 1);
        }

        $user = User::whereHas('tasks', function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
             ->whereMonth('created_at', $startDate->month);
        })->with(['department','tasks' => function($query)use($startDate){
            $query->whereYear('created_at', $startDate->year)
             ->whereMonth('created_at', $startDate->month);
        },'tasks.approvals' => function($query){
            $query->where('user_id', auth()->id());
        }]);

        $user = $user->findOrFail($userId);
        foreach($user->tasks as $task) {
            if(request()->post('date') != null) {

                 // -- Undo per tanggal -- //
                if($task->created_at->format('Y-m-d') == request()->post('date')) {
                    foreach($task->approvals as $approval) {
                        if($approval->approval_status != 'Not Approved') {
                            $approval->approval_status = 'Not Approved';
                            $approval->update();
                        }
                    }
                }
                // -- Undo per tanggal -- //

            } else {

                // -- Undo All -- //
                foreach($task->approvals as $approval) {
                    if($approval->approval_status != 'Not Approved') {
                        $approval->approval_status = 'Not Approved';
                        $approval->update();
                    }
                }
                // -- Undo All -- //

            }
        }

        if($month != null && $year != null ) {
            $DATE = Carbon::parse($startDate)->format('F Y');
            return redirect()->route('dashboard.admin', ['month' => $month, 'year' => $year])->with( [ 'DATE' => $DATE ] );
        }

        if(request()->post('date') != null) {
            return redirect('/dashboard/task-detail?date=' . request()->post('date'));
        }
        
        if(request()->post('id') != null){
            return redirect('/dashboard/task-detail?id=' . request()->post('id'));
        }
        
        return redirect('/dashboard-admin/approval');
    }

    public function approveSingle(Request $request) {
        $taskId = request()->post('taskId');

        $approvals = Approval::where('task_id', $taskId)->get();
        $tasks = Task::where('id', $taskId)->get();
        
        foreach($approvals as $approval) {
            if($approval->approval_status != 'Approved') {
                $approval->approval_status = 'Approved';
                $approval->update();
            }
        }

        // -- Notification -- //
        foreach($tasks as $task) {
            $time = Carbon::parse($task->created_at)->format('l, d F');
            $timezone = time() + (60 * 60 * 7);

            if($task->absence_id == null) {
                if($task->name == "Other, please mention in Task Remark") {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Task Submission Approval',
                        'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Approved by Super Admin",
                        'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Approved",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                } else {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Task Submission Approval',
                        'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                        'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Approved",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                }
                
                $id = Notification::create($message)->id;
                $message['id'] = $id;
            } else {
                if($task->name == "Other, please mention in Absence Remark") {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Day Off Permisson Approval',
                        'body' => "Your record for day off \"" . $task->absence_remark . "\" in " . $time . " Approved by Super Admin",
                        'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Approved",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                } else {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Day Off Permisson Approval',
                        'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Approved by Super Admin",
                        'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Approved",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                }
                

                $id = Notification::create($message)->id;
                $message['id'] = $id;
            }

            MessageCreated::dispatch($message);
        }
        // -- Notification -- //

        return redirect()->back();
    }

    public function rejectSingle(Request $request) {
        $taskId = request()->post('taskId');

        $approvals = Approval::where('task_id', $taskId)->get();
        $tasks = Task::where('id', $taskId)->get();
        
        foreach($approvals as $approval) {
            if($approval->approval_status != 'Rejected') {
                $approval->approval_status = 'Rejected';
                $approval->update();
            }
        }

        // -- Notification -- //
        foreach($tasks as $task) {
            $time = Carbon::parse($task->created_at)->format('l, d F');
            $timezone = time() + (60 * 60 * 7);

            if($task->absence_id == null) {
                if($task->name == "Other, please mention in Task Remark") {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Task Submission Rejection',
                        'body' => "Your record for task \"" . $task->daily_task_remark . "\" in " . $time . " Rejected by Super Admin",
                        'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Rejected",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                } else {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Task Submission Rejection',
                        'body' => "Your record for task \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                        'optional_body' => "Your task \"" . $task->name . "\" in " . $time . " is Rejected",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                }
                
                $id = Notification::create($message)->id;
                $message['id'] = $id;
            } else {
                if($task->name == "Other, please mention in Absence Remark") {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Day Off Permisson Rejection',
                        'body' => "Your record for day off \"" . $task->absence_remark . "\" in " . $time . " Rejected by Super Admin",
                        'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Rejected",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                } else {
                    $message = [
                        'sender_id' => auth()->id(),
                        'receiver_id' => $task->user_id,
                        'task_id' => $task->id,
                        'title' => 'Day Off Permisson Rejection',
                        'body' => "Your record for day off \"" . $task->name . "\" in " . $time . " Rejected by Super Admin",
                        'optional_body' => "Your day off \"" . $task->name . "\" in " . $time . " is Rejected",
                        'time_created' => date("l, d F H:i", $timezone),
                        'status' => 'unread'
                    ];
                }

                $id = Notification::create($message)->id;
                $message['id'] = $id;
            }

            MessageCreated::dispatch($message);
        }
        // -- Notification -- //

        return redirect()->back();
    }

    public function undoSingle(Request $request) {
        $taskId = request()->post('taskId');
        $approvals = Approval::where('task_id', $taskId)->get();
        
        foreach($approvals as $approval) {
            if($approval->approval_status != 'Not Approved') {
                $approval->approval_status = 'Not Approved';
                $approval->update();
            }
        }

        return redirect()->back();
    }

    public function export(Request $request) {
        $Month = request()->post('month');
        $Year = request()->post('year');

        $userId = $request->userId;
        $userName = $request->userName;
        $timezone = time() + (60 * 60 * 7);
        $dateNow  = date("YmdHi", $timezone);
        $excelName = "Task-" . $userName . "-" . $dateNow . ".xlsx";

        return Excel::download(new TaskExport($userId, $Month, $Year), $excelName);
    }

    public function unlock(Request $request) {
        date_default_timezone_set('Asia/Jakarta');
        $datetime = new DateTime(date('Y-m-d H:i:s'));
        $datetime->modify('+1 day');    
        
        if(session('user_id') != null) {
            $unlockData = [
                'user_id' => session('user_id'),
                'date_unlock' => request()->post('date'),
                'begin' => date('Y-m-d H:i:s'),
                'end' => $datetime->format('Y-m-d H:i:s'),
            ];
        }
        
        Unlock::create($unlockData);

        if(request()->post('date') != null) {
            return redirect('/dashboard/task-detail?date=' . request()->post('date'));
        }
    }

    public function detail() {
        $isMoodEmpty = true;
        $isMonth = true;

        /*** Area for adding CSS ***/
        $this->fragment['mainapp']['css'] = [
            "text/css,stylesheet,".url("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"),
            "text/css,stylesheet,".url("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"),
            "text/css,stylesheet,".url("plugins/datatables-buttons/css/buttons.bootstrap4.min.css"),
        ];
        /*** Area for adding JS ***/
        $this->fragment['mainapp']['js'] = [
            url("plugins/datatables/jquery.dataTables.min.js"),
            url("plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"),
            url("plugins/datatables-responsive/js/dataTables.responsive.min.js"),
            url("plugins/datatables-responsive/js/responsive.bootstrap4.min.js"),
            url("js/employee/alltask.js"),
        ];

        /*** Area for adding DB ***/
        $today = today(); 
        $dates = []; 
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i);
        }

        if(request()->get('id') != null && request()->get('MONTH') != null && request()->get('YEAR') != null) {
            $data = Task::where('user_id', request()->get('id'))->whereMonth('created_at', request()->get('MONTH'))->whereYear('created_at', request()->get('YEAR'));
            session(['user_id' => request()->get('id'), 'MONTH' => request()->get('MONTH'), 'YEAR' => request()->get('YEAR')]);
        } else {
            $data = Task::where('user_id', session('user_id'))->whereMonth('created_at', session('MONTH'))->whereYear('created_at', session('YEAR'));
        }
    
        if(request()->get('id') != null) {
            $isCompleted = Task::where([
                ['task_status', "Completed"],
                ['user_id', request()->get('id')],
                ]);
            $isInProgress = Task::where([
                ['task_status', "In Progress"],
                ['user_id', request()->get('id')],
                ]);
        } else {
            $isCompleted = Task::where([
                ['task_status', "Completed"],
                ['user_id', session('user_id')],
                ]);
            $isInProgress = Task::where([
                ['task_status', "In Progress"],
                ['user_id', session('user_id')],
                ]);
        }

        // Get profile
        if(request()->get('id') != null) {
            $user = User::where('id', request()->get('id'))->get();
            $department = Department::where('id', $user[0]->department_id)->get('name');
        } else {
            $user = User::where('id', session('user_id'))->get();
            $department = Department::where('id', $user[0]->department_id)->get('name');
        } 

        //logic filter
        $month = request()->get('MONTH');
        $year = request()->get('YEAR');
        if($month != null && $year != null ){
            $dates = array();
            $dt = Carbon::createFromDate($year, $month);
            //dd($month);
            for($i=1; $i < $dt->daysInMonth + 1; ++$i) {
                $dates[] = \Carbon\Carbon::createFromDate($dt->year, $dt->month, $i);
            }
            // $data = $data->whereDate('created_at', $dt );
            $data = $data->whereMonth('created_at', $month)->whereYear('created_at', $year);
        }

        //Logic Saat pencet Sidebar
        if($date = request()->get('date')){
            $isMonth = false;
            $isCompleted = $isCompleted->whereDate('created_at', $date);
            $isInProgress = $isInProgress->whereDate('created_at', $date);
            
            $data = $data->whereDate('created_at', $date );
            $dt = Carbon::createFromFormat('Y-m-d', $date);   
            $dates = array();    
            for($i=1; $i < $dt->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($dt->year, $dt->month, $i);
            }

            session(['date' => $date]);
        }

        if(request()->get('date') == null) {
            $isCompleted = $isCompleted->whereMonth('created_at', request()->get('MONTH'))->whereYear('created_at', request()->get('YEAR'));
            $isInProgress = $isInProgress->whereMonth('created_at', request()->get('MONTH'))->whereYear('created_at', request()->get('YEAR'));
        }

        if(request()->get('id') != null) {
            $mood = Mood::where('user_id', request()->get('id'))->where('created_date', $date)->count();
        } else {
            $mood = Mood::where('user_id', session('user_id'))->where('created_date', $date)->count();
        }

        // dd($mood);

        if ($mood > 0 || $date != null) {
            $isMoodEmpty = false;

            if(request()->get('id') != null) {
                $mood = Mood::where('user_id', request()->get('id'))->where('created_date', $date)->get();
            } else {
                $mood = Mood::where('user_id', session('user_id'))->where('created_date', $date)->get();
            }
        }

        // dd($mood);

        //logic time all task 
        $data = $data->get();
        
        foreach($data as $Data) {
            $Data->approval_status = Approval::where('task_id', $Data->id)->value('approval_status');
        }

        $isCompleted = $isCompleted->count();
        $isInProgress = $isInProgress->count();
        
        // Fragments
        $this->fragment['isCompleted'] = $isCompleted;
        $this->fragment['isInProgress'] = $isInProgress;
        $this->fragment['dates'] = $dates;
        $this->fragment['today'] = $today;
        $this->fragment['tasks'] = $data;
        $this->fragment['user'] = $user;
        $this->fragment['department'] = $department;
        $this->fragment['isMoodEmpty'] = $isMoodEmpty;
        $this->fragment['isMonth'] = $isMonth;

        if ($isMoodEmpty == false) {
            $this->fragment['mood'] = $mood;
        }

        $dates = null;
        
        /*** View ***/
        return view('admin-superadmin.alltask-superadmin', $this->fragment);
    }

    public function show($id)
    {
        $tasks = Task::where('id', $id)->get();
        foreach ($tasks as $task) {
            $project = Project::where('id', $task->project_id)->value('name');
            if($task->project_id == 0){
                $project = "Other, please mention in Project Remark";
            } else {
                $project = Project::where('id', $task->project_id)->value('name');
            }

            if($task->site_id == 0){
                $site = "Other, please mention in Site Remark";
            } else {
                $site = Site::where('id', $task->site_id)->value('name');
            }
            
            $transportation = Transportation::where('id', $task->transportation_id)->value('name');
            $typeOfWork = TypeOfWork::where('id', $task->type_of_work_id)->value('name');
            $approval = Approval::where('task_id', $task->id)->value('approval_status');
        }
        
        $this->fragment['project'] = $project;
        $this->fragment['site'] = $site;
        $this->fragment['transportation'] = $transportation;
        $this->fragment['type_of_work'] = $typeOfWork;
        $this->fragment['approval'] = $approval;
        $this->fragment['tasks'] = $tasks;

        return view('admin-superadmin.detail-task-superadmin', $this->fragment);
    }

    public function showDayOff($id) 
    {
        $tasks = Task::where('id', $id)->get();
        foreach ($tasks as $task) {
            $project = Project::where('id', $task->project_id)->value('name');
            $site = Site::where('id', $task->site_id)->value('name');
            $transportation = Transportation::where('id', $task->transportation_id)->value('name');
            $typeOfWork = TypeOfWork::where('id', $task->type_of_work_id)->value('name');
            $approval = Approval::where('task_id', $task->id)->value('approval_status');
        }
        
        $this->fragment['project'] = $project;
        $this->fragment['site'] = $site;
        $this->fragment['transportation'] = $transportation;
        $this->fragment['type_of_work'] = $typeOfWork;
        $this->fragment['approval'] = $approval;
        $this->fragment['Task'] = $tasks;

        return view('admin-superadmin.detail-dayoff-superadmin', $this->fragment);
    }
}