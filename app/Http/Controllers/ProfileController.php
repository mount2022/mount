<?php

namespace App\Http\Controllers;
use App\Http\Requests\UpdatePasswordRequest;

use App\Models\User;
use App\Models\Training;
use App\Models\Notification;
use App\Models\DefaultTraining;
use App\Models\DefaultKPI;
use App\Models\KPI;
use App\Models\HeaderKPI;
use App\Models\JobDescription;
use Database\Seeders\TrainingSeeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index() {

        return view("employee.settings-profile",[
            'users' => User::where('id', auth()->user()->id)->get()
        ]);
    }

    public function edit() {
        return view("employee.change-password");
    }

    public function update(UpdatePasswordRequest $request) {
        $request->user()->update([
            'password' => Hash::make($request->get('password'))
        ]);

        return redirect()->back()->with('success', 'password updated successfully!');
    }

    public function notification() {
        $datas = Notification::where('receiver_id', auth()->user()->id)->get();
        
        $this->fragment['notification'] = $datas;
        return view("employee.notification", $this->fragment);
    }

    public function crop(Request $request) {
        $file = $request->file('wizard_picture');
        $path = 'user/images/';
        $newName = 'UIMG_' . date('Ymd') . uniqid() . '.jpg';
        $upload = $file->move(public_path($path), $newName);
        
        $update = User::where('id', auth()->user()->id)->get();
        foreach($update as $data) {
            $data->picture = $newName;
            $data->update();
        }

        if(!$upload) {
            return response()->json(['status'=> 0, 'msg' => 'Update went wrong, try again later']);
        } else {
            return response()->json(['status'=> 1, 'msg' => 'Your image has been uploaded']);
        }
    }

    public function clearNotification(Request $request) {
        Notification::where('receiver_id', auth()->user()->id)->delete();

        return redirect()->back();
    }

    public function KeyPerformanceIndicator() {
        $KPI = KPI::where('user_id', auth()->user()->id)->get();
        $KPI_Title = HeaderKPI::where('user_id', auth()->user()->id)->get();

        $this->fragment['KPI'] = $KPI;
        $this->fragment['KPI_Title'] = $KPI_Title;

        return view("employee.kpi", $this->fragment);
    }

    public function JobDescription() {
        $JobDesc = JobDescription::where('user_id', auth()->user()->id)->get();

        $this->fragment['JobDesc'] = $JobDesc;

        return view("employee.jobdesc", $this->fragment);
    }

    public function TrainingPlan() {
        $Trainings = Training::where('user_id', auth()->user()->id)->get();
        $TrainingTitle = DefaultTraining::where('note', 'Title')->where('user_id', auth()->user()->id)->get();

        $this->fragment['Trainings'] = $Trainings;
        $this->fragment['TrainingTitle'] = $TrainingTitle;

        return view("employee.training-plan", $this->fragment);
    }

    /*-- Super Admin -- */
    public function ProfileSA($id) {
        $Users = User::where('id', $id)->get();

        $this->fragment['Users'] = $Users;

        return view("admin-superadmin.employee-detail.settings-profile", $this->fragment);
    }

    public function ChangePasswordSA($id) {
        $Users = User::where('id', $id)->get();

        $this->fragment['Users'] = $Users;

        return view("admin-superadmin.employee-detail.change-password", $this->fragment);
    }

    public function KeyPerformanceIndicatorSA($id) {
        $KPITitle = HeaderKPI::where('user_id', request()->route('id'))->get();
        $KPIS = KPI::where('user_id', request()->route('id'))->get();

        if($KPIS->isEmpty() == true) {
            $KPIS = DefaultKPI::all();
        }

        $this->fragment['KPITitle'] = $KPITitle;
        $this->fragment['KPIS'] = $KPIS;

        return view("admin-superadmin.employee-detail.kpi-editable", $this->fragment);
    }

    public function JobDescriptionSA($id) {
        $JobDesc = JobDescription::where('user_id', request()->route('id'))->get();

        $this->fragment['JobDesc'] = $JobDesc;

        return view("admin-superadmin.employee-detail.jobdesc-editable", $this->fragment);
    }

    public function TrainingPlanSA($id) {
        $Trainings = Training::where('user_id', request()->route('id'))->get();
        $TrainingTitle = DefaultTraining::where('note', 'Title')->where('user_id', request()->route('id'))->get();

        if($Trainings->isEmpty() == true) {
            $Trainings = DefaultTraining::where('user_id', '0')->where('note', 'DEFAULT')->get();
        }

        $this->fragment['Trainings'] = $Trainings;
        $this->fragment['TrainingTitle'] = $TrainingTitle;

        return view("admin-superadmin.employee-detail.training-plan-editable", $this->fragment);
    }

    public function ResetPassword(Request $request) {
        User::where('id', $request->id)->update(['password' => Hash::make('12345')]);

        return redirect()->back();
    }

    public function TitleTraining(Request $request) {
        $data = array(
            'user_id' => $request->route_id,
            'training_type' => $request->title,
            'when' => $request->period,
            'note' => 'Title'
        );

        $DATA = DefaultTraining::where('note', 'Title')->where('user_id', $request->route_id)->count();
        
        if($DATA > 0) {
            $DATA = DefaultTraining::where('note', 'Title')->where('user_id', $request->route_id)->firstOrFail();
            $DATA->update($data);
        } else {
            DefaultTraining::create($data);
        }

        return redirect('/dashboard-admin/profile/training/' . $request->route_id);
    }

    public function TitleKPI(Request $request) {
        $data = array(
            'user_id' => $request->route_id,
            'position' => $request->position,
            'total_value' => $request->total_value,
            'total_score' => $request->total_score
        );

        $DATA = HeaderKPI::where('user_id', $request->route_id)->count(); 

        if($DATA > 0) {
            $DATA = HeaderKPI::where('user_id', $request->route_id)->firstOrFail(); 
            $DATA->update($data);
        } else {
            HeaderKPI::create($data);
        }

        return redirect('/dashboard-admin/profile/kpi/' . $request->route_id);
    }

    public function ActionTraining(Request $request) {
        if($request->ajax()) {
            if($request->action == 'edit') {
                $data = array(
                    'user_id'       => $request->user_id,
                    'training_type'	=> $request->training_type,
                    'when'		    => $request->when,
                    'objective'		=> $request->objective,
                    'participant'	=> $request->participant,
                    'requirement'	=> $request->requirement,
                    'note'		    => $request->note
                );

                $TrainingID = Training::find($request->id);
                if($TrainingID == null) {
                    Training::create($data);
                } else {
                    $TrainingID->update($data);
                }
            } 

            if($request->action == 'delete') {
                Training::destroy($request->id);
            }

            return response()->json($request);
        }
    }

    public function ActionKPI(Request $request) {
        if($request->ajax()) {
            if($request->action == 'edit') {
                $data = array(
                    'user_id'       => $request->user_id,
                    'kpi'           => $request->kpi,
                    'value'	        => $request->value,
                    'target'		=> $request->target,
                    'metric'		=> $request->metric,
                    'realization'	=> $request->realization,
                    'score'	        => $request->score,
                    'final_score'	=> $request->final_score
                );

                $KPI_ID = KPI::find($request->id);
                if($KPI_ID == null) {
                    KPI::create($data);
                } else {
                    $KPI_ID->update($data);
                }
            } 

            if($request->action == 'delete') {
                KPI::destroy($request->id);
            }

            return response()->json($request);
        }
    }

    public function ActionJobdesc(Request $request) {
        $data = array(
            'user_id'           => $request->user_id,
            'department'        => $request->department,
            'location'          => $request->location,
            'report'            => $request->report,
            'condition'         => $request->condition,
            'scope'             => $request->scope,
            'supervisory'       => $request->supervisory,
            'responsibilities'  => $request->responsibilities,
        );

        $DATA = JobDescription::where('user_id', $request->user_id)->count();

        if($DATA > 0) {
            $DATA = JobDescription::where('user_id', $request->user_id)->firstOrFail();
            $DATA->update($data);
        } else {
            JobDescription::create($data);
        }

        return redirect('/dashboard-admin/profile/jobdesk/' . $request->user_id);
    }
}
