<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Notification;
use App\Events\MessageCreated;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    function index(){
        /*** Check Session ***/
        if ( session('user') ){ return redirect("login");}

        /*** Data for statistic ***/
        $data = Task::where('user_id', auth()->user()->id)->whereMonth('created_at', date('m'));
        $data = $data->get();

        /*** Get dates in current week ***/
        $day_of_week = date('N', strtotime(date('Y-m-d')));
        $given_date = strtotime(date('Y-m-d'));
        $first_of_week =  date('Y-m-d', strtotime("- {$day_of_week} day", $given_date));
        $first_of_week = strtotime($first_of_week);
        
        for($i = 1 ;$i <= 5; $i++) {
            $week_array[] = date('D, d', strtotime("+ {$i} day", $first_of_week));
            $week_array2[] = date('Y-m-d', strtotime("+ {$i} day", $first_of_week));
        }
        
        /*** Get working hours this week ***/
        $hours = array();  

        foreach($week_array2 as $week) {
            $data2 = Task::where('user_id', auth()->user()->id)->where('created_at', $week)->get();
            $totalMinutes = 0;
            $holiday = 0.5;
            $offday = false;

            if($data2 != null) {
                foreach($data2 as $Data) {
                    $start = new \Carbon\Carbon($Data->start_time);
                    $end   = new \Carbon\Carbon($Data->finish_time);
                    $minute  = $start->diffInMinutes($end);
                    $totalMinutes += $minute;

                    if($Data->absence_id != null) {
                        $offday = true;
                    }
                }

                if($offday == true) {
                    $hours[] = $holiday; 
                } else {
                    $hour = floor($totalMinutes / 60);
                    $hours[] = $hour; 
                }                        
            } else {
                $hours[] = '0';
            }
        }

        /*** Nontifications ***/
        $notification = Notification::where('receiver_id', auth()->user()->id)->get();
        // dd($notification);

        /*** Array for debug ***/
        $n = count($week_array);
        for($i = 0; $i < $n; $i++) {
            $test[] = [
                $week_array[$i] => $hours[$i],
            ];
        }
        // dd($test);
        /*** --Array for debug-- ***/
        
        /*** Fragments ****/
        $this->fragment['tasks'] = $data;
        $this->fragment['one_week_date'] = $week_array;
        $this->fragment['hours'] = $hours;
        $this->fragment['notifications'] = $notification;

        // event(new MessageCreated);
        // MessageCreatedRoute::dispatch('TEST');

        /*** View ***/
        return view("dashboard.index", $this->fragment);
    }
}
