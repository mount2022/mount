<?php

namespace App\Http\Controllers;

use App\Imports\TestImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Department;
use App\Models\User;
use App\Models\Role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->get('search') != null) {
            $keyword = '%' . request()->get('search') . '%';
            $user = User::where('name', 'like', $keyword)->get();
        } else {
            if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4) {
                $user = User::orderBy('department_id', 'ASC')->get();
            } else {
                $user = User::where('department_id', auth()->user()->department_id)->get();
            }
        }

        foreach($user as $key){
            $key->department_name = Department::where('id', $key->department_id)->value('name');
        }

        $departmentAll = Department::all();
        $roleAll = Role::all();

        $this->fragment['user'] = $user;
        $this->fragment['departmentAll'] = $departmentAll;
        $this->fragment['roleAll'] = $roleAll;
        
        return view('admin-superadmin.manageemployee', $this->fragment);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        session()->put('Modal', 'Create');

        $validatedData = $request->validate([
            'fullName' => 'required',
            'phoneNumber' => 'required',
            'homeAddress' => 'required',
            'pob' => 'required',
            'dob' => 'required',
            'Gender' => 'required',
            'employeeID' => 'required',
            'emailAddress' => 'required',
            'departmentID' => 'required',
            'employeePosition' => 'required',
            'employmentStatus' => 'required',
            'jobLevel' => 'required',
            'roleID' => 'required',
            'joinDate' => 'required'
        ]);

        $DATA = array(
            'name' => $request->fullName,
            'phone' => $request->phoneNumber,
            'address' => $request->homeAddress,
            'place_of_birth' => $request->pob,
            'date_of_birth' => $request->dob,
            'gender' => $request->Gender,
            'employee_id' => $request->employeeID,
            'email' => $request->emailAddress,
            'department_id' => $request->departmentID,
            'position' => $request->employeePosition,
            'employment_status' => $request->employmentStatus,
            'job_level' => $request->jobLevel,
            'role_id' => $request->roleID,
            'join_date' => $request->joinDate
        );

        $DATA['password'] = bcrypt('12345');
        $DATA['approval_line'] = User::where('role_id', '3')->value('name');

        User::create($DATA);

        return back()->with('created', 'User has been added!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        session()->put('Modal', 'Edit');
        session()->put('EditID', $id);

        $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'employee_id' => 'required',
            'email' => 'required',
            'department_id' => 'required',
            'position' => 'required',
            'join_date' => 'required',
            'employment_status' => 'required',
            'role_id' => 'required',
            'job_level' => 'required'
        ]);

        $data = User::find($id);
        $data->update($validatedData);

        return back()->with('created', 'User has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        User::destroy(request()->post('id'));

        return back()->with('deleted', 'User has been deleted!');
    }
}
