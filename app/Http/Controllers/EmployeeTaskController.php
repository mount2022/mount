<?php

namespace App\Http\Controllers;

use App\Models\Absence;
use App\Models\Approval;
use App\Models\DailyTask;
use App\Models\Task;
use App\Models\Project;
use App\Models\Site;
use App\Models\Transportation;
use App\Models\TypeOfWork;
use App\Models\User;
use App\Models\Mood;
use App\Models\Reminder;
use Carbon\Carbon;
use DateTime;
use DateInterval;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class EmployeeTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() // All.task
    {
        $isMonth = true;
        $isMoodEmpty = true;
        session()->forget('date');

        /*** Area for adding CSS ***/
        $this->fragment['mainapp']['css'] = [
            "text/css,stylesheet,".url("plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"),
            "text/css,stylesheet,".url("plugins/datatables-responsive/css/responsive.bootstrap4.min.css"),
            "text/css,stylesheet,".url("plugins/datatables-buttons/css/buttons.bootstrap4.min.css"),
        ];
        /*** Area for adding JS ***/
        $this->fragment['mainapp']['js'] = [
            url("plugins/datatables/jquery.dataTables.min.js"),
            url("plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"),
            url("plugins/datatables-responsive/js/dataTables.responsive.min.js"),
            url("plugins/datatables-responsive/js/responsive.bootstrap4.min.js"),
            url("js/employee/alltask.js"),
        ];
        /*** Area for adding DB ***/
        
        $today = today(); 
        $dates = []; 
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i);
        }

        // $data = Task::where('user_id', auth()->user()->id)->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'));
        $data = Task::where('user_id', auth()->user()->id);

        $isCompleted = Task::where([
            ['task_status', "Completed"],
            ['user_id', auth()->user()->id],
            ]);
        $isInProgress = Task::where([
            ['task_status', "In Progress"],
            ['user_id', auth()->user()->id],
            ]);
        
        //logic filter
        $month = request()->get('month');
        $year = request()->get('year');
        if($month != null && $year != null ){
            $dates = array();
            $dt = Carbon::createFromDate($year, $month);
            //dd($month);
            for($i=1; $i < $dt->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($dt->year, $dt->month, $i);
            }
            
            $data = Task::where('user_id', auth()->user()->id)->whereMonth('created_at', $month)->whereYear('created_at', $year);
            // $data = $data->whereDate('created_at', $dt);
            // $data = Task::where('user_id', auth()->user()->id)->whereMonth('created_at', $month)->whereYear('created_at', $year);

            $isCompleted = $isCompleted->whereDate('created_at', $dt);
            $isInProgress = $isInProgress->whereDate('created_at', $dt);
        } else {
            $data = $data->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'));
            $isCompleted = $isCompleted->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'));
            $isInProgress = $isInProgress->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'));
        }

        // dd(request()->get('date'));
        
        //Logic Saat pencet Sidebar
        if($date = request()->get('date')){
            $isMonth = false;
            $isCompleted = $isCompleted->whereDate('created_at', $date);
            $isInProgress = $isInProgress->whereDate('created_at', $date);

            $data = Task::where('user_id', auth()->user()->id)->whereDate('created_at', $date);
            $dt = Carbon::createFromFormat('Y-m-d', $date);   
            $dates = array();    
            for($i=1; $i < $dt->daysInMonth + 1; ++$i) {
                $dates[] = \Carbon\Carbon::createFromDate($dt->year, $dt->month, $i);
            }
            
            if(request()->get('date') != null) {
                session(['date' => $date]);
            }
        }

        $data = $data->get();
        foreach($data as $Data) {
            $Data->approval_status = Approval::where('task_id', $Data->id)->value('approval_status');
        }

        $mood = Mood::where('user_id', auth()->user()->id)->where('created_date', $date)->count();
        if ($mood > 0) {
            $isMoodEmpty = false;
            $mood = Mood::where('user_id', auth()->user()->id)->where('created_date', $date)->value('mood');
            $this->fragment['mood'] = $mood;
        }

        //logic time all task 
        $isCompleted = $isCompleted->count();
        $isInProgress = $isInProgress->count();

        // Yesterday in progress reminder
        if(request()->get('date') == null) {
            $yesterday = date('Y-m-d', strtotime("-1 days"));
        } else {
            $date = strtotime(request()->get('date'));
            $yesterday = date('Y-m-d', strtotime("-1 day", $date));
        }
        
        $Reminders = Task::whereIn('id', function($query)use($yesterday) {
            $query->select('task_id')->from('reminders')->whereDate('time_created', $yesterday)->where('reminder', 'true')->where('user_id', auth()->user()->id);
        })->get();
        $countReminders = Reminder::whereDate('time_created', $yesterday)->where('reminder', 'true')->where('user_id', auth()->user()->id)->count();

        foreach($Reminders as $rem) {
            $rem->project_id = Project::where('id', $rem->project_id)->value('name');
            $rem->site_id = Site::where('id', $rem->site_id)->value('name');
        }

        $this->fragment['isCompleted'] = $isCompleted;
        $this->fragment['isInProgress'] = $isInProgress;
        $this->fragment['dates'] = $dates;
        $this->fragment['today'] = $today;
        $this->fragment['tasks'] = $data;
        $this->fragment['isMonth'] = $isMonth;
        $this->fragment['isMoodEmpty'] = $isMoodEmpty;
        $this->fragment['Reminders'] = $Reminders;
        $this->fragment['countReminders'] = $countReminders;

        /*** View ***/
        return view('Employee.tasks.alltask', $this->fragment);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        /*** Area for adding CSS ***/
        $this->fragment['mainapp']['css'] = [
            "text/css,stylesheet,".url("plugins/select2/css/select2.min.css"),
            "text/css,stylesheet,".url("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"),
            "text/css,stylesheet,".url("plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"),
        ];
        /*** Area for adding JS ***/
        $this->fragment['mainapp']['js'] = [
            url("plugins/select2/js/select2.full.min.js"),
            url("plugins/moment/moment.min.js"),
            url("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"),
            url("js/employee/newtask.js"),
        ];
        /*** Area for adding DB ***/
        $date = $request->get('date');
        // dd($request);
        $today = today(); 
        $dates = []; 
        for($i=1; $i < $today->daysInMonth + 1; ++$i) {
            $dates[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i);
        }
        $this->fragment['dates'] = $dates;
        $this->fragment['today'] = $today;
        $this->fragment['projects'] = Project::all();
        $this->fragment['daily_tasks'] = DailyTask::where('department_id', auth()->user()->department_id)->get();
        $this->fragment['type_of_works'] = TypeOfWork::all();
        $this->fragment['transportations'] = Transportation::all();
        $this->fragment['sites'] = Site::all();
        
        /*** View ***/
        if($request->get('type') == 'day-off'){
            $this->fragment['absences'] = Absence::all();
            return view('employee.tasks.day-off', $this->fragment);
        }
        
        return view('employee.tasks.index', $this->fragment);
    }

    public function dependentDropdown(Request $request) {
        $daily_task = DailyTask::where('department_id', auth()->user()->department_id)->where('project_id', $request->get('id'))->pluck('name', 'id');
        
        return response()->json($daily_task);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = $request->date;
        
        // --Day off-- //
        if($request->absence_id != null){
            $validatedData = $request->validate([
                'absence_id' => 'required',
            ]);

            if($request->absence_id == 4){
                $validatedData = $request->validate([ 
                    'absence_remark' => 'required',
                ]);
            }

            if($request->absence_remark != null){
                $validatedData['absence_remark'] = $request->absence_remark;
                if($request->absence_id == 4){
                    $absenceName = "Other, please mention in Absence Remark";
                } else {
                    $absenceName = Absence::where('id', $request->absence_id)->first()->name;
                }
            } else {
                $absenceName = Absence::where('id', $request->absence_id)->first()->name;
            }
            
            if($request->absence_description != null){
                $validatedData['absence_description'] = $request->absence_description;
            }

            // $absenceName = Absence::where('id',$request->absence_id)->first()->name;
            $createdAt = Carbon::parse($request->date);
            
            $validatedData['absence_id'] = $request->absence_id;
            $validatedData['created_at'] = $createdAt;
            $validatedData['updated_at'] = $createdAt;
            $validatedData['name'] = $absenceName;
            $validatedData['user_id'] = auth()->user()->id;

            $task = Task::create($validatedData);
            $users = User::where('role_id', '>' ,'1')->get();

            /*================Approval Create Logic===============*/
            foreach($users as $user){
                if($user->role_id == 3){
                    Approval::create([
                        'user_id' => $user->id,
                        'task_id' => $task->id
                    ]);
                }
            }

            return redirect('/dashboard/task?date=' .  $date);
        }
        // --Day off-- //
        
        $validatedData = $request->validate([
            'start_time' => 'required',
            'finish_time' => 'required',
            'project_id' => 'required',
            'daily_task_id' => 'required',
            'type_of_work_id' => 'required',
            'transportation_id' => 'required',
            'site_id' => 'required',
            'task_status' => 'required',
        ]);

        if($request->daily_task_id == 0){
            $validatedData = $request->validate([ 
                'daily_task_remark' => 'required',
            ]);
        }
        if($request->site_id == 0){
            $validatedData = $request->validate([
                'site_remark' => 'required',
            ]);
        }
        if($request->project_id == 0){
            $validatedData = $request->validate([
                'project_remark' => 'required',
            ]);
        }

        if($request->daily_task_remark != null){
            $validatedData['daily_task_remark'] = $request->daily_task_remark;
            if($request->daily_task_id == 0) {
                $dailyTask = "Other, please mention in Task Remark";
            } else {
                $dailyTask = DailyTask::where('id', $request->daily_task_id)->first()->name;
            }
        } else {
            $dailyTask = DailyTask::where('id', $request->daily_task_id)->first()->name;
        }

        if($request->site_remark != null){
            $validatedData['site_remark'] = $request->site_remark;
        }

        if($request->project_remark != null){
            $validatedData['project_remark'] = $request->project_remark;
        }

        if($request->note != null){
            $validatedData['note'] = $request->note;
        }

        // $dailyTask = DailyTask::where('id',$request->daily_task_id)->first()->name;
        $startTime = Carbon::parse($request->start_time);
        $finishTime = Carbon::parse($request->finish_time);
        $totalTime = $startTime->diff($finishTime)->format('%H:%I:%S');
        $createdAt = Carbon::parse($request->date);

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['project_id'] = $request->project_id;
        $validatedData['daily_task_id'] = $request->daily_task_id;
        $validatedData['type_of_work_id'] = $request->type_of_work_id;
        $validatedData['transportation_id'] = $request->transportation_id;
        $validatedData['site_id'] = $request->site_id;
        $validatedData['name'] = $dailyTask;
        $validatedData['start_time'] = $startTime;
        $validatedData['finish_time'] = $finishTime;
        $validatedData['duration'] = $totalTime;
        $validatedData['task_status'] = $request->task_status;
        $validatedData['created_at'] = $createdAt;
        $validatedData['updated_at'] = $createdAt;

        $task = Task::create($validatedData);
        $users = User::where('role_id', '>' ,'1')->get();

        /*================Approval Create Logic===============*/
        // foreach($users as $user){
        //     if($user->role_id == 3 || ($user->role_id == 2 && $user->department_id == auth()->user()->department_id)){
        //         Approval::create([
        //             'user_id' => $user->id,
        //             'task_id' => $task->id
        //         ]);
        //     }
        // }

        /*================Approval Create Logic===============*/
        foreach($users as $user){
            if($user->role_id == 3){
                Approval::create([
                    'user_id' => $user->id,
                    'task_id' => $task->id
                ]);
            }
        }

        if($validatedData['task_status'] == 'In Progress') {
            $reminder = [
                'user_id' => auth()->user()->id,
                'task_id' => $task->id,
                'time_created' => $createdAt
            ];

            Reminder::create($reminder);
        }

        return redirect('/dashboard/task?date=' .  $date)->with('added', 'New Task has been added!');

        // return redirect('/dashboard/task')->with('added', 'New Task has been added!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tasks = Task::where('id', $id)->get();
        foreach ($tasks as $task) {
            $project = Project::where('id', $task->project_id)->value('name');
            if($task->project_id == 0){
                $project = "Other, please mention in Project Remark";
            } else {
                $project = Project::where('id', $task->project_id)->value('name');
            }

            if($task->site_id == 0){
                $site = "Other, please mention in Site Remark";
            } else {
                $site = Site::where('id', $task->site_id)->value('name');
            }
            
            $transportation = Transportation::where('id', $task->transportation_id)->value('name');
            $typeOfWork = TypeOfWork::where('id', $task->type_of_work_id)->value('name');
            $approval = Approval::where('task_id', $task->id)->value('approval_status');
        }
        
        $this->fragment['project'] = $project;
        $this->fragment['site'] = $site;
        $this->fragment['transportation'] = $transportation;
        $this->fragment['type_of_work'] = $typeOfWork;
        $this->fragment['approval'] = $approval;
        $this->fragment['tasks'] = $tasks;

        return view('employee.tasks.detail-task', $this->fragment);
    }

    public function showDayOff($id) 
    {
        $tasks = Task::where('id', $id)->get();
        dd($tasks);
        foreach ($tasks as $task) {
            $project = Project::where('id', $task->project_id)->value('name');
            $site = Site::where('id', $task->site_id)->value('name');
            $transportation = Transportation::where('id', $task->transportation_id)->value('name');
            $typeOfWork = TypeOfWork::where('id', $task->type_of_work_id)->value('name');
            $approval = Approval::where('task_id', $task->id)->value('approval_status');
        }
        
        $this->fragment['project'] = $project;
        $this->fragment['site'] = $site;
        $this->fragment['transportation'] = $transportation;
        $this->fragment['type_of_work'] = $typeOfWork;
        $this->fragment['approval'] = $approval;
        $this->fragment['Task'] = $tasks;

        return view('employee.tasks.detail-dayoff', $this->fragment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $date = request()->post('date');
        $id = request()->post('id');
        Task::destroy($id);

        return redirect('/dashboard/task?date=' .  $date)->with('deleted', 'Task has been deleted!');
    }

    public function dismiss() 
    {
        $Reminder = Reminder::where('time_created', request()->post('date'))->get();

        foreach($Reminder as $reminder) {
            if($reminder->reminder == 'true') {
                $reminder->reminder = 'false';
                $reminder->update();
            }
        }

        return redirect()->back();
    }
}
