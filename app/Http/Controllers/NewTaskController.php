<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewTaskController extends Controller
{
    public function index(){
        /*** Check Session ***/
        if ( session('user') ){ return redirect("login");}
        /*** Area for adding CSS ***/
        $this->fragment['site']['css'] = [
            "text/css,stylesheet,".url("plugins/select2/css/select2.min.css"),
            "text/css,stylesheet,".url("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"),
        ];
        /*** Area for adding JS ***/
        $this->fragment['site']['js'] = [
            url("plugins/select2/js/select2.full.min.js"),
            url("js/employee/newtask.js"),
        ];
        /*** View ***/
        return view("employee.tasks.index", $this->fragment);
    }
}
