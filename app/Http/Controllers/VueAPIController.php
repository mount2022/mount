<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Models\Notification;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class VueAPIController extends Controller
{
    public function notification() {
        $UserId = request()->get('id');
        $datas = Notification::where('receiver_id', $UserId)->where('status', 'Unread')->get();

        foreach($datas as $data) {
            $message[] = [
                'id' => $data->id,
                'sender_id' => $data->sender_id,
                'receiver_id' => $data->receiver_id,
                'task_id' => $data->task_id,
                'title' => $data->title,
                'body' => $data->body,
                'time_created' => $data->time_created,
                'status' => $data->status
            ];
        }

        return response()->json($datas);
    }

    public function markAsRead() {
        $id = request()->get('id');
        $datas = Notification::where('id', $id)->get();
        
        foreach($datas as $data) {
            $data->status = 'Read';
            $data->update();
        }

        return response()->json($datas);
    }
}
