<?php

namespace App\Exports;

use App\Models\Task;
use App\Models\Department;
use App\Models\User;
use App\Models\Project;
use App\Models\Site;
use App\Models\DailyTask;
use App\Models\Transportation;
use App\Models\TypeOfWork;
use App\Models\Notification;
use App\Models\Absence;
use App\Models\Approval;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Carbon\Carbon;

class TaskExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $id;
    protected $month;
    protected $year;

    function __construct($id, $month, $year) {
        $this->id = $id;
        $this->month = $month;
        $this->year = $year;
    }

    public function view(): View
    {
        $total = 0;
        $totalMinutes = 0;

        $tasks = Task::where('user_id', $this->id)->whereMonth('created_at', $this->month)->whereYear('created_at', $this->year)->get();
        $days = Task::distinct('created_at')->whereMonth('created_at', $this->month)->whereYear('created_at', $this->year)->where('user_id', $this->id)->whereNull('absence_id')->count();

        foreach($tasks as $task) {
            $task->dep = User::where('id', $task->user_id)->value('department_id');
            $task->dep = Department::where('id', $task->dep)->value('name');
            $task->approval_status = Approval::where('task_id', $task->id)->value('approval_status');
            $task->id = User::where('id', $task->user_id)->value('id');
            $task->site_id = Site::where('id', $task->site_id)->value('name');
            $task->user_id = User::where('id', $task->user_id)->value('name');
            $task->project_id = Project::where('id', $task->project_id)->value('name');
            $task->absence_id = Absence::where('id', $task->absence_id)->value('name');
            $task->daily_task_id = DailyTask::where('id', $task->daily_task_id)->value('name');
            $task->type_of_work_id = TypeOfWork::where('id', $task->type_of_work_id)->value('name');
            $task->transportation_id = Transportation::where('id', $task->transportation_id)->value('name');	

            if ($task->absence_id == null) {
                $start = new \Carbon\Carbon($task->start_time);
                $end   = new \Carbon\Carbon($task->finish_time);
                $minutesToday  = $start->diffInMinutes($end);
                $totalMinutes += $minutesToday;

                $now = \Carbon\Carbon::now();
				$then = $now->copy()->addMinutes($totalMinutes);
				$hours = $now->diffInHours($then);
				$totalMinutes = $now->diffInMinutes($then) - ($hours * 60);

				$total += $hours;
            }
        }

        return view('exports.tasks', [
            'tasks' => $tasks,
            'working_days' => $days,
            'hours' => $total
        ]);
    }
}