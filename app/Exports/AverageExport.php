<?php

namespace App\Exports;

use App\Models\Task;
use App\Models\User;
use App\Models\Department;

use Carbon\Carbon;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class AverageExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $month;
    protected $year;
    protected $department;

    function __construct($month, $year, $department) {
        $this->month = $month;
        $this->year = $year;
        $this->department = $department;
    }

    public function view(): View
    {   
        $User = User::where('department_id', $this->department)->get();
        $userCount = User::where('department_id', $this->department)->count();
        
        foreach($User as $user) {
            $user->department_id = Department::where('id', $user->department_id)->value('name');
            $user->task = Task::where('user_id', $user->id)->whereMonth('created_at', $this->month)->whereYear('created_at', $this->year)->get();

            $totalMinutes = 0;
            $workingDays = 0;

            foreach($user->task as $task) {
                if ($task->absence_id == null) {
                    $start = new \Carbon\Carbon($task->start_time);
                    $end   = new \Carbon\Carbon($task->finish_time);
                    $minute  = $start->diffInMinutes($end);
                    $totalMinutes += $minute;
                    $workingDays++;
                }
            }

            $user->hour = floor($totalMinutes / 60);
            $user->days = $workingDays;
            if($user->days != 0 && $user->hour != 0) {
                $user->average = floor($user->hour / $user->days);
            } else {
                $user->average = 0;
            }
        }

        $averageHour = 0;

        foreach($User as $user) {
            $averageHour += $user->average;
        }

        if($averageHour != 0 && $userCount != 0) {
            $totalAverage = $averageHour / $userCount;
        } else {
            $totalAverage = 0;
        }

        $Date = Carbon::createFromDate($this->year, $this->month, 1);

        return view('exports.average', [
            'User' => $User,
            'totalAverage' => $totalAverage,
            'Date' => $Date
        ]);
    }
}
