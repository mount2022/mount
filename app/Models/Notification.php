<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = ['id'];
    use HasFactory;

    // public function sender(){

    //     return $this->belongsTo(User::class);
    // }

    // public function receiver(){

    //     return $this->belongsTo(User::class);
    // }

    // public function task(){

    //     return $this->belongsTo(Task::class);
    // }
}
