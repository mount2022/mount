<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DailyTask extends Model
{
    protected $guarded = ['id'];
    use HasFactory;

    public function task(){

        return $this->hasMany(Task::class);
    }

    public function project(){

        return $this->belongsTo(Project::class);
    }

    public function department(){

        return $this->belongsTo(Department::class);
    }
}
