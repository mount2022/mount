<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefaultKPI extends Model
{
    use HasFactory;
    public $table = "default_kpis";
    protected $guarded = ['id'];
    
    public function user(){

        return $this->belongsTo(User::class);
    }
}
