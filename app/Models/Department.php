<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = ['id'];
    use HasFactory;

    public function user(){

        return $this->hasMany(User::class);
    }

    public function daily_task(){

        return $this->hasMany(DailyTask::class);
    }
}
