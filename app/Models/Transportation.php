<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
    protected $guarded = ['id'];
    use HasFactory;

    public function task(){

        return $this->hasMany(Task::class);
    }
}


