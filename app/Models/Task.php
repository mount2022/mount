<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = ['id'];
    use HasFactory;

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function project(){

        return $this->belongsTo(Project::class);
    }

    public function daily_task(){

        return $this->belongsTo(DailyTask::class);
    }

    public function type_of_work(){

        return $this->belongsTo(TypeOfWork::class);
    }

    public function site(){

        return $this->belongsTo(Site::class);
    }

    public function transportation(){

        return $this->belongsTo(Transportation::class);
    }

    public function absence(){

        return $this->belongsTo(Absence::class);
    }

    public function approvals(){

        return $this->hasMany(Approval::class);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }
}
