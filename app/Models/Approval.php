<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $guarded = ['id'];
    use HasFactory;

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function task(){

        return $this->belongsTo(Task::class);
    }

    
}
