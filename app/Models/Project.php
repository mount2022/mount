<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = ['id'];
    use HasFactory;

    public function daily_task(){
        return $this->hasMany(DailyTask::class);
    }

    public function task(){
        return $this->hasMany(Task::class);
    }
}
