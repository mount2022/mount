@extends('layouts-admin-w-sidebar.main')
@section('content')
    <style>
        .dot {
            height: 10px;
            width: 10px;
            background-color: #42A2D0;
            border-radius: 100%;
            display: inline-block;
        }
        .small-dot {
            height: 5px;
            width: 5px;
            margin-left: 2px;
            margin-right: 2px;
            background-color: #42A2D0;
            border-radius: 100%;
            display: inline-block;
        }
        .newtask{
            background-color: #4107BB;
            border: none;
            border-radius: 8px;
        }
        .newtask:hover{
            opacity: 80%;
            background-color: #4107BB;
        }
        .dayoff{
            background-color: #FF6060;
            border: none;
            border-radius: 8px;
        }
        .dayoff:hover{
            opacity: 80%;
            background-color: #FF6060;
        }
        .submit{
            background-color: #00DFC7;
            border: none;
            border-radius: 8px;
        }
        .submit:hover{
            opacity: 80%;
            background-color: #00DFC7;
        }
        .task:hover{
            opacity: 80%;
            background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%);
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #ffffff;
            margin-top: 5%;
            margin-left: 30%;
            padding: 20px;
            border: 1px solid #888;
            width: 55%;
            border-radius: 16px;
            box-shadow: 0px 2px 40px rgba(0, 0, 0, 0.15);
        }

        /* The Close Button */
        .close {
            margin-left: 95%;
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover, .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .radiogroupAboveLabel label{
            display:inline-block;
            text-align:center;
            vertical-align: text-top;
            margin:0 1em;
            color:#A8A8A8;
            font-size: 16px;
            font-weight: 400
        }

        .radiogroupAboveLabel label input[type="radio"] {
            display:block;
            margin:0.75em auto;
        }
        .alert.alert-success{
            color: white;
            margin-left: 40%;
            margin-top: 1%;
            margin-bottom: -1%;
            width: 20%;
            border-radius: 8px;
            text-align: center;
            background-color: #00DFC7;
            border: none;
        }

        .maxChar {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 500px;
        }
    </style>

    <nav class="navbar" style="background-color: #F5F4FF; box-shadow:unset;">
        <div class="container" style="height: 40px; margin-left: 55px;">
            <div class="col-sm-3" style="margin-top: 10px; margin-right: -30px;">
                @php
                    $picture = App\Models\User::where('id', $user[0]->id)->value('picture');
                    $path = url('user/images/' . $picture);
                @endphp
                <img src="{{ $path }}" width="40" height="40" class="rounded-circle">							
                <div style="margin-top: -45px; margin-left: 55px;">
                <h5 style="color: #1F1F1F;">{{ $user[0]->name }}</h5>
                <p style="color: #A8A8A8; margin-top: -7px;">{{ $user[0]->employee_id }} <span class="small-dot" style="background-color: #A8A8A8;"></span> {{ $department[0]['name'] }}</p>
                </div>
            </div>

            @php
                $filled = 0;
                $days = 0;
                
                foreach ($dates as $date){
                    $days = $days + 1;
                    if (\App\Models\Task::where('user_id', $user[0]->id)->whereDate('created_at', $date)->whereNull('absence_id')->count() > 0 ){
                        $filled = $filled + 1;
                    }
                }

                function formatMinutes($minutes) {
                    $now = \Carbon\Carbon::now();
                    $then = $now->copy()->addMinutes($minutes);
                    $hours = $now->diffInHours($then);
                    $minutes = $now->diffInMinutes($then) - ($hours * 60);

                    return \sprintf('%dh %02dmin', $hours, $minutes);
                }

                $totalMinutes = 0;

                foreach ($tasks as $task) {
                    if ($task->absence_id == null) {
                    $start = new \Carbon\Carbon($task->start_time);
                    $end   = new \Carbon\Carbon($task->finish_time);
                    $minutesToday  = $start->diffInMinutes($end);
                    $totalMinutes += $minutesToday;
                    
                    }
                }
            @endphp

            <ul class="nav justify-content-end" style="margin-right: -17px;">
                <li style="margin-right: 60px; margin-top:-12px">
                    <div class="row" style="font-size: 10px; color: #4107BB;">
                        Filled working days
                    </div>
                    <div class="row" style="font-size: 20px; margin-top: -2%; color: #4107BB;">
                        <!-- <span style="color: #4107BB; margin-left: -9%;">3</span> <span style="color: #A8A8A8; margin-left: -10%;">/ 21 days</span> -->
                        {{ $filled }} / {{ $days }} days
                    </div>
                </li>
                <li style="margin-right: 50px; margin-top:-12px; color: #4107BB;">
                    <div class="row" style="font-size: 10px;">
                        @if ($isMonth == true)
                            Total working time this month
                        @else
                            Total working time this day
                        @endif
                    </div>
                    <div class="row" style="font-size: 20px; margin-top: -2%">
                        {{ formatMinutes($totalMinutes) }}
                    </div>
                </li>
                <li style="margin-right: 20px; margin-top:-2%; font-weight: 500; font-size: 20px; color: #4107BB;">
                    {{ Carbon\Carbon::parse($dates[0])->format('F Y') }}
                </li>
            </ul>
        </div>
    </nav>

    @if (session()->has('added'))
        <div class="alert alert-success" role="alert">
            {{ session('added') }}
        </div>
    @elseif (session()->has('deleted'))
        <div class="alert alert-success" style="background-color: #FF6060;" role="alert">
            {{ session('deleted') }}
        </div>
    @endif
    
    <div class="container" style="padding-top: 2%;">
        <div class="row">
            @if (request()->get('date') == null)
                <h1>All Task(s) in {{ Carbon\Carbon::parse($dates[0])->format('F Y') }}</h1>
            @else
                <h1>All Task(s) in {{ Carbon\Carbon::parse(request()->get('date'))->format('l, d F Y') }}</h1>
            @endif

            <div class="col-sm-2">
                @if ($isMonth == true)
                    <p style="color: #A8A8A8;">This month's record:</p>
                @else
                    <p style="color: #A8A8A8;">This day's record:</p>
                @endif
            </div>

            <div class="col-sm-3" style="margin-left: -4%;">
                <span class="dot"></span> <p style="color: #1F1F1F; margin-left: 7%; margin-top: -8%;">Task Completed : {{ $isCompleted }}</p> 
            </div>

            <div class="col-sm-3" style="margin-left: -8%;">
                <span class="dot" style="background-color: #FCA010;"></span> <p style="color: #1F1F1F; margin-left: 7%; margin-top: -8%;">Task in progress : {{ $isInProgress }}</p> 
            </div>
            
            @if ($isMoodEmpty != true)
                @foreach ($mood as $m)
                    <div class="col-sm-3 " style="align: right; margin-left: 20%;">
                        <span class="dot" style="background-color: #FCA010; visibility: hidden;"></span> <p style="color: #1F1F1F; margin-left: 7%; margin-top: -8%;">Mood Rate : {{ $m->mood }}</p> 
                    </div>
                @endforeach
            @endif
        </div>

        @php
            date_default_timezone_set('Asia/Jakarta');
        @endphp

        {{-- Unlock Button --}}
        <ul class="nav justify-content">    
            @if (request()->get('date') == date('Y-m-d') || request()->get('date') == now()->subDay(1)->format('Y-m-d') || date('Y-m-d') <= request()->get('date') || $isMonth == true) @else
                @if (\App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', session('user_id'))->count() <= 0)
                    <form action="/dashboard-admin/unlock" method="POST">
                        @csrf
                        <input type="hidden" name="userId" value="{{ $user[0]->id }}">
                        <input type="hidden" name="date" value="{{ request()->get('date') }}">
                        <input type="hidden" name="id" value="{{ session('user_id') }}">
                        <li>
                            <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                            style="background-color: #00DFC7; color: #FFFFFF; border-color: #00DFC7; border-radius: 8px;">Unlock For 24 Hour</button>
                        </li>
                    </form>
                @else
                    @if (date('Y-m-d H:i:s') <= \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', session('user_id'))->latest()->first()->end)
                        @php
                            $until = \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', session('user_id'))->latest()->first()->end;
                            $until = \Carbon\Carbon::parse($until);
                        @endphp  
                        <div class="col" style="width: 100%">
                            <h5>Unlock until : {{ $until->format('d F Y - H:i') }}</h5>
                        </div>
                    @else
                        <form action="/dashboard-admin/unlock" method="POST">
                            @csrf
                            <input type="hidden" name="userId" value="{{ $user[0]->id }}">
                            <input type="hidden" name="date" value="{{ request()->get('date') }}">
                            <input type="hidden" name="id" value="{{ session('user_id') }}">
                            <li>
                                <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                                style="background-color: #00DFC7; color: #FFFFFF; border-color: #00DFC7; border-radius: 8px;">Unlock For 24 Hour</button>
                            </li>
                        </form>
                    @endif
                    
                @endif
            @endif
        </ul>
        {{-- Unlock Button --}}

        @php 
            $isUndo = true; 
        @endphp
    
        @foreach ($tasks as $task)
            @if ($task->absence_id == null)
                {{-- Tasks --}}
                <a href="/dashboard/task/admin/{{ $task->id }}" style="text-decoration: none;">
                    <div class="container" >
                        <div class="row" style="margin-top: 2%;">
                            <div class="card task" style="background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%); box-shadow:none; color:white; border-radius: 16px; border: none;">
                                <div class="card-body" style="display: flex;">
                                    <div class="col-sm-5">
                                        @if ($task->daily_task_id == 0)
                                            <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->daily_task_remark }}</p>
                                        @else
                                            <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->name }}</p>
                                        @endif
                                        @php
                                            $start = \Carbon\Carbon::parse($task->start_time)->format('H:i');
                                            $finish = \Carbon\Carbon::parse($task->finish_time)->format('H:i');
                                        @endphp
                                        <p style="margin-top: -15px; font-weight: 300;"><em>{{ $task->created_at->format('d F Y') }}, {{ $start }} - {{ $finish }}</em></p>
                                    </div>
                    
                                    <div class="col" style="text-align: center;">
                                        @if ($task->task_status == 'Completed')
                                            <div class="card p-2" style="border: none; box-shadow: none; background-color: #FFFFFF; color: #42A2D0; margin-top: 25px; margin-left: 240px; width: 28%;">
                                                {{ $task->task_status }}
                                            </div>
                                        @else
                                            <div class="card p-2" style="border: none; box-shadow: none; background-color: #FFFFFF; color: #FCA010; margin-top: 25px; margin-left: 240px; width: 28%;">
                                                {{ $task->task_status }}
                                            </div>
                                        @endif

                                        {{-- Approval Status --}}
                                        @if ($task->approval_status != null)
                                            @if ($task->approval_status == "Not Approved")
                                                <div class="card p-2" style="font-size: 13px;border: none; box-shadow: none; background-color: #FFFFFF; 
                                                color: #FE6060; margin-top: 25px; margin-left: 240px; width: 28%;">
                                                    {{ $task->approval_status }}
                                                </div>
                                            @else
                                                <div class="card p-2" style="font-size: 14px;border: none; box-shadow: none; background-color: #FFFFFF; 
                                                color: #42A2D0; margin-top: 25px; width: 28%; margin-left:240px;">
                                                    {{ $task->approval_status }}
                                                </div>
                                            @endif
                                        @endif
                                        {{-- Approval Status --}}
                                    </div>

                                    
                                    <div class="col-sm-4" style="margin-right: -205px;">
                                        <p style="margin-top: 12px; margin-bottom: -3%; font-weight: 200;">Total time</p>
                                        <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ Carbon\Carbon::parse($task->duration)->format('H') }}h {{ Carbon\Carbon::parse($task->duration)->format('i') }}min</p>
                                    </div>
                                    
                                    {{-- <div class="col-sm-1" style="margin-top: 20px;">
                                        <form action="/dashboard/task/{{ $task->name }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button button type="submit" class="btn" onclick="return confirm('Are you sure?')" style="background-color: none; border: none; color: #ffffff;">
                                                <img src="/image/delete.png" alt="search">
                                            </button>
                                        </form>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- Tasks --}}
            @else
                {{-- Day Off --}}
                <a href="/dashboard/task/admin/off/{{ $task->id }}" style="text-decoration: none;">
                    <div class="container" >
                        <div class="row" style="margin-top: 2%;">
                            <div class="card task" style="background: linear-gradient(270deg, #FE6060 0%, #EF929E 100%); box-shadow:none; color:white; border-radius: 16px; border: none;">
                                <div class="card-body" style="display: flex;">
                                    <div class="col-sm-5">
                                        @if ($task->absence_id == 4)
                                            <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->absence_remark }}</p>
                                        @else
                                            <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->name }}</p>
                                        @endif
                                        <p style="margin-top: -15px; font-weight: 200;"><em>{{ $task->created_at->format('d F Y') }}</em></p>
                                    </div>

                                    {{-- Approval Status --}}
                                    <div class="col-sm-3" style="text-align: center;">
                                        @if ($task->approval_status != null)
                                            @if ($task->approval_status == "Not Approved")
                                                <div class="card p-2" style="font-size: 14px;border: none; box-shadow: none; background-color: #FFFFFF; 
                                                color: #FE6060; margin-top: 25px; width: 40%; margin-left:240px;">
                                                    {{ $task->approval_status }}
                                                </div>
                                            @else
                                                <div class="card p-2" style="font-size: 14px;border: none; box-shadow: none; background-color: #FFFFFF; 
                                                color: #42A2D0; margin-top: 25px; width: 40%; margin-left:240px;">
                                                    {{ $task->approval_status }}
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                    {{-- Approval Status --}}

                                    <div class="col-sm-4" style="margin-right: -210px; margin-left: 200px;">
                                        <p style="margin-top: 12px; margin-bottom: -3%; font-weight: 200;">Total time</p>
                                        <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">OFF</p>
                                    </div>

                                    {{-- <div class="col-sm-1" style="margin-top: 20px;">
                                        <form action="/dashboard/task/{{ $task->name }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button button type="submit" class="btn" style="background-color: none; border: none; color: #ffffff;">
                                                <img src="/image/delete.png" alt="search">
                                            </button>
                                        </form>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- Day Off --}}
            @endif
            
            @php
                if ($task->approval_status == 'Not Approved') { 
                    $isUndo = false;
                }
            @endphp
        @endforeach

        <ul class="nav justify-content-end">
            @if ($tasks->isEmpty() != true && auth()->user()->role_id == 3)
                @if ($isUndo == true)
                    <form action="/dashboard-admin/approval/undo" method="post">
                        @csrf
                        <input type="hidden" name="userId" value="{{ $user[0]->id }}">
                        <input type="hidden" name="month" value="{{ request()->get('month') }}">
                        <input type="hidden" name="year" value="{{ request()->get('year') }}">
                        <input type="hidden" name="date" value="{{ request()->get('date') }}"> 
                        <input type="hidden" name="id" value="{{ session('user_id') }}">
                        <li>
                            <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                            style="background-color: #ffffff; color: #A8A8A8; border-color: #A8A8A8; border-radius: 8px;">UNDO ACTION</a>
                        </li>&nbsp; &nbsp;
                    </form>
                @else
                    <form action="/dashboard-admin/approval/approve" method="post">
                        @csrf
                        <input type="hidden" name="userId" value="{{ $user[0]->id }}">
                        <input type="hidden" name="month" value="{{ request()->get('month') }}">
                        <input type="hidden" name="year" value="{{ request()->get('year') }}">
                        <input type="hidden" name="date" value="{{ request()->get('date') }}">
                        <input type="hidden" name="id" value="{{ session('user_id') }}">
                        <li>
                            <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                            style="background-color: #00DFC7; color: #FFFFFF; border-color: #00DFC7; border-radius: 8px;">Approve</button>
                        </li> &nbsp; &nbsp;
                    </form>

                    <form action="/dashboard-admin/approval/reject" method="post">
                        @csrf
                        <input type="hidden" name="userId" value="{{ $user[0]->id }}">
                        <input type="hidden" name="month" value="{{ request()->get('month') }}">
                        <input type="hidden" name="year" value="{{ request()->get('year') }}">
                        <input type="hidden" name="date" value="{{ request()->get('date') }}">
                        <input type="hidden" name="id" value="{{ session('user_id') }}">
                        <li>
                            <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                            style="background-color: #FF6060; color: #FFFFFF; border-color: #FF6060; border-radius: 8px; margin-left: 10%;">Reject</button>
                        </li>
                    </form>
                @endif
            @endif
        </ul>
    </div>

    <meta name="user-id" content="{{ auth()->user()->id }}">

@endsection