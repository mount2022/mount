@extends('layouts-admin.main')
@section('content')

    <style>
        .scrollDashboard {
            max-height: 495px;
            height: 495px;
            overflow: scroll;
            margin-bottom: 20px;
            max-width: 100%;
            overflow-x: hidden;
            overflow-y: auto;
        }
        .small-dot {
            height: 5px;
            width: 5px;
            margin-left: 1px;
            margin-right: 1px;
            border-radius: 100%;
            display: inline-block;
        }
        .modal-content{
            width: 55rem;
            border-radius: 16px;
            box-shadow: 0px 2px 40px 0px #00000026;
            border-color: white;
            padding: 35px;
        }
    </style>

    <div class="row">
        <div class="col mt-5" style="margin-left: 10%;">
            <h5 style="color: #4107BB"> Good day,</h5>
            <h2 style="color: #4107BB; font-weight: 600;"> {{ auth()->user()->name }}</h2>
        </div>
    </div>

    <div class="row">
        <div class="col" style="margin-left: 10%; margin-top: 3%;">
            <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px; width: 850px;">
                <div class="card-body" style="padding: 5%;">
                    <h4 style="font-weight: 500;">Today Recap</h4>
                    <h6 style="font-weight: 400; margin-bottom: 5%; color: #A8A8A8;">see people's recent progress</h6>

                    <div class="row" style="margin-left: 1px;">
                        <ul class="nav">
                            <li class="nav-item" style="margin-right: 8px;">
                                <a href="#ModalPeopleSubmitted" id="BtnPeopleSubmitted" style="text-decoration: none;">
                                    <div class="card-default" style="background: #F7F9FF; color:#1f1f1f; border-radius: 8px; width: 175px;">
                                        <div class="card-body" style="padding: 10%;">
                                            <img src="/image/submit.png" alt="submit" style="width: 20%; margin-bottom: 9px;">
                                            <h2 style="font-weight: 500;">{{ $PS }}</h2>
                                            <h6 style="font-weight: 400; margin-bottom: 5%; font-size: smaller;">people submitted</h6>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <!-- ModalPeopleSubmitted -->
                            <div id="ModalPeopleSubmitted" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/submitted.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">People who have submitted</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $PS }} People</h6>
                                        
                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($submitted as $submit)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $submit->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $submit->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $submit->employee_id }}</td>
                                                                <td>{{ $submit->department_id }}</td>
                                                                @php $number++; @endphp
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ModalPeopleSubmitted -->

                            <li class="nav-item" style="margin-right: 8px;">
                                <a href="#ModalPeopleNotSubmitted" id="BtnPeopleNotSubmitted" style="text-decoration: none;">
                                    <div class="card-default" style="background: #FE60601A; color:#1f1f1f; border-radius: 8px; width: 175px;">
                                        <div class="card-body" style="padding: 10%;">
                                            <img src="/image/notfilled.png" alt="submit" style="width: 20%; margin-bottom: 9px;">
                                            <h2 style="font-weight: 500;">{{ $HF }}</h2>
                                            <h6 style="font-weight: 400; margin-bottom: 5%; font-size: smaller;">people haven't filled</h6>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <!-- ModalPeopleNotSubmitted -->
                            <div id="ModalPeopleNotSubmitted" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/notsubmitted.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">People who haven't submitted</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $HF }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($notSubmitted as $notSubmit)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $notSubmit->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $notSubmit->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $notSubmit->employee_id }}</td>
                                                                <td>{{ $notSubmit->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ModalPeopleNotSubmitted -->
                            
                            <li class="nav-item" style="margin-right: 8px;">
                                <div class="card-default" style="background: #3CAFE71A; color:#1f1f1f; border-radius: 8px; width: 175px;">
                                    <div class="card-body" style="padding: 10%;">
                                        <img src="/image/complete.png" alt="submit" style="width: 20%; margin-bottom: 9px;">
                                        <h2 style="font-weight: 500;">{{ $TC }}</h2>
                                        <h6 style="font-weight: 400; margin-bottom: 5%; font-size: smaller;">tasks completed</h6>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="card-default" style="background: #FCA0101A; color:#1f1f1f; border-radius: 8px; width: 175px;">
                                    <div class="card-body" style="padding: 10%;">
                                        <img src="/image/progress.png" alt="submit" style="width: 20%; margin-bottom: 9px;">
                                        <h2 style="font-weight: 500;">{{ $TP }}</h2>
                                        <h6 style="font-weight: 400; margin-bottom: 5%; font-size: smaller;">tasks still in progress</h6>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                    <h5 style="font-weight: 500; margin-top: 5%;">Submitted tasks in {{ date('d F Y') }}</h5>
                    
                    <div class="scrollDashboard">
                        @if ($Task->isEmpty())
                            <div class="row" style="color: #A8A8A8; text-align:center;  padding: 150px 0;">
                                <h5 class="mt-4" style="margin-left: 3%; font-size: 18px;">No one has submitted yet</h5>
                            </div>
                        @else
                            @foreach ($Task->reverse() as $task)
                                @if (auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                                    <div style="margin-top: 30px; margin-bottom: 30px;">
                                        @php
                                            $path = url('user/images/' . $task->picture);
                                        @endphp
                                        <img src="{{ $path }}" width="70" height="70" class="rounded-circle">
                                        <div style="margin-top: -70px; margin-left: 95px;">
                                            <h6 style="color: #1F1F1F; margin-bottom: 5px; font-weight: 400;">{{ $task->task_date }}, {{ $task->start_time }}</h6>

                                            @if ($task->absence_id == null)
                                                @if ($task->name == "Other, please mention in Task Remark")
                                                    <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->daily_task_remark }}</h5>
                                                @else
                                                    <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->name }}</h5>
                                                @endif
                                            @else
                                                @if ($task->name == "Other, please mention in Absence Remark")
                                                <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->absence_remark }}</h5>
                                                @else
                                                    <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->name }}</h5>
                                                @endif
                                            @endif

                                            <ul class="nav">
                                                <li class="nav-item" style="margin-right: 12px;">
                                                    
                                                    @if ($task->absence_id != null)
                                                        <div class="card-default" style="background: #FE606033; color:#FE6060; border-radius: 4px;">
                                                            <div class="card-body" style="padding: 15px; padding-top: 8px; padding-bottom: 2px;">
                                                                <h6 style="font-weight: 400;">Day Off</h6>
                                                            </div>
                                                        </div>
                                                    @else
                                                        @if ($task->task_status == "Completed")
                                                            <div class="card-default" style="background: #3CAFE733; color:#3CAFE7; border-radius: 4px;">
                                                                <div class="card-body" style="padding: 15px; padding-top: 8px; padding-bottom: 2px;">
                                                                    <h6 style="font-weight: 400;">{{ $task->task_status }}</h6>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="card-default" style="background: #FCA01033 ; color:#FCA010; border-radius: 4px;">
                                                                <div class="card-body" style="padding: 15px; padding-top: 8px; padding-bottom: 2px;">
                                                                    <h6 style="font-weight: 400;">{{ $task->task_status }}</h6>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        
                                                    @endif

                                                </li>
                                                <li class="nav-item" style="margin-top: 5px; margin-right: 12px;">
                                                    <span style="color: #A8A8A8;"><img src="/image/people.png" style="margin-right: 10px;">{{ $task->user_name }}, {{ $task->department }}</span>
                                                </li>

                                                <span class="small-dot" style="margin-top: 15px; margin-right: 10px; background-color: #A8A8A8;"></span>

                                                @if ($task->absence_id != null)
                                                    <li class="nav-item" style="margin-top: 5px;">
                                                        <span style="color: #A8A8A8;"><img src="/image/clock.png" style="margin-right: 10px;">0 hour 0 minutes</span>
                                                    </li>
                                                @else
                                                    <li class="nav-item" style="margin-top: 5px;">
                                                        <span style="color: #A8A8A8;"><img src="/image/clock.png" style="margin-right: 10px;">{{ $task->hour }} hour {{ $task->minute }} minutes</span>
                                                    </li>
                                                @endif
                                                
                                            </ul>
                                        </div>
                                    </div>
                            
                                    <div class="dropdown-divider"></div>            
                                @elseif (auth()->user()->role_id != 3 && $task->department_id == auth()->user()->department_id)
                                    <div style="margin-top: 30px; margin-bottom: 30px;">
                                        @php
                                            $path = url('user/images/' . $task->picture);
                                        @endphp
                                        <img src="{{ $path }}" width="70" height="70" class="rounded-circle">
                                        <div style="margin-top: -70px; margin-left: 95px;">
                                            <h6 style="color: #1F1F1F; margin-bottom: 5px; font-weight: 400;">{{ $task->task_date }}, {{ $task->start_time }}</h6>
                                        
                                            @if ($task->absence_id == null)
                                                @if ($task->name == "Other, please mention in Task Remark")
                                                    <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->daily_task_remark }}</h5>
                                                @else
                                                    <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->name }}</h5>
                                                @endif
                                            @else
                                                @if ($task->name == "Other, please mention in Absence Remark")
                                                <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->absence_remark }}</h5>
                                                @else
                                                    <h5 style="color: #1F1F1F; font-weight: 400 ;">{{ $task->name }}</h5>
                                                @endif
                                            @endif

                                            <ul class="nav">
                                                <li class="nav-item" style="margin-right: 12px;">
                                                    
                                                    @if ($task->absence_id != null)
                                                        <div class="card-default" style="background: #FE606033; color:#FE6060; border-radius: 4px;">
                                                            <div class="card-body" style="padding: 15px; padding-top: 8px; padding-bottom: 2px;">
                                                                <h6 style="font-weight: 400;">Day Off</h6>
                                                            </div>
                                                        </div>
                                                    @else
                                                        @if ($task->task_status == "Completed")
                                                            <div class="card-default" style="background: #3CAFE733; color:#3CAFE7; border-radius: 4px;">
                                                                <div class="card-body" style="padding: 15px; padding-top: 8px; padding-bottom: 2px;">
                                                                    <h6 style="font-weight: 400;">{{ $task->task_status }}</h6>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="card-default" style="background: #FCA01033 ; color:#FCA010; border-radius: 4px;">
                                                                <div class="card-body" style="padding: 15px; padding-top: 8px; padding-bottom: 2px;">
                                                                    <h6 style="font-weight: 400;">{{ $task->task_status }}</h6>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        
                                                    @endif

                                                </li>
                                                <li class="nav-item" style="margin-top: 5px; margin-right: 12px;">
                                                    <span style="color: #A8A8A8;"><img src="/image/people.png" style="margin-right: 10px;">{{ $task->user_name }}, {{ $task->department }}</span>
                                                </li>

                                                <span class="small-dot" style="margin-top: 15px; margin-right: 10px; background-color: #A8A8A8;"></span>

                                                @if ($task->absence_id != null)
                                                    <li class="nav-item" style="margin-top: 5px;">
                                                        <span style="color: #A8A8A8;"><img src="/image/clock.png" style="margin-right: 10px;">0 hour 0 minutes</span>
                                                    </li>
                                                @else
                                                    <li class="nav-item" style="margin-top: 5px;">
                                                        <span style="color: #A8A8A8;"><img src="/image/clock.png" style="margin-right: 10px;">{{ $task->hour }} hour {{ $task->minute }} minutes</span>
                                                    </li>
                                                @endif
                                                
                                            </ul>
                                        </div>
                                    </div>
                            
                                    <div class="dropdown-divider"></div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col" style="margin-right: 10%; margin-top: 3%;">
            <div class="row justify-content-center">
                <div class="col">
                    <div class="card card-default border-0" style="background: #F7F9FF; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;">
                        <div class="card-body" style="padding: 10%;">
                            <h4 style="font-weight: 500; color: #4107BB;">Mood Meter</h4>
                            <h6 style="font-weight: 400; margin-bottom: 5%; color: #A8A8A8;">Retrieving data from {{ date('d F Y') }}</h6>
                            
                            <a href="#RGModal" id="BtnRG" style="text-decoration: none;">
                                <div>
                                    <img src="/image/really-great.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $RG }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400; text-decoration: none; color: #212529;">Really Great</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px; color: #212529!important;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px; text-decoration: none;">{{ $RG }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- Really Great -->
                            <div id="RGModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/really-great.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Really Great</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $RG }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($RGdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Really Great -->

                            <a href="#GRModal" id="BtnGR" style="text-decoration: none;">
                                <div>
                                    <img src="/image/great.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $GR }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400; color: #212529;">Great</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px; color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $GR }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- Great -->
                            <div id="GRModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/great.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Great</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $GR }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($GRdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Great -->

                            <a href="#VGModal" id="BtnVG" style="text-decoration: none;">
                                <div>
                                    <img src="/image/very-good.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $VG }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400; color: #212529;">Very Good</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px; color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $VG }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- Very Good -->
                            <div id="VGModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/very-good.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Very Good</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $VG }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($VGdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Very Good -->

                            <a href="#GDModal" id="BtnGD" style="text-decoration: none;">
                                <div>
                                    <img src="/image/good.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $GD }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400; color: #212529;">Good</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px; color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $GD }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- Good -->
                            <div id="GDModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/good.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Good</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $GD }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($GDdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Good -->

                            <a href="#OKModal" id="BtnOK" style="text-decoration: none;">
                                <div>
                                    <img src="/image/okay.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $OK }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400; color: #212529;">Okay</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px; color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $OK }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- Ok -->
                            <div id="OKModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/okay.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Okay</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $OK }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($OKdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Ok -->

                            <a href="#SOModal" id="BtnSO" style="text-decoration: none;">
                                <div>
                                    <img src="/image/so-so.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $SO }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400; color: #212529;">So-so</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px; color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $SO }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- SO -->
                            <div id="SOModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/so-so.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">So-so</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $SO }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($SOdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- SO -->

                            <a href="#MHModal" id="BtnMH" style="text-decoration: none;">
                                <div>
                                    <img src="/image/meh.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $MH }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400; color: #212529;">Meh</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px; color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $MH }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- MH -->
                            <div id="MHModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/meh.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Meh</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $MH }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($MHdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MH -->

                            <a href="#UWModal" id="BtnUW" style="text-decoration: none;">
                                <div>
                                    <img src="/image/unwell.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $UW }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400;color: #212529;">Unwell</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px;color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $UW }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- UW -->
                            <div id="UWModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/unwell.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Unwell</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $UW }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($UWdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- UW -->

                            <a href="#SCModal" id="BtnSC" style="text-decoration: none;">
                                <div>
                                    <img src="/image/sick.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $SC }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400;color: #212529;">Sick</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px;color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $SC }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- SC -->
                            <div id="SCModal" class="modal" style="background-color: rgba(0,0,0,0.4);">
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/sick.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Sick</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $SC }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($SCdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- SC -->

                            <a href="#CWModal" id="BtnCW" style="text-decoration: none;">
                                <div>
                                    <img src="/image/worst.png">
                                    <div class="progress" style="width: 90%; margin-top: -25px; margin-left: auto;">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $CW }}%; background: #4107BB;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <ul class="nav" style="margin-top: 15px; margin-left: 55px;">
                                        <li class="nav-item">
                                            <h6 style="font-weight: 400;color: #212529;">Couldn't be worse</h6>
                                        </li>
                                        <li class="nav-item" style="margin-left: auto; margin-top: -5px;color: #212529;">
                                            <span><img src="/image/mood-person.png" style="margin-right: 10px;">{{ $CW }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </a>

                            <!-- CW -->
                            <div id="CWModal" class="modal" style="background-color: rgba(0,0,0,0.4);">z
                                <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
                                    <div class="modal-content">
                                        <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
                                        <img class="mb-3" src="/image/worst.png" alt="" style="width:50px;">
                                        <h5 style="position: absolute; left: 100px; top: 60px;">Couldn't be worse</h5>
                                        <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">{{ $CW }} People</h6>

                                        <div class="row scrollDashboard">
                                            <div class="col-12">
                                                <table id="dtOrderExample" class="table table-borderless">
                                                    <thead>
                                                        <tr style="color: #A8A8A8;">
                                                            <th scope="col" style="font-weight: 400;">No</th>
                                                            <th scope="col" style="font-weight: 400;">Employee name</th>
                                                            <th scope="col" style="font-weight: 400;">Employee ID</th>
                                                            <th scope="col" style="font-weight: 400;">Department</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php $number = 1; @endphp
                                                        @foreach ($CWdata as $data)
                                                            <tr>
                                                                <th scope="row">{{ $number }}</th>
                                                                <td>
                                                                    <img src="{{ url('user/images/' . $data->picture) }}" width="40" height="40" class="rounded-circle">
                                                                    <div style="margin-top: -45px; margin-left: 55px;">
                                                                        <h6 style="color: #1F1F1F; margin-top: 15px; font-weight: 400;">{{ $data->name }}</h6>
                                                                    </div>
                                                                </td>
                                                                <td>{{ $data->employee_id }}</td>
                                                                <td>{{ $data->department_id }}</td>
                                                            </tr>
                                                            @php $number++; @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CW -->
                            
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <meta name="user-id" content="{{ auth()->user()->id }}">

    <script>
        /*-- ModalPeopleSubmitted --*/
        var modalPS = document.getElementById("ModalPeopleSubmitted");
        var btnPS = document.getElementById("BtnPeopleSubmitted");
        var spanPS = document.getElementsByClassName("close")[0];
        btnPS.onclick = function() {
            modalPS.style.display = "block";
        }
        spanPS.onclick = function() {
            modalPS.style.display = "none";
        }

        /*-- ModalPeopleNotSubmitted --*/
        var modalPNS = document.getElementById("ModalPeopleNotSubmitted");
        var btnPNS = document.getElementById("BtnPeopleNotSubmitted");
        var spanPNS = document.getElementsByClassName("close")[1];
        btnPNS.onclick = function() {
            modalPNS.style.display = "block";
        }
        spanPNS.onclick = function() {
            modalPNS.style.display = "none";
        }

        /*-- RGModal --*/
        var modalRG = document.getElementById("RGModal");
        var btnRG = document.getElementById("BtnRG");
        var spanRG = document.getElementsByClassName("close")[2];
        btnRG.onclick = function() {
            modalRG.style.display = "block";
        }
        spanRG.onclick = function() {
            modalRG.style.display = "none";
        }

        /*-- GRModal --*/
        var modalGR = document.getElementById("GRModal");
        var btnGR = document.getElementById("BtnGR");
        var spanGR = document.getElementsByClassName("close")[3];
        btnGR.onclick = function() {
            modalGR.style.display = "block";
        }
        spanGR.onclick = function() {
            modalGR.style.display = "none";
        }

        /*-- VGModal --*/
        var modalVG = document.getElementById("VGModal");
        var btnVG = document.getElementById("BtnVG");
        var spanVG = document.getElementsByClassName("close")[4];
        btnVG.onclick = function() {
            modalVG.style.display = "block";
        }
        spanVG.onclick = function() {
            modalVG.style.display = "none";
        }

        /*-- GDModal --*/
        var modalGD = document.getElementById("GDModal");
        var btnGD = document.getElementById("BtnGD");
        var spanGD = document.getElementsByClassName("close")[5];
        btnGD.onclick = function() {
            modalGD.style.display = "block";
        }
        spanGD.onclick = function() {
            modalGD.style.display = "none";
        }

        /*-- OKModal --*/
        var modalOK = document.getElementById("OKModal");
        var btnOK = document.getElementById("BtnOK");
        var spanOK = document.getElementsByClassName("close")[6];
        btnOK.onclick = function() {
            modalOK.style.display = "block";
        }
        spanOK.onclick = function() {
            modalOK.style.display = "none";
        }

        /*-- SOModal --*/
        var modalSO = document.getElementById("SOModal");
        var btnSO = document.getElementById("BtnSO");
        var spanSO = document.getElementsByClassName("close")[7];
        btnSO.onclick = function() {
            modalSO.style.display = "block";
        }
        spanSO.onclick = function() {
            modalSO.style.display = "none";
        }

        /*-- MHModal --*/
        var modalMH = document.getElementById("MHModal");
        var btnMH = document.getElementById("BtnMH");
        var spanMH = document.getElementsByClassName("close")[8];
        btnMH.onclick = function() {
            modalMH.style.display = "block";
        }
        spanMH.onclick = function() {
            modalMH.style.display = "none";
        }

        /*-- UWModal --*/
        var modalUW = document.getElementById("UWModal");
        var btnUW = document.getElementById("BtnUW");
        var spanUW = document.getElementsByClassName("close")[9];
        btnUW.onclick = function() {
            modalUW.style.display = "block";
        }
        spanUW.onclick = function() {
            modalUW.style.display = "none";
        }

        /*-- SCModal --*/
        var modalSC = document.getElementById("SCModal");
        var btnSC = document.getElementById("BtnSC");
        var spanSC = document.getElementsByClassName("close")[10];
        btnSC.onclick = function() {
            modalSC.style.display = "block";
        }
        spanSC.onclick = function() {
            modalSC.style.display = "none";
        }

        /*-- CWModal --*/
        var modalCW = document.getElementById("CWModal");
        var btnCW = document.getElementById("BtnCW");
        var spanCW = document.getElementsByClassName("close")[11];
        btnCW.onclick = function() {
            modalCW.style.display = "block";
        }
        spanCW.onclick = function() {
            modalCWC.style.display = "none";
        }

        /*-- Close Modal When Click on Page --*/
        window.onclick = function(event) {
            if (event.target == modalPS) {
                modalPS.style.display = "none";
            } else if (event.target == modalPNS) {
                modalPNS.style.display = "none";
            } else if (event.target == modalRG) {
                modalRG.style.display = "none";
            } else if (event.target == modalGR) {
                modalGR.style.display = "none";
            } else if (event.target == modalVG) {
                modalVG.style.display = "none";
            } else if (event.target == modalGD) {
                modalGD.style.display = "none";
            } else if (event.target == modalOK) {
                modalOK.style.display = "none";
            } else if (event.target == modalSO) {
                modalSO.style.display = "none";
            } else if (event.target == modalMH) {
                modalMH.style.display = "none";
            } else if (event.target == modalUW) {
                modalUW.style.display = "none";
            } else if (event.target == modalSC) {
                modalSC.style.display = "none";
            } else if (event.target == modalCW) {
                modalCW.style.display = "none";
            }
        }
    </script>

@endsection