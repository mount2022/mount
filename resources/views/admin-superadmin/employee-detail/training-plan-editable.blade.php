@extends('layouts-admin.main')

@section('content')

<style>
    .picture-container {
        position: relative;
        cursor: pointer;
        text-align: center;
    }
    .picture {
        width: 106px;
        height: 106px;
        background-color: #999999;
        border: 4px solid #CCCCCC;
        color: #FFFFFF;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        transition: all 0.2s;
        -webkit-transition: all 0.2s;
    }
    .picture:hover {
    border-color: #2ca8ff;
    }
    .content.ct-wizard-green .picture:hover {
        border-color: #05ae0e;
    }
    .content.ct-wizard-blue .picture:hover {
        border-color: #3472f7;
    }
    .content.ct-wizard-orange .picture:hover {
        border-color: #ff9500;
    }
    .content.ct-wizard-red .picture:hover {
        border-color: #ff3b30;
    }
    .picture input[type="file"] {
        cursor: pointer;
        display: block;
        height: 100%;
        left: 0;
        opacity: 0 !important;
        position: absolute;
        top: 0;
        width: 100%;
    }
    .picture-src {
        width: 100%;

    }

    .dropdown-item{
        color: #A8A8A8;
    }
    .dropdown-item:hover{
        background-color: #F7F9FF;
        color: #4107BB;
    }
    img.icon:hover, img.icon:focus{
        filter: invert(11%) sepia(98%) saturate(5938%) hue-rotate(263deg) brightness(70%) contrast(115%);
    }
    .column-title{
        background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%), linear-gradient(0deg, #A8A8A8, #A8A8A8);
        color: white;
        height: 3.2rem;
    }
    th {
        font-size:12px;
        text-align: center; 
        color: white;
        height: 3.2rem;
        background: linear-gradient(0deg, #A8A8A8, #A8A8A8);

    }
    .submit{
    background-color: #00DFC7;
    border: none;
    border-radius: 8px;
    }
    .submit:hover{
        opacity: 80%;
        background-color: #00DFC7;
    }
</style>

<script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-tabledit@1.0.0/jquery.tabledit.min.js"></script>

<div class="row" style="padding-bottom: 20%;">
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:35%; height:100%; left:30%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <br>
                <!-- profile picture -->
                <div class="container">
                    <div class="picture-container">                        
                        <div class="picture">
                            @php
                                $picture = App\Models\User::where('id', request()->route('id'))->value('picture');
                                $path = url('user/images/' . $picture);
                            @endphp
                            <img src="{{ $path }}" class="picture-src" id="wizardPicturePreview" title="">
                            {{-- <input type="file" id="wizard_picture" name="wizard_picture" class=""> --}}
                        </div>
                    </div>
                </div>
                <!-- profile picture -->


                <br>
                <!-- sidebar button -->
                <div class="row">
                    <a class="nav-link disable" href="/dashboard-admin/profile/{{ request()->route('id') }}">
                        <img class="icon" src="/image/user.png" width="10%" alt=""> &nbsp; My Profile
                    </a>
                    <a class="nav-link" href="/dashboard-admin/profile/change-password/{{ request()->route('id') }}">
                        <img class="icon" src="/image/lock.png" width="10%" alt=""> &nbsp; Change Password
                    </a>
                    {{-- <a class="nav-link disable" href="#">
                        <img class="icon" src="/image/notif.png" width="10%" alt=""> &nbsp; Notification
                    </a> --}}
                    <a class="nav-link" href="/dashboard-admin/profile/kpi/{{ request()->route('id') }}">
                        <img class="icon" src="/image/kpi.png" width="10%" alt=""> &nbsp; Key Performance Indicator
                    </a>
                    <a class="nav-link" href="/dashboard-admin/profile/jobdesk/{{ request()->route('id') }}">
                        <img class="icon" src="/image/jobdesc.png" width="10%" alt=""> &nbsp; Job Description
                    </a>
                    <a class="nav-link active" href="/dashboard-admin/profile/training/{{ request()->route('id') }}">
                        <img class="icon" src="/image/training.png" width="10%" alt=""> &nbsp; Training Plan
                    </a>
                </div>
                <div class="row" style="margin-left: 5%; margin-top: 100%;">
                    <form action="/logout" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-sm profile-logout">LOG OUT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Training Plan  -->
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:125%; height:100%; left:-35%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <form action="/dashboard-admin/profile/training/title" method="post"> @csrf
                    <h4>Training Plan</h4>
                    <input type="hidden" name="route_id" value="{{ request()->route('id') }}">
                    &nbsp;

                    @if(auth()->user()->role_id == 3)
                        @if($TrainingTitle->isEmpty() == true)
                            <div class="row">
                                <div class="form-group" style="width: 30rem;">
                                    <p style="color: #A8A8A8; font-weight: 400;">Title</p>
                                    <input type="text" name="title" class="form-control" style="margin-top: -45px; margin-left: 180px;" placeholder="Training Title">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group" style="width: 30rem;">
                                    <p style="color: #A8A8A8; font-weight: 400;">Period</p>
                                    <input type="text" name="period" class="form-control" style="margin-top: -45px; margin-left: 180px;" placeholder="Period">
                                </div>
                            </div>
                        @else
                            @foreach($TrainingTitle as $value)
                                <div class="row">
                                    <div class="form-group" style="width: 30rem;">
                                        <p style="color: #A8A8A8; font-weight: 400;">Title</p>
                                        <input type="text" name="title" class="form-control" style="margin-top: -45px; margin-left: 180px;" 
                                        placeholder="Training Title" value="{{ $value->training_type }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group" style="width: 30rem;">
                                        <p style="color: #A8A8A8; font-weight: 400;">Period</p>
                                        <input type="text" name="period" class="form-control" style="margin-top: -45px; margin-left: 180px;" 
                                        placeholder="Period" value="{{ $value->when }}">
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    @else
                        <div class="row">
                            <div class="form-group" style="width: 30rem;">
                                <p style="color: #A8A8A8; font-weight: 400;">Title</p>
                                <input type="text" name="title" class="form-control" style="margin-top: -45px; margin-left: 180px;" placeholder="Training Title" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group" style="width: 30rem;">
                                <p style="color: #A8A8A8; font-weight: 400;">Period</p>
                                <input type="text" name="period" class="form-control" style="margin-top: -45px; margin-left: 180px;" placeholder="Period" disabled>
                            </div>
                        </div>
                    @endif
                    
                    @if(auth()->user()->role_id == 3)
                        <button type="submit" class="btn btn-primary mb-3 pl-4 pr-4 pt-2 pb-2 submit" style="float: right;margin-top: -40px;">SAVE TITLE</button>
                    @endif
                </form>

                @if(auth()->user()->role_id == 3)
                    <button id='add' for-table='#editable' class="btn btn-sm mb-3 pl-3 pr-3 pt-2 pb-2" style="background-color: #ffffff; color: #00DFC7; border-color: #00DFC7; border-radius: 8px;">
                        ADD NEW PLAN
                    </button>
                @endif

                @if(auth()->user()->role_id == 3)
                    <table id="editable" class="table table-bordered"> @csrf
                @else
                    <table id="" class="table table-bordered">
                @endif
                    <thead>
                        <tr>
                            @if(auth()->user()->role_id == 3)
                                <th scope="col" class="align-middle">ID</th>
                            @endif
                            <th scope="col" class="align-middle">NO</th>
                            <th scope="col" class="align-middle">TRAINING TYPE</th>
                            <th scope="col" class="align-middle">WHEN</th>
                            <th scope="col" class="align-middle">OBJECTIVES</th>
                            <th scope="col" class="align-middle">PARTICIPANTS</th>
                            <th scope="col" class="align-middle">TRAINING REQUIREMENT</th>
                            <th scope="col" class="align-middle">NOTES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $number = 1;
                        @endphp
                        @foreach($Trainings as $training)
                            <tr>
                                @if(auth()->user()->role_id == 3)
                                    <td style="text-align: center;">{{ $training->id }}</td>
                                @endif
                                <td style="text-align: center;">{{ $number++ }}</td>
                                <td>{{ $training->training_type }}</td>
                                <td style="text-align: center;">{{ $training->when }}</td>
                                <td>{{ $training->objective }}</td>
                                <td style="text-align: center;">{{ $training->participant }}</td>
                                <td>{{ $training->requirement }}</td>
                                <td style="text-align: center;">{{ $training->note }}</td>
                                <td style="display: none;"><input type="hidden" class="tabledit-input" name="user_id" value="{{ request()->route('id') }}"></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> --}}

<script>
    $(document).ready(function() {

        $("#add").click(function(e){
            var table = $(this).attr('for-table');  //get the target table selector
            var $tr = $(table + ">tbody>tr:last-child").clone(true, true);  //clone the last row
            var nextID = parseInt($tr.find("input.tabledit-identifier").val()) + 1; //get the ID and add one.
            $tr.find("input.tabledit-identifier").val(nextID);  //set the row identifier
            $tr.find("span.tabledit-identifier").text(nextID);  //set the row identifier
            $(table + ">tbody").append($tr);    //add the row to the table
            $tr.find(".tabledit-edit-button").click();  //pretend to click the edit button
            $tr.find("input:not([type=hidden]), select").val("");   //wipe out the inputs.
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token' : $("input[name=_token]").val()
            }
        });

        $('#editable').Tabledit({
            url: '{{ route("training.action") }}',
            dataType: "json",
            hideIdentifier: 'true',
            buttons: {
                edit: {
                    html: '<img src="/image/edit.png" alt="" width="20px">',
                    action: 'edit'
                },
                delete: {
                    html: '<img src="/image/delete-red.png" alt="" width="20px">',
                    action: 'delete'
                }
            },
            columns:{
                identifier: [0, 'id'],
                editable: [[2, 'training_type'], [3, 'when'], [4, 'objective'], [5, 'participant'], [6, 'requirement'], [7, 'note']]
            },
            restoreButton: false,
            onSuccess:function(data, textStatus, jqXHR) {
                console.log(data);
                if(data.action == 'delete') {
                    $('#' + data.id).remove();
                }

                window.location.reload();
            },
            onFail: function(jqXHR, textStatus, errorThrown) {
                // deal with errors there
            },
            onAjax: function(action, serialize) {
                // open your xhr here 
                console.log("on Ajax");
                console.log("action : ", action);
                console.log("data : ", serialize);
            }
        });
    });
</script>

@endsection