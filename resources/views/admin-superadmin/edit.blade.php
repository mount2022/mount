<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="EditModal{{ $user->id }}">
    <div class="modal-dialog modal-lg">
        <form method="POST" action="/dashboard-admin/employee/update/{{ $user->id }}">
        @csrf
            <div class="modal-content">
                <div class="modal-header" style="margin: 0px; padding: 0px;">
                    <img class="mb-2" src="/image/edit.png" alt="" style="width:50px;">
                    <h5 class="modal-title mb-2" id="exampleModal" style="margin-left: 2%;">Edit Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col"> 
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Full Name</label>
                                <div class="form-group">
                                    <input class="form-control @error('name') is-invalid @enderror" type="text" id="" name="name" 
                                    placeholder="Full Name" value="{{ old('name', $user->name) }}"> 
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Phone Number</label>
                                <div class="form-group">
                                    <input class="form-control @error('phone') is-invalid @enderror" type="text" id="" name="phone" 
                                    placeholder="Phone Number" value="{{ old('phone', $user->phone) }}"> 
                                    @error('phone')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Home Address</label>
                            <div class="form-group">
                                <input class="form-control @error('address') is-invalid @enderror" type="text" id="" name="address"
                                placeholder="Home Address" value="{{ old('address', $user->address) }}"> 
                                @error('address')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Place of Birth</label>
                                <div class="form-group">
                                    <input class="form-control @error('place_of_birth') is-invalid @enderror" type="text" id="" name="place_of_birth" 
                                    placeholder="Place of Birth" value="{{ old('place_of_birth', $user->place_of_birth) }}"> 
                                    @error('place_of_birth')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Date of Birth</label>
                                <div class="input-group flex-nowrap">
                                    <input class="form-control @error('date_of_birth') is-invalid @enderror" type="date" id="date_of_birth" 
                                    name="date_of_birth" value="{{ old('date_of_birth', $user->date_of_birth) }}">
                                    <span class="input-group-text" id="addon-wrapping"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                @error('date_of_birth')
                                    <div style="color: #dc3545; font-size: .875em;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-2 mb-5">
                            <div class="col-sm-2">
                                <label for="task_status" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Gender</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="Male" {{ strtolower(old('gender', $user->gender)) == "male" ? 'checked' : '' }}>
                                    <label class="form-check-label" for="gender">Male</label>
                                </div>
                            </div>
                            <div class="col">
                                <label for="task_status" style="font-weight:200; font-size: 12px; color: #FFFFFF;">Gender</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender" value="Female" {{ strtolower(old('gender', $user->gender)) == "female" ? 'checked' : '' }}>
                                    <label class="form-check-label" for="gender">Female</label>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown-divider" style="margin-top: -30px;"></div>

                        <div class="row">
                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Employee ID</label>
                                <div class="form-group">
                                    <input class="form-control @error('employee_id') is-invalid @enderror" type="text" id="" name="employee_id" placeholder="Employee ID"
                                    value="{{ old('employee_id', $user->employee_id) }}"> 
                                    @error('employee_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Email Address</label>
                                <div class="form-group">
                                    <input class="form-control @error('email') is-invalid @enderror" type="text" id="" name="email" placeholder="Email Address" 
                                    value="{{ old('email', $user->email) }}"> 
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Department</label>
                                <div class="form-group">
                                    <select name="department_id" class="selectpicker form-control select2 @error('department_id') is-invalid @enderror" title="-- Select Department --">
                                        @foreach($departmentAll as $department)
                                            <option value="{{ $department->id }}" {{ old('department_id', $user->department_id) ==$department->id ? 'selected' : '' }}>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('department_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Position</label>
                                <div class="form-group">
                                    <input class="form-control @error('position') is-invalid @enderror" type="text" id="" name="position" placeholder="Position" 
                                    value="{{ old('position', $user->position) }}"> 
                                    @error('position')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Employment Status</label>
                                <div class="form-group">
                                    <select name="employment_status" class="selectpicker form-control select2 @error('employment_status') is-invalid @enderror" title="-- Select Employee Status --">
                                        <option value="Contract" {{ old('employment_status', $user->employment_status) == "Contract" ? 'selected' : '' }}>Contract</option>
                                        <option value="Permanent" {{ old('employment_status', $user->employment_status) == "Permanent" ? 'selected' : '' }}>Permanent</option>
                                        <option value="Internship" {{ old('employment_status', $user->employment_status) == "Internship" ? 'selected' : '' }}>Internship</option>
                                    </select>
                                    @error('employment_status')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Job Level</label>
                                    <input class="form-control @error('job_level') is-invalid @enderror" type="input" id="" name="job_level" placeholder="Job Level"
                                    value="{{ old('job_level', $user->job_level) }}">
                                    @error('job_level')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Employee Role</label>
                                <div class="form-group">
                                    <select name="role_id" class="selectpicker form-control select2 @error('role_id') is-invalid @enderror" title="-- Select Employee Role --">
                                        @foreach($roleAll as $role)
                                            <option value="{{ $role->id }}" {{ old('role_id', $user->role_id) == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('role_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Join Date</label>
                                <div class="input-group flex-nowrap">
                                    <input class="form-control @error('join_date') is-invalid @enderror" type="date" id="" name="join_date" value="{{ old('join_date', $user->join_date) }}">
                                    <span class="input-group-text" id="addon-wrapping"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                @error('join_date')
                                    <div style="color: #dc3545; font-size: .875em;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn submit" style="color: white;">UPDATE</button> 
                    <button type="button" class="btn" data-dismiss="modal" 
                    style="background-color: #ffffff; color: #A8A8A8; border-color: #A8A8A8; border-radius: 8px;">CANCEL</button>
                </div>
            </div>
        </form>
    </div>
</div>