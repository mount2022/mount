@extends('layouts-admin.main')

@section('content')
	<style>
		.newtask{
			background-color: #4107BB;
			border: none;
			border-radius: 8px;
		}

		.newtask:hover{
			opacity: 80%;
			background-color: #4107BB;
		}

		.dayoff{
			background-color: #FF6060;
			border: none;
			border-radius: 8px;
		}

		.dayoff:hover{
			opacity: 80%;
			background-color: #FF6060;
		}

		.submit{
			background-color: #00DFC7;
			border: none;
			border-radius: 8px;
		}

		.submit:hover{
			opacity: 80%;
			background-color: #00DFC7;
		}

		.height {
			height: 100vh
		}

		.form {
			position: relative
		}

		.form .fa-search {
			position: absolute;
			top: 20px;
			left: 270px;
			color: #DEDEDE;
		}

		.form-input {
			height: 55px;
			text-indent: 10px;
			border-radius: 8px;
		}

		.form-input:focus {
			box-shadow: none;
			border-color: #DEDEDE;
		}

		.dot {
			height: 17px;
			width: 17px;
			border-radius: 100%;
			display: flex;
			margin-top: -29px;
			margin-left: 32px;
		}

		table.dataTable thead .sorting:after,
		table.dataTable thead .sorting:before,
		table.dataTable thead .sorting_asc:after,
		table.dataTable thead .sorting_asc:before,
		table.dataTable thead .sorting_asc_disabled:after,
		table.dataTable thead .sorting_asc_disabled:before,
		table.dataTable thead .sorting_desc:after,
		table.dataTable thead .sorting_desc:before,
		table.dataTable thead .sorting_desc_disabled:after,

		table.dataTable thead .sorting_desc_disabled:before {
			bottom: .5em;
		}
	</style>

	@php
		$isSession = false;
	@endphp

	<nav class="navbar" style="background-color: #F5F4FF; box-shadow:unset;">
		<div class="container" style="height: 40px; margin-left: 185px;">
			<div class="col-sm-1" style="margin-top: -10px; margin-right: -30px;">
				<h4 style="color: #4107BB; font-weight: 300; margin-left:-10%; font-size: 20px;">Select</h4>
			</div>

			<div class="col-sm-2" style="width: 12%; margin-top: 1px;">
			<form action="#" method="get">
				<div class="form-group">
					<select id="month" name="month" class="form-control select2" style="border: none; border-radius: 8px;">
						<option selected disabled>Month</option>
						<option value="01">January</option>
						<option value="02">February</option>
						<option value="03">March</option>
						<option value="04">April</option>
						<option value="05">May</option>
						<option value="06">June</option>
						<option value="07">July</option>
						<option value="08">August</option>
						<option value="09">Sertember</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">Desember</option>
					</select>
				</div>
			</div>
				<div class="col-sm-2" style="width: 12%; margin-top: 1px;">
					<div class="form-group">
						<select id="year" name="year" class="form-control select2" style="border: none; border-radius: 8px;">
							<option selected disabled>Year</option>
								<?php 
									$year = date('Y');
									$min = $year - 10;
									$max = $year;
									for( $i=$max; $i>=$min; $i-- ) {
										echo '<option value='.$i.'>'.$i.'</option>';
									}
								?>
						</select>
					</div>
				</div>

				@if(request()->get('department') != null)
					<input type="hidden" name="department" value="{{ request()->get('department') }}">
				@endif

				<div class="col" style="margin-top: -1%;">
					<button class="btn" style="background-color: #4107BB; color: #ffffff; border-radius: 8px;">
						<img src="/image/search.png" alt="search">
					</button>
				</div>
			</form>
				
			<ul class="nav justify-content-end" style="margin-right: 10px;">
				<li style="margin-top:-10%; font-weight: 500; font-size: 20px; color: #4107BB;">
					@if (request()->get('month') != null && request()->get('year') != null)
						@php
							$date = \Carbon\Carbon::createFromDate(request()->get('year'), request()->get('month'), 1);
						@endphp
						{{ $date->format('F Y') }}
					@else
						{{ date("F Y") }}
					@endif
				</li>
			</ul>
		</div>
	</nav>

	<div class="container" style="padding-top: 30px;">
		<nav>
			<ul class="nav">
				@if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
					<form action="/dashboard-averageHours" method="GET" class="form-inline">
					<p style="color: #A8A8A8;">Choose Department</p>
						<li class="nav-item" style="width: 23.5%;">
								<li class="nav-item">
									<div class="input-group mb-3" style="margin-top: -1%;">
										<select id="department" name="department" class="form-control select2" style="border-radius: 8px;">
											@foreach ($Department as $department)
												@if ($isSearch == true)
													<option value="{{ $department->id }}" {{ request()->get('department') == $department->id ? "selected" : "" }}>{{ $department->name }}</option>
												@else  
													<option value="{{ $department->id }}" {{ auth()->user()->department_id == $department->id ? "selected" : "" }}>{{ $department->name }}</option>
												@endif
											@endforeach
										</select>
										@if(request()->get('department') != null)
											<input type="hidden" name="month" value="{{ request()->get('month') }}">
											<input type="hidden" name="year" value="{{ request()->get('year') }}">
										@endif
									</div>
								</li>
							
						</li>
						<li class="nav-item"  style="margin-left: 5px; margin-top: -5%;">
							<button class="btn" type="submit" style="background-color: #4107BB; color: #ffffff; border-radius: 8px;">
								<img src="/image/search.png" alt="search">
							</button>
						</li>
					</form>
				@endif
				<li class="nav-item" style="margin-left: auto; margin-top: 15px;">
					<p style="color: #A8A8A8;">Action</p>
				</li>
				&nbsp; &nbsp; &nbsp;
				<li class="nav-item">
					<form action="/dashboard-averageHours/export" method="post"> @csrf
						@if(request()->get('month') != null && request()->get('year') != null && request()->get('department') != null)
							<input type="hidden" name="month" value="{{ request()->get('month') }}">
							<input type="hidden" name="year" value="{{ request()->get('year') }}">
							<input type="hidden" name="department" value="{{ request()->get('department') }}">
						@elseif (request()->get('month') == null && request()->get('year') == null && request()->get('department') != null)
							<input type="hidden" name="month" value="{{ date('m') }}">
							<input type="hidden" name="year" value="{{ date('Y') }}">
							<input type="hidden" name="department" value="{{ request()->get('department') }}">
						@else
							<input type="hidden" name="month" value="{{ date('m') }}">
							<input type="hidden" name="year" value="{{ date('Y') }}">
							<input type="hidden" name="department" value="{{ auth()->user()->department_id }}">
						@endif
						<button type="submit" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">EXPORT</button>
					</form>
				</li>
			</ul>
		</nav>

		<div class="dropdown-divider"></div>

		<div class="row pt-3 pb-4 position-relative">
			<div class="col-md-3">
				<!-- <div class="form">
					<form class="form" id="searchForm" method="GET" action="#">
						<input type="text" name="search" id="search" class="form-control form-input" placeholder="Find someone..."> 
						@csrf
					</form>
					<i class="fa fa-search"></i> 
				</div> -->
				<p style="color: #A8A8A8;">Department</p>
				@php
					$dep = $User[0]['department_id'];
				@endphp
				<h2>{{ $dep }}</h2>
			</div>
			<div class="col-md-3 position-absolute end-0">
				<p style="color: #A8A8A8;">Total Average Hours</p>
				<h2>{{ sprintf('%0.2f', $totalAverage) }} hours/day</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<table id="dtOrderExample" class="table table-borderless">
					<thead>
						<tr style="color: #A8A8A8;">
							<th scope="col" style="font-weight: 400;">Employee Name</th>
							<th scope="col" style="font-weight: 400;">Unit</th>
							<th scope="col" style="font-weight: 400;">Working Hours</th>
							<th scope="col" style="font-weight: 400;">Working Days</th>
							<th scope="col" style="font-weight: 400;">Average Hours</th>
						</tr>
					</thead>
					<tbody>
						<a href="#" class="list">
							@foreach($User as $user)
								<tr>
									<td>
										@php
											$path = url('user/images/' . $user->picture);
										@endphp
										<img src="{{ $path }}" width="40" height="40" class="rounded-circle">
										<div style="margin-top: -45px; margin-left: 55px;">
											<h5 style="color: #1F1F1F;">{{ $user->name }}</h5>
											<p style="color: #A8A8A8; margin-top: -7px;">{{ $user->employee_id }}</p>
										</div>
									</td>
									<td><h5 style="font-weight: 300;">{{ $user->department_id }}</h5></td>
									<td><h5 style="font-weight: 300;">{{ $user->hour }} hours</h5></td>
									<td><h5 style="font-weight: 300;">{{ $user->days }} days</h5></td>
									<td><h2>{{ $user->average }} hours/day</h2></td>
								</tr>
							@endforeach
							
						</a>
					</tbody>
				</table>
			</div>
		</div>

		<meta name="user-id" content="{{ auth()->user()->id }}">
	</div>

@endsection