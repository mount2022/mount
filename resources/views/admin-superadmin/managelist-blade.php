@extends('layouts-admin.main')

@section('content')
<style>
    .newtask{
        background-color: #4107BB;
        border: none;
        border-radius: 8px;
    }
    .submit{
    background-color: #00DFC7;
    border: none;
    border-radius: 8px;
    }
    .form-control{
    border: none!important;
    background-color: #F7F7F7;
    border-radius: 8px;
    }
     /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }

  /* Modal Content */
  .modal-content {
    background-color: #ffffff;
    margin-top: 7%;
    margin-left: 23%;
    padding: 20px;
    border: 1px solid #888;
    width: 55%;
    border-radius: 16px;
    box-shadow: 0px 2px 40px rgba(0, 0, 0, 0.15);
  }

  /* The Close Button */
  .close {
    margin-left: 95%;
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }
</style>

<div class="row">
  <div class="col mt-5" style="margin-left: 10%;">
    <nav>
		<ul class="nav">
            <li class="nav-item">
                <h2 style="color: #4107BB; font-weight: 600;">List of Project name</h2>
                <h6 style="font-weight: 400;">Manage Field / Project name</h6>
			</li>
            <li class="nav-item" style="margin-left: 840px;">
                <a href="#" id="myBtn" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">+ ADD NEW OPTION</a>
                <div id="myModal" class="modal">
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <h2 style="color: #4107BB; font-weight: 600; text-align: center;">Add New Option</h2>
                        <h6 style="font-weight: 400; text-align: center;">This option will be displayed on the dropdown list of the field</h6>
                        <div class="form-group" style="width: 70%; margin-left: 135px; margin-top: 10px;">
                            <input class="form-control" type="text" id="note" name="note" placeholder="New Option...">
                        </div>
                
                        <div class="col justify-content-end">
                            <a href="#" id="myBtn2" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="margin-top: 20px; margin-left: 360px;">ADD TO LIST</a>
                        </div>
                    </div>
                </div>
			</li>
        </ul>
    </nav>
  </div>
</div>

<div class="row" style="margin-left: 155px;margin-top: 20px;">
			<div class="col-12">
                <table id="dtOrderExample" class="table table-borderless">
                    <thead>
                    <tr style="color: #A8A8A8;">
                        <th scope="col-sm-5" style="font-weight: 400;">ID</th>
                        <th scope="col" style="font-weight: 400;">Project Name</th>
                        <th scope="col" style="font-weight: 400;">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Shell Retail</td>
                            <td>
                                <ul class="nav">
                                    <li>
                                        <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" style="background-color: #ffffff; color: #FF6060; border-color: #FF6060; border-radius: 8px;">Delete</button>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
</div>


<script>
  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the button that opens the modal
  var btn = document.getElementById("myBtn");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks the button, open the modal 
  btn.onclick = function() {
    modal.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
</script>

@endsection