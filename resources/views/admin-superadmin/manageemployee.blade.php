@extends('layouts-admin.main')
@section('content')
	<style>
		.newtask{
			background-color: #4107BB;
			border: none;
			border-radius: 8px;
		}
		.newtask:hover{
			opacity: 80%;
			background-color: #4107BB;
		}
		.dayoff{
			background-color: #FF6060;
			border: none;
			border-radius: 8px;
		}
		.dayoff:hover{
			opacity: 80%;
			background-color: #FF6060;
		}
		.submit{
			background-color: #00DFC7;
			border: none;
			border-radius: 8px;
		}
		.submit:hover{
			opacity: 80%;
			background-color: #00DFC7;
		}
		.height {
			height: 100vh
		}

		.form {
			position: relative
		}
		.form .fa-search {
			position: absolute;
			top: 20px;
			left: 270px;
			color: #DEDEDE;
		}
		.form-input {
			height: 55px;
			text-indent: 10px;
			border-radius: 8px;
		}
		.form-input:focus {
			box-shadow: none;
			border-color: #DEDEDE;
		}
		.dot {
			height: 17px;
			width: 17px;
			border-radius: 100%;
			display: flex;
			margin-top: -29px;
			margin-left: 32px;
		}
		.modal-content {
			width: 55rem;
			border-radius: 16px;
			box-shadow: 0px 2px 40px 0px #00000026;
			border-color: white;
			padding: 35px;
		}
		table.dataTable thead .sorting:after,
		table.dataTable thead .sorting:before,
		table.dataTable thead .sorting_asc:after,
		table.dataTable thead .sorting_asc:before,
		table.dataTable thead .sorting_asc_disabled:after,
		table.dataTable thead .sorting_asc_disabled:before,
		table.dataTable thead .sorting_desc:after,
		table.dataTable thead .sorting_desc:before,
		table.dataTable thead .sorting_desc_disabled:after,
		table.dataTable thead .sorting_desc_disabled:before {
			bottom: .5em;
		}
		.required:after {
			content:" *";
			color: red;
		}
	</style>

	
	<!-- WORKING Selectpicker JS -->
	{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script> --}}

	<div class="container" style="padding-top: 30px;">
		@if (session()->has('created'))
			<div class="alert alert-success" role="alert">
				{{ session('created') }}
			</div>
		@elseif (session()->has('deleted'))
			<div class="alert alert-success" style="background-color: #FF6060;" role="alert">
				{{ session('deleted') }}
			</div>
		@endif

		<div class="row pt-3 pb-4">
			<div class="col-md-3">
				<div class="form">
					<form class="form" id="searchForm" method="GET" action="/dashboard-admin/employee">
						<input type="text" name="search" id="search" class="form-control form-input" placeholder="Find someone..."> 
					</form>
					<i class="fa fa-search"></i> 
				</div>
			</div>

			@if(auth()->user()->role_id == 3)
				<div class="col-md-3" style="margin-left: auto;">
					<button type="button" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3" style="background-color: #4107BB; border: none; border-radius: 8px;" 
					data-toggle="modal" data-target=".ModalAdd">
						+ ADD NEW EMPLOYEE
					</button>
				</div>
			@endif
			
			<div class="row">
				<div class="col-12">
					<table id="dtOrderExample" class="table table-borderless">
						<thead>
							<tr style="color: #A8A8A8;">
								<th scope="col" style="font-weight: 400;">No</th>
								<th scope="col" style="font-weight: 400;">Employee Name</th>
								<th scope="col" style="font-weight: 400;">Employee ID</th>
								<th scope="col" style="font-weight: 400;">Department</th>
								@if(auth()->user()->role_id == 3)
									<th scope="col" style="font-weight: 400;">Action</th>
								@endif
							</tr>
						</thead>
						<tbody>
							@php $number = 1; @endphp
							@foreach($user as $user)
								
									<tr>
										<td><h5>{{ $number++ }}</h5></td>
										<td>
											@php $path = url('user/images/' . $user->picture); @endphp
											<img src="{{ $path }}" width="40" height="40" class="rounded-circle">

											<a href="/dashboard-admin/profile/{{ $user->id }}" style="text-decoration: none;">
												<h5 style="color: #1F1F1F;margin-top: -35px; margin-left: 55px;">{{ $user->name }}</h5>
											</a>
										</td>
										<td><h5 style="font-weight: 300;">{{ $user->employee_id }}</h5></td>
										<td><h5 style="font-weight: 300;">{{ $user->department_name }}</h5></td>
										@if(auth()->user()->role_id == 3)
											<td>
												<ul class="nav">
													<li>
														{{-- <form action="/dashboard-admin/employee/{{ $user->id }}/edit" method="GET">
															@csrf
															<button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
															style="background-color: #ffffff; color: #4107BB; border-color: #4107BB; border-radius: 8px;">
																<img src="/image/edit-detail.png" alt="">&nbsp;Edit Detail
															</button>
														</form> --}}
														@include('admin-superadmin.edit')
														<a href="#"  class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" style="background-color: #ffffff; color: #4107BB; border-color: #4107BB; border-radius: 8px;"
														data-target="#EditModal{{ $user->id }}" data-toggle="modal">
															<img src="/image/edit-detail.png" alt="">&nbsp;Edit Detail
														</a>													
													</li> &nbsp; &nbsp; 
													<li>
														<form action="/dashboard-admin/employee/delete" method="POST">
															@csrf
															<input type="hidden" name="id" value="{{ $user->id }}">
															<button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" onclick="return confirm('Are you sure to delete?')" 
															style="background-color: #ffffff; color: #FF6060; border-color: #FF6060; border-radius: 8px;">
																<img src="/image/delete-red.png" alt="">Delete User</button>
														</form>
													</li>
												</ul>
											</td>
										@endif
									</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade bd-example-modal-lg ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal">
		<div class="modal-dialog modal-lg">
			<form method="POST" action="/dashboard-admin/employee"> @csrf
				<div class="modal-content">
					<div class="modal-header" style="margin: 0px; padding: 0px;">
						<img class="mb-2" src="/image/edit-employee.png" alt="" style="width:50px;">
						<h5 class="modal-title mb-2" id="exampleModal" style="margin-left: 2%;">Add New Employee</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Full Name</label>
									<div class="form-group">
										<input class="form-control @error('fullName') is-invalid @enderror" type="text" id="" name="fullName" 
										placeholder="Full Name" value="{{ old('fullName') }}"> 
										@error('fullName')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>

								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Phone Number</label>
									<div class="form-group">
										<input class="form-control @error('phoneNumber') is-invalid @enderror" type="text" id="" name="phoneNumber" 
										placeholder="Phone Number" value="{{ old('phoneNumber') }}"> 
										@error('phoneNumber')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
							</div>

							<div class="row">
								<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Home Address</label>
								<div class="form-group">
									<input class="form-control @error('homeAddress') is-invalid @enderror" type="text" id="" name="homeAddress"
									placeholder="Home Address" value="{{ old('homeAddress') }}"> 
									@error('homeAddress')
										<div class="invalid-feedback">
											{{ $message }}
										</div>
									@enderror
								</div>
							</div>

							<div class="row">
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Place of Birth</label>
									<div class="form-group">
										<input class="form-control @error('pob') is-invalid @enderror" type="text" id="" name="pob" 
										placeholder="Place of Birth" value="{{ old('pob') }}"> 
										@error('pob')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Date of Birth</label>
									<div class="input-group flex-nowrap">
										<input class="form-control @error('dob') is-invalid @enderror" type="date" id="dob" name="dob" value="{{ old('date_of_birth') }}">
										<span class="input-group-text" id="addon-wrapping"><i class="far fa-calendar-alt"></i></span>
									</div>
									@error('dob')
										<div style="color: #dc3545; font-size: .875em;">
											{{ $message }}
										</div>
									@enderror
								</div>
							</div>

							<div class="row mt-2 mb-5">
								<div class="col-sm-2">
									<label for="task_status" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Gender</label>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="Gender" id="Gender" value="Male" {{ old('Gender') == "Male" ? "checked" : "" }}>
										<label class="form-check-label" for="Gender">Male</label>
									</div>
								</div>

								<div class="col">
									<label for="task_status" style="font-weight:200; font-size: 12px; color: #FFFFFF;">Gender</label>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="Gender" id="Gender" value="Female" {{ old('Gender') == "Female" ? "checked" : "" }}>
										<label class="form-check-label" for="Gender">Female</label>
									</div>
								</div>
							</div>

							<div class="dropdown-divider" style="margin-top: -30px;"></div>

							<div class="row">
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Employee ID</label>
									<div class="form-group">
										<input class="form-control @error('employeeID') is-invalid @enderror" type="text" id="" name="employeeID" 
										placeholder="Employee ID" value="{{ old('employeeID') }}"> 
										@error('employeeID')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>

								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Email Address</label>
									<div class="form-group">
										<input class="form-control @error('emailAddress') is-invalid @enderror" type="text" id="" name="emailAddress" 
										placeholder="Email Address" value="{{ old('emailAddress') }}"> 
										@error('emailAddress')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Department</label>
									<div class="form-group">
										<select name="departmentID" class="selectpicker form-control select2 @error('departmentID') is-invalid @enderror" 
										title="-- Select Department --">
											@foreach($departmentAll as $department)
												<option value="{{ $department->id }}" {{ old("departmentID") == $department->id ? "selected" : "" }}>{{ $department->name }}</option>
											@endforeach
										</select>
										@error('departmentID')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>

								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Position</label>
									<div class="form-group">
										<input class="form-control @error('employeePosition') is-invalid @enderror" type="text" id="" name="employeePosition" 
										placeholder="Position" value="{{ old('employeePosition') }}"> 
										@error('employeePosition')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Employment Status</label>
									<div class="form-group">
										<select name="employmentStatus" class="selectpicker form-control select2 @error('employmentStatus') is-invalid @enderror" 
										title="-- Select Employee Status --">
											<option value="Contract" {{ old("employmentStatus") == "Contract" ? "selected" : "" }}>Contract</option>
											<option value="Permanent" {{ old("employmentStatus") == "Permanent" ? "selected" : "" }}>Permanent</option>
											<option value="Internship" {{ old("employmentStatus") == "Internship" ? "selected" : "" }}>Internship</option>
										</select>
										@error('employmentStatus')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Job Level</label>
										<input class="form-control @error('jobLevel') is-invalid @enderror" type="input" id="" name="jobLevel" value="{{ old('jobLevel') }}">
										@error('jobLevel')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Employee Role</label>
									<div class="form-group">
										<select name="roleID" class="selectpicker form-control select2 @error('roleID') is-invalid @enderror" title="-- Select Employee Role --">
											@foreach($roleAll as $role)
												@if($role->name != 'SuperAdmin')
													<option value="{{ $role->id }}" {{ old("roleID") == $role->id  ? "selected" : "" }}>{{ $role->name }}</option>
												@endif
											@endforeach
										</select>
										@error('roleID')
											<div class="invalid-feedback">
												{{ $message }}
											</div>
										@enderror
									</div>
								</div>
								<div class="col">
									<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Join Date</label>
									<div class="input-group flex-nowrap">
										<input class="form-control @error('joinDate') is-invalid @enderror" type="date" id="" name="joinDate" value="{{ old('joinDate') }}">
										<span class="input-group-text" id="addon-wrapping"><i class="far fa-calendar-alt"></i></span>
									</div>
									@error('joinDate')
										<div style="color: #dc3545; font-size: .875em;">
											{{ $message }}
										</div>
									@enderror
								</div>
							</div>

							{{-- <div class="row">
								<div class="col">
									<form>
										<div class="form-group">
											<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Key Performance Indicator (KPI)</label>
											<input type="file" class="form-control-file" id="exampleFormControlFile1">
										</div>
									</form>
								</div>
								<div class="col">
									<form>
										<div class="form-group">
											<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Job Description</label>
											<input type="file" class="form-control-file" id="exampleFormControlFile2">
										</div>
									</form>
								</div>
								<div class="col">
									<form>
										<div class="form-group">
											<label for="" id="" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Training Plan</label>
											<input type="file" class="form-control-file" id="exampleFormControlFile2">
										</div>
									</form>
								</div>
							</div> --}}
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn submit" style="color: white;">ADD TO LIST</button> 
						<button type="button" class="btn" data-dismiss="modal" 
						style="background-color: #ffffff; color: #A8A8A8; border-color: #A8A8A8; border-radius: 8px;">CANCEL</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<meta name="user-id" content="{{ auth()->user()->id }}">


	@if (count($errors) > 0 && session()->get('Modal') == "Create")
		<script type="text/javascript">
			$( document ).ready(function() {
				$('#modal').modal('show');
			});
		</script>
	@elseif (count($errors) > 0 && session()->get('Modal') == "Edit")
		<script type="text/javascript">
			$( document ).ready(function() {
				var a = '#EditModal' + {!! session()->get('EditID') !!};
				$(a).modal('show');
			});
		</script>
	@endif
@endsection
