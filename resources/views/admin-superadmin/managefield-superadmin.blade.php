@extends('layouts-admin.main')

@section('content')
<style>
    .newtask{
        background-color: #4107BB;
        border: none;
        border-radius: 8px;
    }
</style>

<div class="row">
  <div class="col mt-5" style="margin-left: 10%;">
    <nav>
		<ul class="nav">
            <li class="nav-item">
                <h2 style="color: #4107BB; font-weight: 600;">Manage Field</h2>
                <h6 style="font-weight: 400;">Section that will be displayed on the employee’s task sheet form</h6>
			</li>
            {{-- <li class="nav-item" style="margin-left: 640px;">
                <a href="#" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">+ CREATE NEW FIELD</a>
			</li> --}}
        </ul>
    </nav>
  </div>
</div>

{{-- @php
    dd($tables);
@endphp --}}

@foreach ($tables as $table)
    @php
        $string = str_replace(' ', '', $table);
    @endphp
    <a href="/dashboard-managelist?name={{ $string }}" style="text-decoration: none;">
        <div class="container" >
            <div class="row" style="margin-top: 2%; margin-left:-35px;">
                <div class="card task" style="box-shadow:none; color:white; border-radius: 16px;">
                    <div class="card-body" style="display: flex;">
                        <div class="col-sm-5">
                            <p style="font-weight: 400; font-size: 36px; color: black; margin-top: 10px;">{{ $table }}</p>
                        </div>
                        {{-- <div class="col-sm-1" style="margin-top: 15px; margin-left:550px;">
                            <button button type="submit" class="btn" style="background-color: none; border: none; color: #ffffff;">
                                <img src="/image/edit.png" alt="search">
                            </button>
                        </div>
                        <div class="col-sm-1" style="margin-top: 15px; margin-left:-40px;">
                            <button button type="submit" class="btn" style="background-color: none; border: none; color: #ffffff;">
                                <img src="/image/delete-gray.png" alt="search">
                            </button>
                        </div>
                        <div class="col-sm-1" style="margin-top: 20px; margin-left:-30px;">
                            <button button type="submit" class="btn" style="background-color: none; border: none; color: #ffffff;">
                                <img src="/image/toggle.png" alt="search">
                            </button>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </a>
@endforeach

<meta name="user-id" content="{{ auth()->user()->id }}">

@endsection