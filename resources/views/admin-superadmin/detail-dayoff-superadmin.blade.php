@extends('layouts-admin.main')

@section('content')

{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

<style>
    .form-control{
        border: none!important;
        background-color: #F7F7F7;
        border-radius: 8px;
    }
    .select2-container--default .select2-selection--single{
        border: none;
        background-color: #F7F7F7;
        border-radius: 8px;
    }
</style>

<!-- Main content -->
<div class="content">
    <div class="container p-5" style="margin-bottom: 150px;">
        <div class="card card-default" style="margin-left: 160px; width: 70%; border-radius: 16px; border: none; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1);">
            <div class="card-body">

                @php 
                    $isUndo = true; 
                @endphp

                @foreach ($Task as $task)
                    <h1 class="ml-5" style="color: #FF6060; margin-top: 20px;">Day Off</h1>
                    <h5 class="ml-5" style="color: #A8A8A8; margin-top: 10px; font-weight: 200;"><em>{{ $task->created_at->format('d F Y') }}</em></h5>
                    <div class="container">
                        <div class="row mt-4" style="margin-left: 3%;">
                            <div class="col" style="margin-right:-7%;">
                                <form>
                                    <fieldset disabled>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Reason of Absence</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $task->name }}">
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Absence Remarks</label>
                                            <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->absence_remark }}</textarea>
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Description</label>
                                            <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->absence_description }}</textarea>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Approval Status</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $approval }}">
                                        </div>
                                    </fieldset>
                                </form> <br><br>
                            </div>
                        </div>
                    </div>

                    @php
                        $taskId = $task->id;
                        if ($approval == 'Not Approved') { 
                            $isUndo = false;
                        }
                    @endphp
                @endforeach

                <div class="container">
                    <ul class="nav justify-content-end" style="margin-right: 10%;">
                        @if ($Task->isEmpty() != true && auth()->user()->role_id == 3)
                            @if ($isUndo == true)
                                <form action="/dashboard-admin/approval-task/undo" method="post">
                                    @csrf
                                    <input type="hidden" name="taskId" value="{{ $taskId }}">
                                    <li>
                                        <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                                        style="background-color: #ffffff; color: #A8A8A8; border-color: #A8A8A8; border-radius: 8px;">UNDO ACTION</a>
                                    </li> &nbsp; &nbsp;
                                </form>
                            @else
                                <form action="/dashboard-admin/approval-task/approve" method="post">
                                    @csrf
                                    <input type="hidden" name="taskId" value="{{ $taskId }}">
                                    <li>
                                        <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                                        style="background-color: #00DFC7; color: #FFFFFF; border-color: #00DFC7; border-radius: 8px;">Approve</button>
                                    </li> &nbsp; &nbsp;
                                </form>

                                <form action="/dashboard-admin/approval-task/reject" method="post">
                                    @csrf
                                    <input type="hidden" name="taskId" value="{{ $taskId }}">
                                    <li>
                                        <button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                                        style="background-color: #FF6060; color: #FFFFFF; border-color: #FF6060; border-radius: 8px; margin-left: 10%;">Reject</button>
                                    </li>
                                </form>
                            @endif
                        @endif
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

@endsection
