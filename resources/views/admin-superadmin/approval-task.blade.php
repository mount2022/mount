@extends('layouts-admin.main')

@section('content')

<style>
.newtask{
	background-color: #4107BB;
	border: none;
	border-radius: 8px;
}
.newtask:hover{
    opacity: 80%;
    background-color: #4107BB;
}
.dayoff{
    background-color: #FF6060;
    border: none;
    border-radius: 8px;
}
.dayoff:hover{
    opacity: 80%;
    background-color: #FF6060;
}
.submit{
    background-color: #00DFC7;
    border: none;
    border-radius: 8px;
}
.submit:hover{
    opacity: 80%;
    background-color: #00DFC7;
}
.height {
    height: 100vh
}

.form {
    position: relative
}
.form .fa-search {
    position: absolute;
    top: 20px;
    left: 270px;
    color: #DEDEDE;
}
.form-input {
    height: 55px;
    text-indent: 10px;
    border-radius: 8px;
}
.form-input:focus {
    box-shadow: none;
	border-color: #DEDEDE;
}
.dot {
	height: 17px;
    width: 17px;
    border-radius: 100%;
    display: flex;
    margin-top: -29px;
    margin-left: 32px;
}
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
bottom: .5em;
}
/* .list{
	background-color: #F5F4FF;
} */
</style>

@php
	$isSession = false;
@endphp

<nav class="navbar" style="background-color: #F5F4FF; box-shadow:unset;">
    <div class="container" style="height: 40px; margin-left: 185px;">
        <div class="col-sm-1" style="margin-top: -10px; margin-right: -30px;">
          <h4 style="color: #4107BB; font-weight: 300; margin-left:-10%; font-size: 20px;">Select</h4>
        </div>

        
          <div class="col-sm-2" style="width: 12%; margin-top: 1px;">
              <form action="#" method="get">
              <div class="form-group">
                <select id="month" name="month" class="form-control select2" style="border: none; border-radius: 8px;">
                  <option selected disabled>Month</option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03">March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">Sertember</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
          </div>
          <div class="col-sm-2" style="width: 12%; margin-top: 1px;">
            <div class="form-group">
              <select id="year" name="year" class="form-control select2" style="border: none; border-radius: 8px;">
                <option selected disabled>Year</option>
                <?php 
                $year = date('Y');
                $min = $year - 10;
                $max = $year;
                for( $i=$max; $i>=$min; $i-- ) {
                echo '<option value='.$i.'>'.$i.'</option>';
                }?>
              </select>
            </div>
          </div>
          <div class="col" style="margin-top: -1%;">
            <button class="btn" style="background-color: #4107BB; color: #ffffff; border-radius: 8px;">
              <img src="/image/search.png" alt="search">
            </button>
          </div>
        </form>

        <ul class="nav justify-content-end" style="margin-right: 10px;">
          <li style="margin-top:-10%; font-weight: 500; font-size: 20px; color: #4107BB;">
			@if (session()->get( 'DATE' ) != null)
				{{ session()->get( 'DATE' ) }}
				@php
					session()->forget('DATE');
					$isSession = true;
				@endphp
			@else
				@php
					$isSession = false;
				@endphp
				{{ date("F Y") }}
			@endif
            
          </li>
        </ul>

    </div>
</nav>

<div class="container" style="padding-top: 30px;">
	@if (auth()->user()->role_id == 3)
		<nav>
			<ul class="nav">
				<li class="nav-item">
					{{-- SEELCT ALL --}}
					<input type="checkbox" id="checkAll" aria-label="Checkbox for following text input" 
					style="height: 20px; width: 20px; background-color: #DEDEDE; border-radius: 4px;"> 
				</li>
				&nbsp; &nbsp;
				<li class="nav-item">
					<p>Select All</p>
				</li>
				<li class="nav-item" style="margin-left: 700px;">
					<p style="color: #A8A8A8;">Quick actions</p>
				</li>
				&nbsp; &nbsp; &nbsp;
				<li style="margin-top: -15px;">
					<a href="#" id="approveAllSelectedRecord" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit">APPROVE SELECTED</a>
				</li>
				&nbsp; &nbsp;
				<li style="margin-top: -15px;">
					<a href="#" id="rejectAllSelectedRecord" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 dayoff">REJECT SELECTED</a>
				</li>
				&nbsp; &nbsp;
				{{-- <li style="margin-top: -15px;">
					<a href="#" id="exportAllSelectedRecord" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">EXPORT SELECTED</a>
					https://stackoverflow.com/questions/46298489/download-maatwebsite-excel-using-ajax-in-laravel
				</li> --}}
			</ul>
		</nav>

		<div class="dropdown-divider"></div>
	@endif


	<div class="row pt-3 pb-4">
		<div class="col-md-3">
			<div class="form">
				<form class="form" id="searchForm" method="GET" action="/dashboard-admin/approval">
					<input type="text" name="search" id="search" class="form-control form-input" placeholder="Find someone..."> 
					{{-- @csrf --}}
				</form>
				<i class="fa fa-search"></i> 
			</div>
		</div>
	</div>

	<!-- <div class="container"> -->
		<div class="row">
			<div class="col-12">
			<table id="dtOrderExample" class="table table-borderless">
				<thead>
				<tr style="color: #A8A8A8;">
					@if (auth()->user()->role_id == 3)
						<th scope="col"></th>
					@endif
					<th scope="col" style="font-weight: 400;">Employee Name</th>
					<th scope="col" style="font-weight: 400;">Unit</th>
					<th scope="col" style="font-weight: 400;">Status</th>
					<th scope="col" style="font-weight: 400;">Action</th>
					<th scope="col" style="font-weight: 400;">Approval status</th>
				</tr>
				</thead>
				<tbody>
					
				{{-- <a href="#" class="list">
					<tr>
						<td>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" aria-label="Checkbox for following text input" style="height: 20px; width: 20px; background-color: #DEDEDE; border-radius: 4px;">
						</div>
						</td>
						<td>
							<img src="https://picsum.photos/seed/picsum/200/300" width="40" height="40" class="rounded-circle">
							<div style="margin-top: -45px; margin-left: 55px;">
								<h5 style="color: #1F1F1F;">Adrian Putra</h5>
								<p style="color: #A8A8A8; margin-top: -7px;">ID1234</p>
							</div>
						</td>
						<td>Architecture</td>
						<td style="color: #00DFC7; font-weight: 500;">All Approved</td>
						<!-- <td>Pending</td> -->
						<td>
							<ul class="nav">
								<!-- <li>
									<a href="#" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" style="background-color: #ffffff; color: #00DFC7; border-color: #00DFC7; border-radius: 8px;">Approve</a>
								</li>
								&nbsp; &nbsp;
								<li>
									<a href="#" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" style="background-color: #ffffff; color: #FF6060; border-color: #FF6060; border-radius: 8px;">Reject</a>
								</li> -->
								<li>
									<a href="#" class="btn btn-sm btn-primary mb-3 pt-2 pb-2" style="padding-left: 42px; padding-right: 42px; background-color: #ffffff; color: #A8A8A8; border-color: #A8A8A8; border-radius: 8px;">UNDO ACTION</a>
								</li>
								&nbsp; &nbsp;
								<li>
									<a href="#" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" style="background-color: #ffffff; color: #4107BB; border-color: #4107BB; border-radius: 8px;">Export</a>
								</li>
							</ul>
						</td>
						<td>
							<div>
								<h5 style="color: #42A2D0; margin-left: 10px;">A</h5> <span class="dot" style="background-color: #42A2D0;"></span><h5 style="color: #1F1F1F; margin-top: -20px; margin-left: 65px; font-weight: 400;">All days approved</h5>
								<h5 style="color: #EF929E;">SA</h5> <span class="dot" style="background-color: #EF929E;"></span><h5 style="color: #1F1F1F; margin-top: -20px; margin-left: 65px; font-weight: 400;">All days approved</h5>
							</div>
						</td>
					</tr>
				</a> --}}
				
				@php
					$MONTH = date("m");
					$YEAR = date("Y");
					
					if(session()->get( 'MONTH' ) != null && session()->get( 'YEAR' ) != null && $isSession == true) {
						$MONTH = session()->get( 'MONTH' );
						$YEAR = session()->get( 'YEAR' );

						session()->forget('MONTH');
						session()->forget('YEAR');
					} 
				@endphp

				@foreach ($users as $user)

				{{-- @if ($user['status'] == "Pending")  --}}
					
				<a href="#" class="list">
					<tr>
						@if (auth()->user()->role_id == 3)
							<td>
								<div class="custom-control custom-checkbox">
									{{-- SEELCT MULTIPLE --}}
									<input type="checkbox" name="userIds" value="{{ $user['id'] }}" class="checkBoxClass" aria-label="Checkbox for following text input" style="height: 20px; width: 20px; background-color: #DEDEDE; border-radius: 4px;">
								</div>
							</td>
						@endif
						<td>
							@php
								$path = url('user/images/' . $user->picture);
							@endphp
							<img src="{{ $path }}" width="40" height="40" class="rounded-circle">							
							<div style="margin-top: -45px; margin-left: 55px;">
								{{-- <h5 style="color: #1F1F1F;">{{ $user->name }}</h5> --}}
								{{-- <a href="{{ url('/dashboard-admin-detail') }}"> --}}
								<a href="/dashboard/task-detail?id={{ $user['id'] }}&MONTH={{ $MONTH }}&YEAR={{ $YEAR }}" style="text-decoration:none;">
									<h5 style="color: #1F1F1F;">{{ $user->name }}</h5>
								</a>
								<p style="color: #A8A8A8; margin-top: -7px;">{{ $user->employee_id }}</p>
							</div>
						</td>
						<td>{{ $user['department']['name'] }}</td>
						<!-- <td style="color: #00DFC7; font-weight: 500;">All Approved</td> -->
						<td>{{ $user['status'] }}</td>
						<td>
							<ul class="nav">
								@if (auth()->user()->role_id == 3)
									@if ($user['status'] == "All Approved" || $user['trueStatus'] == "Rejected")
										<form action="/dashboard-admin/approval/undo" method="post">
											<input type="hidden" name="userId" value="{{ $user['id'] }}">
											<input type="hidden" name="month" value="{{ $MONTH }}">
											<input type="hidden" name="year" value="{{ $YEAR }}">
											@csrf
											<li>
												<button type="submit" class="btn btn-sm btn-primary mb-3 pt-2 pb-2" 
													style="padding-left: 42px; padding-right: 42px; background-color: #ffffff; color: #A8A8A8; 
													border-color: #A8A8A8; border-radius: 8px;">UNDO ACTION</a>
											</li>
										&nbsp; &nbsp;
										</form>
									@else
										<form action="/dashboard-admin/approval/approve" method="post">
											<input type="hidden" name="userId" value="{{ $user['id'] }}">
											<input type="hidden" name="month" value="{{ $MONTH }}">
											<input type="hidden" name="year" value="{{ $YEAR }}">
											@csrf
											<li>
												<button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
												style="background-color: #ffffff; color: #00DFC7; border-color: #00DFC7; border-radius: 8px;">Approve</button>
											</li>
										&nbsp; &nbsp;
										</form>
										<form action="/dashboard-admin/approval/reject" method="post">
											<input type="hidden" name="userId" value="{{ $user['id'] }}">
											<input type="hidden" name="month" value="{{ $MONTH }}">
											<input type="hidden" name="year" value="{{ $YEAR }}">
											@csrf
											<li>
												<button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
												style="background-color: #ffffff; color: #FF6060; 
												border-color: #FF6060; border-radius: 8px; margin-left: 12px;">Reject</button>
											</li>
										</form>
									@endif
								@endif
								&nbsp; &nbsp;
								<form action="/dashboard-admin/export" method="post">
									<input type="hidden" name="userId" value="{{ $user['id'] }}">
									<input type="hidden" name="userName" value="{{ $user['name'] }}">
									<input type="hidden" name="month" value="{{ $MONTH }}">
									<input type="hidden" name="year" value="{{ $YEAR }}">
									@csrf
									<li>
										<li>
											<button type="submit" class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" style="background-color: #ffffff; color: #4107BB; border-color: #4107BB; border-radius: 8px;">Export</button>
										</li>
									</li>
								</form>
							</ul>
						</td>
						<td>
							<div>
								{{-- @php
									if($user['status'] == 'Pending'){
										echo '<h5 style="color: #1F1F1F; margin-top: -20px; margin-left: 65px; font-weight: 400;">Several days not approved yet</h5>';
									}else{
										echo '<h5 style="color: #1F1F1F; margin-top: -20px; margin-left: 65px; font-weight: 400;">All days approved</h5>';
									}
								@endphp --}}

								@php
									$isAllApprove = true;
									$isAllRejected = false;
									$taskCount = 1;
									$rejectCount = 1;

									foreach ($user->tasks as $task) {
										foreach ($task->approvals as $approval) {
											$taskCount++;
											if($approval->approval_status == 'Not Approved') {
												$isAllApprove = false;
											}

											if($approval->approval_status == 'Rejected') {
												$rejectCount++;
											}
										}
									}
									
									if($taskCount == $rejectCount) {
										$isAllRejected = true;
									}
								@endphp
								
								@if ($isAllApprove == false)
									<h5 style="color: #1F1F1F; margin-top: 5px; font-weight: 400;">Several days not approved yet</h5>
								@else
									@if ($isAllRejected == false)
										<h5 style="color: #1F1F1F; margin-top: 5px; font-weight: 400;">All days approved</h5>
									@else
										<h5 style="color: #1F1F1F; margin-top: 5px; font-weight: 400;">All days rejected</h5>
									@endif
								@endif
								
								<!-- <h5 style="color: #1F1F1F; margin-top: 5px; font-weight: 400;">All days approved</h5> -->
							</div>
						</td>
					</tr>
				</a>
					{{-- @endif --}}
				@endforeach

			</tbody>
		</table>
	</div>
</div>
<!-- </div> -->

<meta name="user-id" content="{{ auth()->user()->id }}">

	<script>
		/*document.getElementById('body').onkeyup = function(e) {
			if (e.keyCode === 13) {
				document.getElementById('searchForm').submit();
				document.getElementById('searchForm').reset();
			}

			return true;
		}*/

		$(document).ready(function() {
			// Ajax Multiple Select 
			$("#checkAll").click(function() {
				$(".checkBoxClass").prop('checked', $(this).prop('checked'));
			});

			// Approve
			$("#approveAllSelectedRecord").click(function(e) {
				e.preventDefault();
				var allIds = [];

				$("input:checkbox[name=userIds]:checked").each(function() {
					allIds.push($(this).val());
				});

				$.ajax({
					url:"{{ route('approval.selected') }}",
					type:"POST",
					data:{
						_token	:	$("input[name=_token]").val(),
						ids		: 	allIds,
						month	: 	{{ $MONTH }},
						year	: 	{{ $YEAR }},
					},
					success:function(response) {
						console.log('Masuk');
						$('input[type=checkbox]').prop('checked', false);
						location.reload();
					}
				});
			});

			// Reject
			$("#rejectAllSelectedRecord").click(function(e) {
				e.preventDefault();
				var allIds = [];

				$("input:checkbox[name=userIds]:checked").each(function() {
					allIds.push($(this).val());
				});

				$.ajax({
					url:"{{ route('reject.selected') }}",
					type:"POST",
					data:{
						_token	:	$("input[name=_token]").val(),
						ids		:	allIds,
						month	: 	{{ $MONTH }},
						year	: 	{{ $YEAR }},
					},
					success:function(response) {
						$('input[type=checkbox]').prop('checked',false);
						location.reload();
					}
				});
			});	
		});
	</script>


</div>

@endsection