@extends('layouts-admin.main')

@section('content')
<style>
    .newtask{
        background-color: #4107BB;
        border: none;
        border-radius: 8px;
    }
    .task:hover{
    opacity: 50%;
    background: white;
  }
</style>

<div class="row">
  <div class="col mt-5" style="margin-left: 10%;">
    <nav>
		<ul class="nav">
            <li class="nav-item">
                <h2 style="color: #4107BB; font-weight: 600;">Manage Field</h2>
                <h6 style="font-weight: 400;">Section that will be displayed on the employee’s task sheet form</h6>
			</li>
        </ul>
    </nav>
  </div>
</div>

<a href="#" style="text-decoration: none;">
    <div class="container" >
        <div class="row" style="margin-top: 2%; margin-left:-35px;">
            <div class="card task" style="box-shadow:none; color:white; border-radius: 16px;">
                <div class="card-body" style="display: flex;">
                    <div class="col-sm-5">
                        <p style="font-weight: 400; font-size: 36px; color: black; margin-top: 10px;">Project name</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>

@endsection