@extends('layouts-admin.main')

@section('content')
  <style>
    .newtask{
      background-color: #4107BB;
      border: none;
      border-radius: 8px;
    }

    .submit{
      background-color: #00DFC7;
      border: none;
      border-radius: 8px;
    } 

    .submit:hover{
      opacity: 80%;
      background-color: #00DFC7;
    }

    /* The Modal (background) */
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
      background-color: #ffffff;
      margin-top: 150px;
      margin-left: 400px;
      padding: 20px;
      border: 1px solid #888;
      width: 55%;
      border-radius: 16px;
      box-shadow: 0px 2px 40px rgba(0, 0, 0, 0.15);
    }

    /* The Close Button */
    .close {
      margin-left: 95%;
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover, .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }

    .new-option{
      border: none;
      border-radius: 8px;
      background: #F7F7F7;
      padding: 15px;
      width: 50rem;
      margin-left: 3rem;
      text-align: center;
    }

    table td {
      overflow: hidden;
    }
  </style>

  <div class="row">
    <div class="col mt-5" style="margin-left: 10%;">
      <nav>
        <ul class="nav">
          <li class="nav-item">
            <h2 style="color: #4107BB; font-weight: 600;">List of {{ $name }}</h2>
            <h6 style="font-weight: 400;">Manage Field / {{$name }}</h6>
          </li>
          <li class="nav-item" style="margin-left: 815px;"> 
            <a href="#" id="myBtn" class="btn btn-sm btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">+ ADD NEW OPTION</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  @if(request()->get('name') == 'TaskName')
    <div class="container" >
      <div class="row mt-4">
        <div class="card task" style="margin-left:-25px; background-color: #F5F4FF; box-shadow:none; color:white; border-radius: 16px; border: none;">
          <div class="card-body" style="display: flex;">
            <nav class="navbar" style=" box-shadow:unset;">
              <p style="color: black; margin-top: 10px;"><strong>Please select project and department first!</strong></p>
              <form action="/dashboard-managelist" method="get">

                <div class="row" style="margin-left: 5%; padding: 0px;">
                  <div class="col" style="width: 350px;">
                    <select id="project" name="project" class="form-control select2" style="border: none; border-radius: 8px;">
                      <option selected disabled>Project</option>
                      @foreach($Projects as $project)
                        <option value="{{ $project->id }}"  {{ request()->get('project') == $project->id ? 'selected' : '' }}>{{ $project->name }}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col" style="padding: 0px;">
                    <select id="department" name="department" class="form-control select2" style="border: none; border-radius: 8px;">
                      <option selected disabled>Department</option>
                      @foreach($Departments as $department)
                        <option value="{{ $department->id }}" {{ request()->get('department') == $department->id ? 'selected' : '' }}>{{ $department->name }}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col">
                    <input type="hidden" name="name" id="name" value="{{ request()->get('name') }}">
                    <button class="btn" type="submit" style="background-color: #4107BB; color: #ffffff; border-radius: 8px;">
                      <img src="/image/search.png" alt="search">
                    </button>
                  </div>
                </div>

              </form>
            </nav>
          </div>
        </div>
      </div>
    </div>
  @endif

  <div class="container" style="margin-top: 2%;">
    @if(request()->get('name') == 'TaskName') {{-- Task Name  --}}
      @if($datas != null) {{-- Task Name With Search --}}
        <table id="dtOrderExample" class="table table-borderless">
          <thead>
            <tr style="color: #A8A8A8;">
              <th scope="col-sm-3" style="font-weight: 400;">ID</th>
              <th scope="col" style="font-weight: 400;">Project Name</th>
              <th scope="col-sm-5" style="font-weight: 400;">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($datas as $data)
              <tr>
                <td width="5%">{{ $data->id }}</td>
                <td width="80%">{{ $data->name }}</td>
                <td>
                  <ul class="nav">
                    <li>
                      <form action="/dashboard-managelist/delete" method="POST"> @csrf
                        <input type="hidden" id="id" name="id" value="{{ $data->id }}">
                        <input type="hidden" id="fieldName" name="fieldName" value="{{ request()->get('name') }}">
                        <button type="submit" onclick="return confirm('Are you sure to delete?')"  class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                        style="background-color: #ffffff; color: #FF6060; border-color: #FF6060; border-radius: 8px;">Delete</button>
                      </form>
                    </li>
                  </ul>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      @endif
    @else {{-- Not Task Name --}}
      <table id="dtOrderExample" class="table table-borderless">
        <thead>
          <tr style="color: #A8A8A8;">
            <th scope="col-sm-3" style="font-weight: 400;">ID</th>
            <th scope="col" style="font-weight: 400;">Project Name</th>
            <th scope="col-sm-5" style="font-weight: 400;">Action</th>
          </tr>
        </thead>
        <tbody>
          @php
            $number = 1;
          @endphp
          @foreach ($datas as $data)
            @if($data->name != 'Other, Please mention in Absence Remark')
              <tr>
                <td width="5%">{{ $number++ }}</td>
                <td width="80%">{{ $data->name }}</td>
                <td>
                  <ul class="nav">
                    <li>
                      <form action="/dashboard-managelist/delete" method="POST"> @csrf
                        <input type="hidden" id="id" name="id" value="{{ $data->id }}">
                        <input type="hidden" id="fieldName" name="fieldName" value="{{ request()->get('name') }}">
                        <button type="submit" onclick="return confirm('Are you sure to delete?')"  class="btn btn-sm btn-primary mb-3 pl-3 pr-3 pt-2 pb-2" 
                        style="background-color: #ffffff; color: #FF6060; border-color: #FF6060; border-radius: 8px;">Delete</button>
                      </form>
                    </li>
                  </ul>
                </td>
              </tr>
            @endif
          @endforeach
        </tbody>
      </table>
    @endif
  </div>

  <!-- Add New Option Modal -->
  <div id="myModal" class="modal">
    <div class="modal-content">
      <span class="close">&times;</span>
      <h3 style="color: #4107BB; text-align: center">Add New Option</h3>
      <p style="text-align: center">This option will be displayed on the dropdown list of the field</p>

      <form action="/dashboard-managelist/create" method="POST"> @csrf
        <div class="form-group">
          <input class="new-option" type="text" id="name" name="name" placeholder="New Option...">
          <input type="hidden" id="fieldName" name="fieldName" value="{{ request()->get('name') }}">
          @if(request()->get('name') == 'TaskName')
            <input type="hidden" id="project_id" name="project_id" value="{{ request()->get('project') }}">
            <input type="hidden" id="department_id" name="department_id" value="{{ request()->get('department') }}">
          @endif
        </div>

        <div class="col">
          <button type="submit" id="" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="margin-left: 23rem;">ADD TO LIST</button>
        </div>
      </form>

      
    </div>
  </div>

  <meta name="user-id" content="{{ auth()->user()->id }}">

  <script>
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    btn.onclick = function() {
      modal.style.display = "block";
    }
    span.onclick = function() {
      modal.style.display = "none";
    }

    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  </script>
@endsection