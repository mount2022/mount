<table>
    <thead>
        <tr>
            <th>Date</th>
            <th>Total Average Hours</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{ $Date->format('F Y') }}</td>
            <td>{{ sprintf('%0.2f', $totalAverage) }} hours/day</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th>Employee Name</th>
            <th>Employee ID</th>
            <th>Unit</th>
            <th>Working Hours</th>
            <th>Working Days</th>
            <th>Average Hours</th>
        </tr>
    </thead>
    <tbody>
        @foreach($User as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->employee_id }}</td>
                <td>{{ $user->department_id }}</td>
                <td>{{ $user->hour }} hours</td>
                <td>{{ $user->days }} days</td>
                <td>{{ $user->average }} hours/day</td>
            </tr>
        @endforeach
    </tbody>
</table>