<table>
    <thead>
    <tr>
        <th>Total Working Days</th>
        <th>Total Working Hours</th>
    </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{ $working_days }} Days</td>
            <td>{{ $hours }} Hours</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr>
        <th>ID Number</th>
        <th>Name</th>
        <th>Project Name</th>
        <th>Department</th>
        <th>Date</th>
        <th>Start Time</th>
        <th>Finish Time</th>
        <th>Total Time</th>
        <th>Task Name</th>
        <th>Absence</th>
        <th>Additional Note</th>
        <th>Task Remarks</th>
        <th>Task Completion Status</th>
        <th>Approval Status</th>
        <th>Type Of Work</th>
        <th>Site Name</th>
        <th>Other Site Name</th>
        <th>Transportation</th>
        <th>Kind Of Transportation</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tasks as $data)
    {{-- @php
        dd($data);
    @endphp --}}
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->user_id }}</td>
            <td>{{ $data->project_id }}</td>
            <td>{{ $data->dep }}</td>

            @php
                $time = \Carbon\Carbon::parse($data->created_at)->format('d F Y');
            @endphp
            <td>{{ $time }}</td>
            <td>{{ $data->start_time }}</td>
            <td>{{ $data->finish_time }}</td>
            <td>{{ $data->duration }}</td>
            <td>{{ $data->name }}</td>
            
            @if ($data->absence_id!= null)
                <td>YES</td>
            @else
                <td>-</td>
            @endif

            @if ($data->daily_task_remark != null)
                <td>{{ $data->daily_task_remark }}</td>
            @else
                <td>-</td>
            @endif
            
            @if ($data->daily_task_remark != null)
                <td>{{ $data->daily_task_remark }}</td>
            @else
                <td>-</td>
            @endif

            <td>{{ $data->task_status }}</td>
            <td>{{ $data->approval_status }}</td>
            <td>{{ $data->type_of_work_id }}</td>
            <td>{{ $data->site_id }}</td>
            <td>-</td>

            @if ($data->transportation_id != null)
                <td>Yes</td>
            @else
                <td>No</td>
            @endif
            
            <td>{{ $data->transportation_id }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
