@extends('layouts.main')

@section('content')

<style>
    .scroll {
        max-height: 200px;
        overflow: scroll;
        margin-bottom: 20px;
        max-width: 100%;
        overflow-x: hidden;
		overflow-y: auto;
    }
	
	.test:hover{
		color:#4107BB;
		background: #F7F9FF;
	}

</style>

<div class="row">
	<div class="col mt-5" style="margin-left: 10%;">
		<h5 style="color: #4107BB"> Good day,</h5>
		<h2 style="color: #4107BB; font-weight: 600;"> {{ auth()->user()->name }}</h2>
	</div>

	<!--<div class="col mt-5" style="text-align:right; margin-right: 10%;">
		<a href="/new-task" style="text-decoration: none">
			<button class="btn btn-login text-uppercase" style="background: #4107BB; font-weight: 600; color: white; border-radius: 8px">+ ADD NEW TASK</button>
		</a>
	</div>-->

</div>

<div class="row">
	<div class="col" style="margin-left: 10%; margin-top: 3%;">
		<div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;">
			<div class="card-body" style="padding: 5%;">
				<h5 style="font-weight: 500; margin-bottom: 10%"> Work time progress this week </h5>

				{{-- <img src="/image/worktime-chart-temporary.png" width="100%" alt="logo" style="margin-bottom: 5%">  --}}
				<div class="chart">
					<canvas id="barChart" style="min-height: 250px; height: 500px; max-height: 460px; max-width: 100%;"></canvas>
				</div>

				<div class="dropdown-divider"></div>
				<div class="text-muted" style="margin-top: 5%; text-align:center"> 
					<span style="margin-right: 5%"> <i class="fa fa-circle font-10 m-r-10" style="color: #00DFC7;"></i> >= 8 hours </span>
					<span style="margin-right: 5%"> <i class="fa fa-circle font-10 m-r-10" style="color: #FCA010;"></i> < 8 hours </span>
					<span style="margin-right: 5%"> <i class="fa fa-circle font-10 m-r-10" style="color: #FF6060;"></i> Day off </span>
					<span> <i class="fa fa-circle font-10 m-r-10" style="color: #EBEBEB"></i> No data </span>
				</div>

			</div>
		</div>
	</div>

	@php
		function formatMinutes($minutes) {
				$now = \Carbon\Carbon::now();
				$then = $now->copy()->addMinutes($minutes);
				$hours = $now->diffInHours($then);
				$minutes = $now->diffInMinutes($then) - ($hours * 60);

				$total['hours'] = $hours;
				$total['minutes'] = $minutes;
				return ($total);
			}

			$totalMinutes = 0;

			foreach ($tasks as $task) {
				if ($task->absence_id == null) {
					$start = new \Carbon\Carbon($task->start_time);
					$end   = new \Carbon\Carbon($task->finish_time);
					$minutesToday  = $start->diffInMinutes($end);
					$totalMinutes += $minutesToday;
				}
			}
	@endphp

	<div class="col" style="margin-right: 10%; margin-top: 3%;">
		<div class="row justify-content-center">
			<div class="col">
				<div class="card card-default border-0" style="background: linear-gradient(270deg, #00DFC7 0%, #42A2D0 127.23%); color:white; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;">
					<div class="card-body" style="padding: 10%;">
						<h5 style="font-weight: 500;"> Total hours this month </h5>
						@php 
							$TOTAL = formatMinutes($totalMinutes); 
						@endphp
						<h1 class="fw-bold mt-5"> {{ $TOTAL['hours'] }} <a style="font-weight: normal;"> hours <a style="font-weight: normal; opacity: 0.6;"> & </a> </h1>
						<h1 class="fw-bold mb-5"> {{ $TOTAL['minutes'] }} <a style="font-weight: normal;"> min. </a> </h1>
					</div>
				</div>
			</div>  

			<div class="col">
				<a href="/dashboard/task?date={{ date('Y-m-d') }}" style="text-decoration: none">
					<button class="card card-default border-0" style="background: #F7F9FF; color:#4107BB; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px; width:100%; text-align:center">
						<div class="card-body" style="padding: 10%; width:100%">
							<img src="/image/plus-circle.png" width="51%" alt="logo">
							<h4 class="fw-bold mt-3"> Create new task </h4>
							<h5 class="mb-3"> for today </h5>
						</div>
					</button>
				</a>
			</div>
		</div>

		<div class="row" style="margin-top: 1.3%; margin-left: 1px; margin-right: 0.5px;  height: 375px;">
			<div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px">
				<div class="card-body" style="padding: 0px">
					<div class="row mt-2" style="font-weight: 500; padding-top: 3%">
						<h5>Notification</h5>
						<div class="dropdown-divider"></div>
					</div>
					
						@if ($notifications->isEmpty())
							<div class="row" style="color: #A8A8A8; text-align:center;  padding: 50px 0;">
								<h5 class="mt-4" style="margin-left: 3%; font-size: 18px;">No Notification</h5>
							</div>
						@else
							<div class="scroll">
								@foreach ($notifications->reverse() as $notification)
									{{-- <div class="row" style="background: #F7F9FF; color: #4107BB;"> --}}
									<div class="row test" style="width: 100%;">
										<h5 class="mt-4" style="margin-left: 3%; font-size: 18px; ">{{ $notification->body }}</h5>
										<span class="mb-4" style="margin-left: 3%">{{ $notification->time_created }}</span>
									</div>
								@endforeach
							</div>
						@endif

						{{-- <div class="row">
							<h5 class="mt-4" style="margin-left: 3%; font-size: 18px;">Your record for 2 December is approved by Admin</h5>
							<span class="mb-4" style="margin-left: 3%">Friday, 3 December 2021, 12.30</span>
						</div> 
						<div class="row">
							<h5 class="mt-4" style="margin-left: 3%; font-size: 18px;">Your record for 2 December is approved by Admin</h5>
							<span class="mb-4" style="margin-left: 3%">Friday, 3 December 2021, 12.30</span>
						</div> --}}
					
					<div class="row" style="font-weight:600; color:#A8A8A8; text-align:center; position: absolute; bottom: 0; width: 100%;">
						<div class="dropdown-divider"></div>
						<a href="/dashboard/profile/notification" style="text-decoration: none">
							<h6 class="mt-3 mb-4" style="font-weight:600; color:#A8A8A8;">SEE ALL NOTIFICATIONS</h6>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<meta name="user-id" content="{{ auth()->user()->id }}">

<script>
	$(function () {
		// console.log(@json($hours));
		/* ChartJS
		* -------
		* Here we will create a few charts using ChartJS
		*/

		//-------------
		//- CHART DATA -
		//-------------
		const Data = @json($hours);
		const backgroundcolor = [];

		for (i = 0; i < Data.length; i++) {
			if(Data[i] >= 8) { 
				backgroundcolor.push('#00DFC7') 
			} else if(Data[i] < 8 && Data[i] != 0.5) { 
				backgroundcolor.push('#FCA010') 
			} else if(Data[i] == 0.5) { 
				backgroundcolor.push('#FF6060') 
			} else {
				backgroundcolor.push('white') 
			}
		}
		
		var areaChartData = {
			labels  : @json($one_week_date),
			datasets: [
				{
					label               : 'Work Hours',
					borderColor         : 'rgba(60,141,188,0.8)',
					pointRadius         : false,
					pointColor          : '#3b8bba',
					pointStrokeColor    : 'rgba(60,141,188,1)',
					pointHighlightFill  : '#fff',
					pointHighlightStroke: 'rgba(60,141,188,1)',
					data                : @json($hours),
					backgroundColor		: backgroundcolor, 
				}
				
			]
		}

		//-------------
		//- BAR CHART -
		//-------------
		var barChartCanvas = $('#barChart').get(0).getContext('2d')
		var barChartData = $.extend(true, {}, areaChartData)
		var temp0 = areaChartData.datasets
		barChartData.datasets = temp0

		var barChartOptions = {
			responsive              : true,
			maintainAspectRatio     : false,
			datasetFill             : false,
			scales: {
				xAxes: [{
					gridLines: {
						color: "rgba(0, 0, 0, 0)",
					}
				}],
				yAxes: [{
					gridLines: {
						color: "rgba(0, 0, 0, 0)",
					},
					scaleLabel: {
						display: true,
						labelString: 'Hours'
					},
					ticks: {
						suggestedMin: 0,
						suggestedMax: 10,
					} 
				}]
			},
			legend:
			{
				display: false
			}
		}

		new Chart(barChartCanvas, {
			type: 'bar',
			data: barChartData,
			options: barChartOptions
		})
	})
</script>

@endsection