<style>
.dropdown-item.active, .dropdown-item:active {
    color: #fff;
    background-color: #4107BB;
}
</style>

<nav class="navbar navbar-light">
  <div class="container">
    <a class="navbar-brand" href="#">
      <img src="/image/mount-mht-logo.png" alt="" width="70%">
    </a>

    <ul class="nav justify-content-end">
      <li class="nav-item">
        <a class="nav-link" href="/dashboard-admin">Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/dashboard-admin/approval">Approvals</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/dashboard-averageHours">Average Hours</a>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Manage
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
            <li><a class="dropdown-item" href="/dashboard-admin/employee">Manage Employee</a></li>
            @if(auth()->user()->role_id == 3)
              <li><a class="dropdown-item" href="/dashboard-managefield">Manage Field</a></li>
            @endif
          </ul>
      </li>

      <!-- <div id="app">
        <notification-component></notification-component>
      </div> -->

      {{-- <li class="nav-item dropdown">
        <a class="nav-link icon-bell" href="#" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-bottom: none;">
          <img src="/image/bell.png" alt="">
        </a>
        <ul class="dropdown-menu" style="right: 0; left: auto; width: 300px; border-radius: 8px; top: 120%; border: none;" aria-labelledby="navbarDropdownMenuLink">
            <div class="profile">Notifications</div>

            <div class="dropdown-divider"></div>

            <div class="profile-details">
              <div style="padding: 15px;">
                <h5>Task Submission Approval</h5>
                <p>Your record for 2 December is approved by Super Admin</p>
                <p>3 Dec, 15.00</p>
              </div>
            </div>

            <div class="dropdown-divider"></div>

            <div style="text-align: center; padding: 2%;">
              <a href="#" type="button" class="btn btn-primary btn-sm profile-settings">
                SEE ALL NOTIFICATION
              </a>
            </div>
          </ul>
      </li> --}}

      <li class="nav-item dropdown">
        @php
            $path = url('user/images/' . auth()->user()->picture);
        @endphp
          <a class="nav-link dropdown-toggle" href="#" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-bottom: none;">
            <img src="{{ $path }}" width="25" height="25" class="rounded-circle">
          </a>
          <ul class="dropdown-menu" style="right: 0; left: auto; width: 300px; border-radius: 8px; top: 120%; border: none;" aria-labelledby="navbarDropdownMenuLink">
            <div class="profile">My Profile</div>

            <div class="dropdown-divider"></div>

            <div class="profile-details">
              <img src="{{ $path }}" width="65" height="65" class="rounded-circle">
              <div style="margin-bottom: -15%">
                <h5 class="name">{{ auth()->user()->name }}</h5>
                <p class="position">{{ \App\Models\Department::where('id', Auth::user()->department_id)->first()->name }}</p>
                <p class="company">PT. Meinhardt EPCM Indonesia</p>
              </div>
            </div>

            <div class="dropdown-divider"></div>

            <div style="text-align: center; padding: 2%;">
              <a href="/dashboard/profile" type="button" class="btn btn-primary btn-sm profile-settings">
                <i class="fas fa-user-cog"></i> ACCOUNT SETTINGS
              </a>
              <br>
              <a href="/dashboard" type="button" class="btn btn-primary btn-sm profile-settings">
                <i class="fas fa-user-cog"></i> Employee Page
              </a>
              <br>
              {{-- <a href="#" type="submit" class="btn btn-primary btn-sm profile-logout">LOG OUT</a> --}}
              <form action="/logout" method="post">
                  @csrf
                  <button type="submit" class="btn btn-primary btn-sm profile-logout">LOG OUT</button>
              </form>
            </div>
          </ul>
      </li>
    </ul>

  </div>
</nav>