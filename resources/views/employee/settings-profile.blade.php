@extends('layouts.main')

@section('content')

<style>
    .picture-container {
        position: relative;
        cursor: pointer;
        text-align: center;
    }
    .picture {
        width: 106px;
        height: 106px;
        background-color: #999999;
        border: 4px solid #CCCCCC;
        color: #FFFFFF;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        transition: all 0.2s;
        -webkit-transition: all 0.2s;
    }
    .picture:hover {
    border-color: #2ca8ff;
    }
    .content.ct-wizard-green .picture:hover {
        border-color: #05ae0e;
    }
    .content.ct-wizard-blue .picture:hover {
        border-color: #3472f7;
    }
    .content.ct-wizard-orange .picture:hover {
        border-color: #ff9500;
    }
    .content.ct-wizard-red .picture:hover {
        border-color: #ff3b30;
    }
    .picture input[type="file"] {
        cursor: pointer;
        display: block;
        height: 100%;
        left: 0;
        opacity: 0 !important;
        position: absolute;
        top: 0;
        width: 100%;
    }
    .picture-src {
        width: 100%;

    }
    .dropdown-item{
        color: #A8A8A8;
    }
    .dropdown-item:hover{
        background-color: #F7F9FF;
        color: #4107BB;
    }
    img.icon:hover, img.icon:focus{
        filter: invert(11%) sepia(98%) saturate(5938%) hue-rotate(263deg) brightness(70%) contrast(115%);
    }
    .form-control:disabled, .form-control[readonly]{
        background-color: #F7F9FF;
    }
</style>

<div class="row">
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:35%; height:100%; left:30%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <br>

                <!-- profile picture -->
                <div class="container">
                    <div class="picture-container">                        
                        <div class="picture">
                            @php
                                $path = url('user/images/' . auth()->user()->picture);
                            @endphp
                            <img src="{{ $path }}" class="picture-src" id="wizardPicturePreview" title="">
                            <input type="file" id="wizard_picture" name="wizard_picture" class="">
                        </div>
                    </div>
                </div>
                <!-- profile picture -->

                <br>
                <!-- sidebar button -->
                <div class="row">
                    <a class="nav-link {{ Request::is('dashboard/profile') ? 'active' : '' }}" href="/dashboard/profile">
                        <img class="icon" src="/image/user.png" width="10%" alt=""> &nbsp; My Profile
                    </a>
                    <a class="nav-link" href="/dashboard/profile/change-password">
                        <img class="icon" src="/image/lock.png" width="10%" alt=""> &nbsp; Change Password
                    </a>
                    <a class="nav-link" href="/dashboard/profile/notification">
                        <img class="icon" src="/image/notif.png" width="10%" alt=""> &nbsp; Notification
                    </a>
                    <a class="nav-link" href="/dashboard/profile/kpi">
                        <img class="icon" src="/image/kpi.png" width="10%" alt=""> &nbsp; Key Performance Indicator
                    </a>
                    <a class="nav-link" href="/dashboard/profile/jobdesk">
                        <img class="icon" src="/image/jobdesc.png" width="10%" alt=""> &nbsp; Job Description
                    </a>
                    <a class="nav-link" href="/dashboard/profile/training">
                        <img class="icon" src="/image/training.png" width="10%" alt=""> &nbsp; Training Plan
                    </a>
                </div>
                <!-- sidebar button -->
                {{-- <div class="row">
                    <a class="nav-link {{ Request::is('dashboard/profile') ? 'active' : '' }}" href="/dashboard/profile">
                        <img class="icon" src="/image/user.png" width="10%" alt=""> &nbsp; My Profile
                    </a>
                    <a class="nav-link {{ Request::is('dashboard/profile/change-password') ? 'active' : '' }}" href="/dashboard/profile/change-password">
                        <img class="icon" src="/image/lock.png" width="10%" alt=""> &nbsp; Change Password
                    </a>
                    <a class="nav-link {{ Request::is('/dashboard/profile/notification') ? 'active' : '' }}" href="/dashboard/profile/notification">
                        <img class="icon" src="/image/notif.png" width="10%" alt=""> &nbsp; Notification
                    </a>
                </div> --}}
                <div class="row" style="margin-left: 5%; margin-top: 100%;">
                    <form action="/logout" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-sm profile-logout">LOG OUT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- personal info -->
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:100%; height:100%; left:-35%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <h4>Personal Info</h4>
                @foreach ($users as $user)
                <div class="row" style="margin-top: 5%;">
                    <form>
                        <fieldset disabled>
                            <div class="form-group" style="width: 70%;">
                                <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Full Name</label>
                                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $user->name }}">
                            </div>
                            <div class="form-group" style="width: 70%;">
                                <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Phone Number</label>
                                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $user->phone }}">
                            </div>
                            <div class="form-group" style="width: 70%;">
                                <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Home Address</label>
                                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $user->address }}">
                            </div>
                            <div class="form-group" style="width: 70%;">
                                <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Place, Date of Birth</label>
                                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $user->place_of_birth }}, {{ \Carbon\Carbon::parse($user->date_of_birth)->format('d F Y')}}">
                            </div>
                            <div class="form-group" style="width: 20%;">
                                <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Gender</label>
                                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $user->gender }}">
                            </div>
                        </fieldset>
                    </form>
                 </div>

            </div>
        </div>
    </div>
</div>


<!-- company info -->
<div class="row" style="width: 51%;margin-left: 50%;margin-top: 5%; padding-bottom: 7rem;">
<div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:100%; height:100%; left:-35%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <h4>Company Info</h4>

                <img src="/image/company-info.png" alt="" style="margin-top: 3%;margin-bottom: 5%;">

                <div class="row">
                    <div class="col">
                        <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Employee ID</label>
                        <h5 style="font-size: 18px;">{{ $user->employee_id }}</h5>
                    </div>

                    <div class="col" style="margin-left: -5%;">
                        <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Work Email (for Login)</label>
                        <h5 style="font-size: 18px;">{{ $user->email }}</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Department</label>
                        <h5 style="font-size: 18px;">{{ \App\Models\Department::where('id',Auth::user()->department_id)->first()->name }}</h5>
                    </div>

                    <div class="col" style="margin-left: -5%;">
                        <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Position</label>
                        <h5 style="font-size: 18px;">{{ $user->position }}</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Join Date</label>
                        <h5 style="font-size: 18px;">{{ \Carbon\Carbon::parse($user->join_date)->format('d F Y')}}</h5>
                    </div>

                    <div class="col" style="margin-left: -5%;">
                        <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Employment Status</label>
                        <h5 style="font-size: 18px;">{{ $user->employment_status }}</h5>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

<script>

    $(document).ready(function(){
        $("#wizard_picture").change(function(){});

        $('#wizard_picture').ijaboCropTool({
            preview : '',
            setRatio: 1,
            allowedExtensions: ['jpg', 'jpeg','png'],
            buttonsText: ['CROP','QUIT'],
            buttonsColor: ['#30bf7d','#ee5155', -15],
            processUrl: '{{ route("crop") }}',
            withCSRF: ['_token', '{{ csrf_token() }}'],
            onSuccess: function(message, element, status){
                // alert(message);
                console.log(message, element, status);
                location.reload();
            },
            onError: function(message, element, status){
                // alert(message);
                console.log(message, element, status);
            }
        });

    });
</script>

@endsection


