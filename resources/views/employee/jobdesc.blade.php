@extends('layouts.main')

@section('content')

<style>
    .picture-container {
        position: relative;
        cursor: pointer;
        text-align: center;
    }
    .picture {
        width: 106px;
        height: 106px;
        background-color: #999999;
        border: 4px solid #CCCCCC;
        color: #FFFFFF;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        transition: all 0.2s;
        -webkit-transition: all 0.2s;
    }
    .picture:hover {
    border-color: #2ca8ff;
    }
    .content.ct-wizard-green .picture:hover {
        border-color: #05ae0e;
    }
    .content.ct-wizard-blue .picture:hover {
        border-color: #3472f7;
    }
    .content.ct-wizard-orange .picture:hover {
        border-color: #ff9500;
    }
    .content.ct-wizard-red .picture:hover {
        border-color: #ff3b30;
    }
    .picture input[type="file"] {
        cursor: pointer;
        display: block;
        height: 100%;
        left: 0;
        opacity: 0 !important;
        position: absolute;
        top: 0;
        width: 100%;
    }
    .picture-src {
        width: 100%;

    }

    .dropdown-item{
        color: #A8A8A8;
    }
    .dropdown-item:hover{
        background-color: #F7F9FF;
        color: #4107BB;
    }
    img.icon:hover, img.icon:focus{
        filter: invert(11%) sepia(98%) saturate(5938%) hue-rotate(263deg) brightness(70%) contrast(115%);
    }
    .column-title{
        background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%), linear-gradient(0deg, #A8A8A8, #A8A8A8);
        color: white;
        height: 3.2rem;
    }
    th.column {
        font-size:12px;
        text-align: center;
        color: white;
        background: #A8A8A8;
    }

    .textarea-title {
        background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%), linear-gradient(0deg, #A8A8A8, #A8A8A8);
        height: 3.2rem;
        color: white;
        padding: 2%;
        margin: 0px;
    }

    .text-area {
        width: 100%;
        margin: 0px;
        padding: 0px;
        height: 100%;
    }

    .ck-editor__editable {
        min-height: 200px !important;
        line-height: 0.5;
    }
</style>

{{-- CKEditor --}}
<script src="https://cdn.ckeditor.com/ckeditor5/31.1.0/classic/ckeditor.js"></script>

<div class="row" style="padding-bottom: 20%;">
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:35%; height:100%; left:30%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <br>
                <!-- profile picture -->
                <div class="container">
                    <div class="picture-container">                        
                        <div class="picture">
                            @php
                                $path = url('user/images/' . auth()->user()->picture);
                            @endphp
                            <img src="{{ $path }}" class="picture-src" id="wizardPicturePreview" title="">
                            <input type="file" id="wizard_picture" name="wizard_picture" class="">
                        </div>
                    </div>
                </div>
                <!-- profile picture -->


                <br>
                <!-- sidebar button -->
                <div class="row">
                    <a class="nav-link" href="/dashboard/profile">
                        <img class="icon" src="/image/user.png" width="10%" alt=""> &nbsp; My Profile
                    </a>
                    <a class="nav-link" href="/dashboard/profile/change-password">
                        <img class="icon" src="/image/lock.png" width="10%" alt=""> &nbsp; Change Password
                    </a>
                    <a class="nav-link" href="/dashboard/profile/notification">
                        <img class="icon" src="/image/notif.png" width="10%" alt=""> &nbsp; Notification
                    </a>
                    <a class="nav-link" href="/dashboard/profile/kpi">
                        <img class="icon" src="/image/kpi.png" width="10%" alt=""> &nbsp; Key Performance Indicator
                    </a>
                    <a class="nav-link active" href="/dashboard/profile/jobdesk">
                        <img class="icon" src="/image/jobdesc.png" width="10%" alt=""> &nbsp; Job Description
                    </a>
                    <a class="nav-link" href="/dashboard/profile/training">
                        <img class="icon" src="/image/training.png" width="10%" alt=""> &nbsp; Training Plan
                    </a>
                </div>
                <div class="row" style="margin-left: 5%; margin-top: 100%;">
                    <form action="/logout" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-sm profile-logout">LOG OUT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- KPI  -->
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:100%; height:100%; left:-35%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <h4>Job Description</h4>
                &nbsp;
                @if($JobDesc->isEmpty() != true)
                    @foreach($JobDesc as $value)
                        <div class="row">
                            <div class="col-sm-3">
                                <p style="color: #A8A8A8; font-weight: 400;">Department</p>
                            </div>
                            <div class="col">
                                <p style="font-weight: 400;">{{ $value->department }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <p style="color: #A8A8A8; font-weight: 400;">Location</p>
                            </div>
                            <div class="col">
                                <p style="font-weight: 400;">{{ $value->location }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <p style="color: #A8A8A8; font-weight: 400;">Report to</p>
                            </div>
                            <div class="col">
                                <p style="font-weight: 400;">{{ $value->report }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <p style="color: #A8A8A8; font-weight: 400;">Working condition</p>
                            </div>
                            <div class="col">
                                <p style="font-weight: 400;">{{ $value->condition }}</p>
                            </div>
                        </div>

                        {{-- Text Area --}}
                        <div class="row pb-1">
                            <div class="form-group">
                                <p class="textarea-title"><strong>SCOPE OF THE ROLE</strong></p>
                                <textarea name="scope" id="editorScope" class="text-area" placeholder="Scope of the role">{{ $value->scope }}</textarea>
                            </div>
                        </div>

                        <div class="row pb-1">
                            <div class="form-group">
                                <p class="textarea-title"><strong>SUPERVISORY & LINE MANAGEMENT RESPONSIBILITIES</strong></p>
                                <textarea name="supervisory" id="editorSupervisory" class="text-area" placeholder="Supervisory & line management responsibilities">{{ $value->supervisory }}</textarea>
                            </div>
                        </div>

                        <div class="row pb-1">
                            <div class="form-group">
                                <p class="textarea-title"><strong>MAIN RESPONSIBILITIES & ACTIVITIES</strong></p>
                                <textarea name="responsibilities" id="editorResponsibilities" class="text-area" placeholder="Main responsibilities & activities">{{ $value->responsibilities }}</textarea>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="row">
                        <p style="color: #A8A8A8; font-weight: 400; text-align: center;">No Data</p>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

<script>
    ClassicEditor
        .create( document.querySelector( '#editorScope' ) )
        .then( editor => {
            editor.isReadOnly = true;
            editor.ui.view.toolbar.element.style.display = 'none';
            console.log( editor );
        })
        .catch( error => {
            console.error( error );
        });

    ClassicEditor
        .create( document.querySelector( '#editorSupervisory' ) )
        .then( editor2 => {
            editor2.isReadOnly = true;
            editor2.ui.view.toolbar.element.style.display = 'none';
            console.log( editor );
        })
        .catch( error => {
            console.error( error );
        });

    ClassicEditor
        .create( document.querySelector( '#editorResponsibilities' ) )
        .then( editor3 => {
            editor3.isReadOnly = true;
            editor3.ui.view.toolbar.element.style.display = 'none';
            console.log( editor );
        })
        .catch( error => {
            console.error( error );
        });
</script>

@endsection