@extends('layouts-w-sidebar.main')

@section('content')

<style>
  .form-control{
    border: none!important;
    background-color: #F7F7F7;
    border-radius: 8px;
  }
  .select2-container--default .select2-selection--single{
    border: none;
    background-color: #F7F7F7;
    border-radius: 8px;
  }
  .alert.alert-danger{
    color: white;
    margin-left: 35%;
    margin-bottom: -3px;
    width: 26%;
    border-radius: 8px;
    background-color: #FF6060;
    border: none;
  }

  .required:after {
    content:" *";
    color: red;
  }
</style>

<!-- WORKING Selectpicker JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>

<!-- Main content -->
<div class="content" style="padding-top: 3%; padding-bottom: 3%;">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <div class="container p-5">
    <div class="card card-default" style="margin-left: 160px; width: 70%; border-radius: 16px; border: none; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1);">
      <div class="card-body">
        <h1 class="ml-5" style="color: #4107BB; margin-top: 20px;">New Task</h1>
        <div class="container">

          <div class="row mt-4" style="margin-left: 3%; margin-bottom: 2%;">
            <div class="col-3" style="margin-right:-7%;">
              <form method="post" action="/dashboard/task">
                @csrf
                <input type="hidden" name="date" value="{{request()->get('date')}}">
              <div class="md-form">
                <label for="start_time" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Start time</label>
                <input type="time" id="start_time" name="start_time" class="form-control @error('start_time') is-invalid @enderror" style="width: 60%;" required>
                @error('start_time')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                @enderror
              </div>
            </div>
            <div class="col-3" style="margin-right:-7%;">
              <div class="md-form">
                  <label for="finish_time" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Finish time</label>
                  <input type="time" id="finish_time" name="finish_time" class="form-control @error('finish_time') is-invalid @enderror" style="width: 60%;" required>
                  @error('finish_time')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                @enderror
                </div>
              </div>
            </div>
          </div>

          <div class="row" style="margin-left: 4%;">
            <div class="col">
              <label for="project" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Project name</label>
              <div class="form-group" style="width: 72%;">
                <select name="project_id" id="project_id" class="selectpicker form-control select2 @error('project_id') is-invalid @enderror" style="width: 100%;" title="-- Select Project Name --" required>
                  @foreach ($projects as $project)
                    <option value="{{ $project->id }}">{{ $project->name }}</option>   
                  @endforeach
                    <option value="0">Other, please mention in Project Remark</option>
                </select>
                @error('project_id')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>

            <div class="col" style="margin-left: -10%;">
              <label for="project_remark" id="project_remark_lable" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Project remarks</label>
              <div class="form-group" style="width: 71%;">
                <input class="form-control @error('project_remark') is-invalid @enderror" type="text" id="project_remark" name="project_remark" placeholder="Remarks..."> 
                @error('project_remark')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row" style="margin-left: 4%;">
            {{-- Dependent Dropdown --}}
            <div class="col">
              <label for="daily_task" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Task name</label>
              <div class="form-group" style="width: 72%;">
                <fieldset>
                  <div style="display:inline-block; max-width: 300px;  width: 300px;">
                    <select name="daily_task_id" style="width: 300px;" id="daily_task_id" data-live-search="true" style="width: 100%;" class="selectpicker form-control select2 @error('daily_task_id') is-invalid @enderror" title="-- Select Task Name --" required> 
                      {{-- <option hidden value="">-- Select task name --</option> --}}
                    </select>
                    @error('start_time')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                  </div>
                </fieldset>
              </div>
            </div>
            {{-- Dependent Dropdown --}}

            {{-- <div class="col">
              <label for="daily_task" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Task name</label>
              <div class="form-group" style="width: 72%;">
                <select name="daily_task_id" class="form-control select2 @error('daily_task_id') is-invalid @enderror" required>  
                <option hidden value="">-- Select task name --</option>
                @foreach ($daily_tasks as $daily_task)
                    <option value="{{ $daily_task->id }}">{{ $daily_task->name }}</option>
                @endforeach
                </select>
                @error('start_time')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                @enderror
              </div>
            </div> --}}

            <div class="col" style="margin-left: -10%;">
              <label for="daily_task_remark" id="daily_task_remark_lable" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Task remarks</label>
              <div class="form-group" style="width: 71%;">
                <input class="form-control @error('daily_task_remark') is-invalid @enderror" type="text" id="daily_task_remark" name="daily_task_remark" placeholder="Remarks...">
                @error('site_remark')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row mt-1" style="margin-left: 4%;">
            <label for="note" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Additional note</label>
            {{-- <div class="form-group" style="width: 40.3%;"> --}}
            <div class="form-group" style="width: 85%;">
              <input class="form-control" type="text" id="note" name="note" placeholder="Note...">
            </div>
          </div>

          <div class="row" style="margin-left: 4%;">
            <div class="col-sm-4">
              <label for="type_of_work" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Type of Work</label>
              <div class="form-group" style="width: 122%;">
                <select name="type_of_work_id" class="selectpicker form-control select2 @error('type_of_work_id') is-invalid @enderror" title="-- Select Type Of Work --" required>  
                {{-- <option hidden value="">-- Select Type Of Work --</option> --}}
                @foreach ($type_of_works as $type_of_work)  
                  <option value="{{ $type_of_work->id }}">{{ $type_of_work->name }}</option>
                @endforeach
                </select>
                @error('type_of_work_id')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                @enderror
              </div>
            </div>

            <div class="col-sm-4" style="margin-left: 93px;">
              <label for="transportation" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Kind of Transportation</label>
              <div class="form-group" style="width: 122%;">
                <select name="transportation_id" class="selectpicker form-control select2 @error('transportation_id') is-invalid @enderror" title="-- Select Transportation --" required>  
                {{-- <option hidden value="">-- Select Transportation --</option> --}}
                @foreach ($transportations as $transportation)    
                  <option value="{{ $transportation->id }}" >{{ $transportation->name }}</option>
                @endforeach
                </select>
                @error('transportation_id')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row" style="margin-left: 4%;">
            <div class="col">
              <label for="site" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Site name</label>
              <div class="form-group" style="width: 72%;">
                <fieldset>
                  <div style="display:inline-block; max-width: 300px; width: 300px;">
                    <select name="site_id" id="site_id" style="width: 300px;" class="selectpicker form-control select2 @error('site_id') is-invalid @enderror" data-live-search="true" title="-- Select Site Name --">  
                      {{-- <option hidden value="">-- Select site name --</option> --}}
                      @if (!$sites->isEmpty())
                        @foreach ($sites as $site)
                          <option value="{{ $site->id }}">{{ $site->name }}</option>
                        @endforeach
                        <option value="0">Other, please mention in Site Remark</option>
                      @else
                        <option value="0">Other, please mention in Site Remark</option>
                      @endif
                    </select>
                    @error('site_id')
                      <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                  </div>
                </fieldset>
              </div>
            </div>

            <div class="col" style="margin-left: -10%;">
              <label for="site_remark" id="site_remark_link" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Other site name</label>
              <div class="form-group" style="width: 71%;">
                <input class="form-control @error('site_remark') is-invalid @enderror" type="text" id="site_remark" name="site_remark" placeholder="Type site name...">
                @error('site_remark')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row mt-2 mb-5" style="margin-left: 4%;">
            <div class="col-sm-3">
              <label for="task_status" style="font-weight:200; font-size: 12px; color: #A8A8A8;" class="required">Task completion status</label>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="task_status" id="task_status" value="In Progress" checked>
                <label class="form-check-label" for="task_status">
                  Still in Progress
                </label>
              </div>
            </div>

            <div class="col">
              <label for="task_status" style="font-weight:200; font-size: 12px; color: #FFFFFF;">Task completion status</label>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="task_status" id="task_status" value="Completed" checked>
                <label class="form-check-label" for="task_status">
                  Completed
                </label>
              </div>
            </div>
          </div>

          <br><br><br>

          <div class="row mb-3 justify-content-end" style="margin-left: -15px; margin-top: -50px;">
            {{-- <div class="col-sm-3" style="margin-right: -80px;"> 
              <button type="submit" class="btn pl-4 pr-4 pt-2 pb-2" style="background-color: #ffffff; color: #A8A8A8; border-color: #A8A8A8;">CANCEL</button>
            </div> --}}
            <div class="col-sm-3"> 
              <button type="submit" class="btn pl-4 pr-4 pt-2 pb-2" style="background-color: #ffffff; color: #00DFC7; border-color: #00DFC7;">SAVE THIS TASK</button>
            </div>
          </div>
        
        </div>
      </div>
    </div>
  </div>
</form>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

  <script>
    $( document ).ready(function() {
      console.log("ready!");
      $('.selectpicker').selectpicker();
      
      $('#project_id').on('change',function(e) {
        var id = $(this).val();

        $.ajax({
          url:"{{ route('dependentDropdown.store') }}",
          type:"POST",
          data:{
            _token:$("input[name=_token]").val(),
            id: id
          },
          success:function(response) {
            // console.log(response)
            $('#daily_task_id').empty();

            if($.trim(response)) {
              $.each(response, function(id, name) {
                $('#daily_task_id').append(new Option(name, id));
              });
              $('#daily_task_id').append(new Option('Other, Please mention in Task Remark', 0));
              $('#daily_task_id').selectpicker('refresh');
            } else {
              $('#daily_task_id').append(new Option('Other, Please mention in Task Remark', 0));
              $('#daily_task_id').selectpicker('refresh');
            }
            
          }
        });
      });

      $('#site_id').on('change', function(e) {
        var val = $(this).val();

        if(val == 0){
          var element = document.getElementById("site_remark_link");
          element.setAttribute( 'class', 'required' );
        }
      });

      $('#project_id').on('change', function(e) {
        var val = $(this).val();

        if(val == 0){
          var element = document.getElementById("project_remark_lable");
          element.setAttribute( 'class', 'required' );
        }
      });

      $('#daily_task_id').on('change', function(e) {
        var val = $(this).val();

        if(val == 0){
          var element = document.getElementById("daily_task_remark_lable");
          element.setAttribute( 'class', 'required' );
        }
      });

    });
  </script>

@endsection