@extends('layouts.main')

@section('content')

{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

<style>
    .form-control{
        border: none!important;
        background-color: #F7F7F7;
        border-radius: 8px;
    }
    .select2-container--default .select2-selection--single{
        border: none;
        background-color: #F7F7F7;
        border-radius: 8px;
    }
</style>

<!-- Main content -->
<div class="content">
    <div class="container p-5" style="margin-bottom: 150px;">
        <div class="card card-default" style="margin-left: 160px; width: 70%; border-radius: 16px; border: none; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1);">
            <div class="card-body">

                @foreach ($Task as $task)
                    <h1 class="ml-5" style="color: #FF6060; margin-top: 20px;">Day Off</h1>
                    <h5 class="ml-5" style="color: #A8A8A8; margin-top: 10px; font-weight: 200;"><em>{{ $task->created_at->format('d F Y') }}</em></h5>
                    <div class="container">
                    <div class="row mt-4" style="margin-left: 3%;">
                        <div class="col" style="margin-right:-7%;">
                        <form>
                            <fieldset disabled>
                                <div class="form-group" style="width: 90%;">
                                    <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Reason of Absence</label>
                                    <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $task->name }}">
                                </div>
                                <div class="form-group" style="width: 90%;">
                                    <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Absence Remarks</label>
                                    <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->absence_remark }}</textarea>
                                </div>
                                <div class="form-group" style="width: 90%;">
                                    <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Description</label>
                                    <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->absence_description }}</textarea>
                                </div>
                            </fieldset>
                        </form> <br><br>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

@endsection
