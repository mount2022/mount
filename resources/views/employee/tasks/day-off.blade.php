@extends('layouts-w-sidebar.main')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<style>
  .form-control{
    border: none!important;
    background-color: #F7F7F7;
    border-radius: 8px;
  }
  .select2-container--default .select2-selection--single{
    border: none;
    background-color: #F7F7F7;
    border-radius: 8px;
  }
</style>

<!-- WORKING Selectpicker JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>

<!-- Main content -->
<div class="content">
  <div class="container p-5">
  	<div class="card card-default" style="margin-left: 160px; width: 70%; border-radius: 16px; border: none; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1);">
      <div class="card-body">
        <h1 class="ml-5" style="color: #FF6060; margin-top: 20px;">Day Off</h1>
        <div class="container">
          <div class="row" style="margin-left: 4%;">
            <div class="col">
              <form method="post" action="/dashboard/task?date={{ request()->get('date') }}">
                @csrf
                <input type="hidden" name="date" value="{{request()->get('date')}}">
              <label for="absence" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Reason of Absence</label>
              <div class="form-group" style="width: 72%;">
                <select name="absence_id" class="selectpicker form-control select2 @error('absence_id') is-invalid @enderror" title="-- Select Reason --" required>  
                {{-- <option hidden value="">-- Select Reason --</option> --}}
                @foreach ($absences as $absence)
                  @if ($absence->id != 4)
                    <option value="{{ $absence->id }}">{{ $absence->name }}</option>
                  @endif
                @endforeach
                <option value="4">Other, Please mention in Absence Remark</option>
                </select>
                @error('absence_id')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>
                @enderror
              </div>
            </div>

            <div class="col" style="margin-left: -10%;">
              <label for="absence_remark" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Others</label>
              <div class="form-group" style="width: 71%;">
                <input class="form-control @error('absence_remark') is-invalid @enderror" type="text" id="absence_remark" name="absence_remark" placeholder="Remarks...">
                @error('absence_remark')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row mt-1" style="margin-left: 4%;">
            <label for="absence_description" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Description</label>
            <div class="form-group" style="width: 85%;">
              <input class="form-control" type="text" id="absence_description" name="absence_description" placeholder="Note...">
            </div>
          </div>

          <br><br><br>

          <div class="row mb-3 justify-content-end" style="margin-left: -15px; margin-top: -50px;">
            {{-- <div class="col-sm-3" style="margin-right: -80px;"> 
              <button type="submit" class="btn pl-4 pr-4 pt-2 pb-2" style="background-color: #ffffff; color: #A8A8A8; border-color: #A8A8A8;">CANCEL</button>
            </div> --}}
            <div class="col-sm-3"> 
              <button type="submit" class="btn pl-4 pr-4 pt-2 pb-2" style="background-color: #ffffff; color: #00DFC7; border-color: #00DFC7;">SAVE THIS TASK</button>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</form>
</div>

<script>
  $( document ).ready(function() {
    console.log("ready!");
    $('.selectpicker').selectpicker();
  }
</script>
@endsection