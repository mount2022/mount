@extends('layouts-w-sidebar.main')

@section('content')

<style>
  .dot {
    height: 10px;
    width: 10px;
    background-color: #42A2D0;
    border-radius: 100%;
    display: inline-block;
  }
  .newtask{
    background-color: #4107BB;
    border: none;
    border-radius: 8px;
  }
  .newtask:hover{
    opacity: 80%;
    background-color: #4107BB;
  }
  .dayoff{
    background-color: #FF6060;
    border: none;
    border-radius: 8px;
  }
  .dayoff:hover{
    opacity: 80%;
    background-color: #FF6060;
  }
  .submit{
    background-color: #00DFC7;
    border: none;
    border-radius: 8px;
  }
  .submit:hover{
    opacity: 80%;
    background-color: #00DFC7;
  }
  .task:hover{
    opacity: 80%;
    background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%);
  }
  .dismiss{
    background-color: #FFFFFF;
    border-color: #A8A8A8;
    border-radius: 8px;
    color: #A8A8A8;
  }
  .dismiss:hover{
    opacity: 80%;
    background-color: #A8A8A8;
  }
  .viewtask{
    background-color: #FCA010;
    border: none;
    border-radius: 8px;
    color: #FFFFFF;
  }
  .viewtask:hover{
    opacity: 80%;
    background-color: #FCA010;
  }
  
  .radiogroupAboveLabel label{
    display:inline-block;
	text-align:center;
	vertical-align: text-top;
	margin:0 1em;
	color:#A8A8A8;
	font-size: 16px;
	font-weight: 400
  }

  .radiogroupAboveLabel label input[type="radio"] {
      display:block;
    margin:0.75em auto;
  }
  .alert.alert-success{
    color: white;
    margin-left: 40%;
    margin-top: 1%;
    margin-bottom: -1%;
    width: 20%;
    border-radius: 8px;
    text-align: center;
    background-color: #00DFC7;
    border: none;
  }

  .maxChar {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 500px;
  }
</style>

<nav class="navbar" style="background-color: #F5F4FF; box-shadow:unset;">
    <div class="container" style="height: 40px; margin-left: 55px;">
        <div class="col-sm-1" style="margin-top: -10px; margin-right: -30px;">
          <h4 style="color: #4107BB; font-weight: 300; margin-left:-10%; font-size: 20px;">Select</h4>
        </div>

        
          <div class="col-sm-2" style="width: 12%; margin-top: 1px;">
              <form action="#" method="get">
              @csrf
              <div class="form-group">
                <select id="month" name="month" class="form-control select2" style="border: none; border-radius: 8px;">
                  <option selected disabled>Month</option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03">March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">Sertember</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
          </div>
          <div class="col-sm-2" style="width: 12%; margin-top: 1px;">
            <div class="form-group">
              <select id="year" name="year" class="form-control select2" style="border: none; border-radius: 8px;">
                <option selected disabled>Year</option>
                <?php 
                $year = date('Y');
                $min = $year - 10;
                $max = $year;
                for( $i=$max; $i>=$min; $i-- ) {
                echo '<option value='.$i.'>'.$i.'</option>';
                }?>
              </select>
            </div>
          </div>
          <div class="col" style="margin-top: -1%;">
            <button class="btn" style="background-color: #4107BB; color: #ffffff; border-radius: 8px;">
              <img src="/image/search.png" alt="search">
            </button>
          </div>
        </form>

        @php
          $filled = 0;
          $days = 0;
          // dd(auth()->user()->id);
          foreach ($dates as $date){
                  $days = $days + 1;  
              // if (\App\Models\Task::whereDate('created_at', $date)->count() > 0 ){
              if (\App\Models\Task::whereDate('created_at', $date)->where('user_id', auth()->user()->id)->whereNull('absence_id')->count() > 0 ){
                  $filled = $filled + 1;
              }
          }
        @endphp

        @php
        function formatMinutes($minutes) {
            $now = \Carbon\Carbon::now();
            $then = $now->copy()->addMinutes($minutes);
            $hours = $now->diffInHours($then);
            $minutes = $now->diffInMinutes($then) - ($hours * 60);

            return \sprintf('%dh %02dmin', $hours, $minutes);
        }

        $totalMinutes = 0;
        @endphp

        @php
            foreach ($tasks as $task) {
              if ($task->absence_id == null) {
                $start = new \Carbon\Carbon($task->start_time);
                $end   = new \Carbon\Carbon($task->finish_time);
                $minutesToday  = $start->diffInMinutes($end);
                $totalMinutes += $minutesToday;
                
              }
            }
        @endphp

        <ul class="nav justify-content-end" style="margin-right: -17px;">
          <li style="margin-right: 60px; margin-top:-12px">
            <div class="row" style="font-size: 10px; color: #4107BB;">
              Filled working days
            </div>
            <div class="row" style="font-size: 20px; margin-top: -2%; color: #4107BB;">
              <!-- <span style="color: #4107BB; margin-left: -9%;">3</span> <span style="color: #A8A8A8; margin-left: -10%;">/ 21 days</span> -->
                {{ $filled }} / {{ $days }} days
            </div>
          </li>
          <li style="margin-right: 50px; margin-top:-12px; color: #4107BB;">
            <div class="row" style="font-size: 10px;">
              @if ($isMonth == true)
                Total working time this month
              @else
                Total working time this day
              @endif
            </div>
            <div class="row" style="font-size: 20px; margin-top: -2%">
              {{ formatMinutes($totalMinutes) }}
            </div>
          </li>
          <li style="margin-right: 20px; margin-top:-2%; font-weight: 500; font-size: 20px; color: #4107BB;">
            {{ Carbon\Carbon::parse($dates[0])->format('F Y') }}
          </li>
        </ul>

    </div>
</nav>

  
@if (session()->has('added'))
  <div class="alert alert-success" role="alert">
    {{ session('added') }}
  </div>
@elseif (session()->has('deleted'))
  <div class="alert alert-success" style="background-color: #FF6060;" role="alert">
    {{ session('deleted') }}
  </div>
@endif
  
<div class="container" style="padding-top: 2%;">
  <div class="row">
        @if (request()->get('date') == null)
          <h1>All Task(s) in {{ Carbon\Carbon::parse($dates[0])->format('F Y') }}</h1>
        @else
          <h1>All Task(s) in {{ Carbon\Carbon::parse(request()->get('date'))->format('l, d F Y') }}</h1>
        @endif
        
    <div class="col-sm-2">
      @if ($isMonth == true)
        <p style="color: #A8A8A8;">This month's record:</p>
      @else
        <p style="color: #A8A8A8;">This day's record:</p>
      @endif
    </div>

    <div class="col-sm-3" style="margin-left: -4%;">
      <span class="dot"></span> <p style="color: #1F1F1F; margin-left: 7%; margin-top: -8%;">Task Completed : {{ $isCompleted }}</p> 
    </div>
    <div class="col-sm-3" style="margin-left: -8%;">
      <span class="dot" style="background-color: #FCA010;"></span> <p style="color: #1F1F1F; margin-left: 7%; margin-top: -8%;">Task in progress : {{ $isInProgress }}</p> 
    </div>

    @if ($isMoodEmpty != true)
      <div class="col-sm-3 " style="align: right; margin-left: 20%;">
        <span class="dot" style="background-color: #FCA010; visibility: hidden;"></span> <p style="color: #1F1F1F; margin-left: 7%; margin-top: -8%;">Mood Rate : {{ $mood }}</p> 
      </div>
    @endif
  </div>

  @php
    date_default_timezone_set('Asia/Jakarta');
  @endphp

  {{-- Unlock Status --}}
  <ul class="nav justify-content">    
    @if (\App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', auth()->user()->id)->count() > 0)
      @if (date('Y-m-d H:i:s') <= \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', auth()->user()->id)->latest()->first()->end)
        @php
          $until = \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', auth()->user()->id)->latest()->first()->end;
          $until = \Carbon\Carbon::parse($until);
        @endphp 
        <div class="col" style="width: 100%; margin-bottom: 2%;">
          <h5>Unlock until : {{ $until->format('d F Y - H:i:s') }}</h5>
        </div>
      @endif
    @endif
  </ul>
  {{-- Unlock Status --}}

  @if (request()->get('date') == date('Y-m-d') || request()->get('date') == now()->subDay(1)->format('Y-m-d') 
  || \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', auth()->user()->id)->count() > 0)
    @php
      $isWorkDay = false;
      $isDayOff = false;

      foreach($tasks as $task) {
        if ($task->absence_id == null) {
          $isWorkDay = true;
          $isDayOff = false;
        } elseif ($task->absence_id != null) {
          $isDayOff = true;
          $isWorkDay = false;
        }
      }

      date_default_timezone_set('Asia/Jakarta');
    @endphp
    
    <div class="row">
      @php
        $unlock = \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', auth()->user()->id)->count();
        if($unlock != 0) {
          $end = \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', auth()->user()->id)->latest()->first()->end;
        } else {
          $end = null;
        }
      @endphp

      @if ($end != null && date('Y-m-d H:i:s') <= $end)

        @if ($isWorkDay == true && $isDayOff == false)
          <div class="col-sm-2">
            <a href="/dashboard/task/create?date={{ request()->get('date') }}" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">+ ADD NEW TASK</a>
          </div>
          @if ($isMoodEmpty == true)
            <div class="col justify-content-end">
              <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="float: right;" 
              data-bs-toggle="modal" data-bs-target="#moodModal">SUBMIT ALL</button>
            </div>
          @endif
        @elseif ($isDayOff == true && $isWorkDay == false)
          @if ($isMoodEmpty == true)
            <div class="col justify-content-end">
              <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="float: right;" 
              data-bs-toggle="modal" data-bs-target="#moodModal">SUBMIT ALL</button>
            </div>
          @endif
        @elseif ($isWorkDay == false && $isDayOff == false)
          <div class="col-sm-2">
            <a href="/dashboard/task/create?date={{ request()->get('date') }}" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">+ ADD NEW TASK</a>
          </div>
          <div class="col-sm-2" style="margin-left:-1%;">
            <a href="/dashboard/task/create?date={{ request()->get('date') }}&type=day-off" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 dayoff">DAY OFF</a>
          </div>
          @if ($isMoodEmpty == true)
            <div class="col justify-content-end">
              <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="float: right;" 
              data-bs-toggle="modal" data-bs-target="#moodModal">SUBMIT ALL</button>
            </div>
          @endif
        @endif

      @elseif (request()->get('date') == date('Y-m-d') || request()->get('date') == now()->subDay(1)->format('Y-m-d') )

        @if ($isWorkDay == true && $isDayOff == false)
          <div class="col-sm-2">
            <a href="/dashboard/task/create?date={{ request()->get('date') }}" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">+ ADD NEW TASK</a>
          </div>
          @if ($isMoodEmpty == true)
            <div class="col justify-content-end">
              <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="float: right;" 
              data-bs-toggle="modal" data-bs-target="#moodModal">SUBMIT ALL</button>
            </div>
          @endif
        @elseif ($isDayOff == true && $isWorkDay == false)
          @if ($isMoodEmpty == true)
            <div class="col justify-content-end">
              <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="float: right;" 
              data-bs-toggle="modal" data-bs-target="#moodModal">SUBMIT ALL</button>
            </div>
          @endif
        @elseif ($isWorkDay == false && $isDayOff == false)
          <div class="col-sm-2">
            <a href="/dashboard/task/create?date={{ request()->get('date') }}" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 newtask">+ ADD NEW TASK</a>
          </div>
          <div class="col-sm-2" style="margin-left:-1%;">
            <a href="/dashboard/task/create?date={{ request()->get('date') }}&type=day-off" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 dayoff">DAY OFF</a>
          </div>
          @if ($isMoodEmpty == true)
            <div class="col justify-content-end">
              <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="float: right;" 
              data-bs-toggle="modal" data-bs-target="#moodModal">SUBMIT ALL</button>
            </div>
          @endif
        @endif
      @endif

      <!-- Mood Modal -->
      <div class="modal fade" id="moodModal" tabindex="-1" aria-labelledby="moodModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
          <div class="modal-content" style="border-radius: 10px;">
            <form method="POST" action="{{ route('mood.create') }}"> @csrf
              <input type="hidden" name="date" id="date" value="{{ request()->get('date') }}">

              <div class="modal-header border-0">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>

              <div class="modal-body">
                <img class="mb-3" src="/image/mood-check.png" alt="mood-check" style="width:15%; display: block; margin-left: auto; margin-right: auto;">
                <p style="font-size: 20px; text-align: center;">How do you feel today?</p>
                <div class="radiogroupAboveLabel" style="text-align: center;">
                  <label for="rate1"> 
                    <input type="radio" name="rating" id="rate1" value="Couldn't be worse">
                    <span style="font-weight: 400"> Couldn't be <br> worse </span>
                  </label>
                  
                  <label for="rate2">
                    <input type="radio" name="rating" id="rate2" value="Sick">
                    <span style="font-weight: 400"> Sick </span>
                  </label>
                  
                  <label for="rate3">
                    <input type="radio" name="rating" id="rate3" value="Unwell">
                    <span style="font-weight: 400"> Unwell </span>
                  </label>
                  
                  <label for="rate4">
                    <input type="radio" name="rating" id="rate4" value="Meh">
                    <span style="font-weight: 400"> Meh </span>
                  </label>

                  <label for="rate5"> 
                    <input type="radio" name="rating" id="rate5" value="So-so">
                    <span style="font-weight: 400"> So-so </span>
                  </label>
                  
                  <label for="rate6">
                    <input type="radio" name="rating" id="rate6" value="Okay">
                    <span style="font-weight: 400"> Okay </span>
                  </label>
                  
                  <label for="rate7">
                    <input type="radio" name="rating" id="rate7" value="Good">
                    <span style="font-weight: 400"> Good </span>
                  </label>
                  
                  <label for="rate8">
                    <input type="radio" name="rating" id="rate8" value="Very good">
                    <span style="font-weight: 400"> Very good </span>
                  </label>

                  <label for="rate9">
                    <input type="radio" name="rating" id="rate9" value="Great">
                    <span style="font-weight: 400"> Great </span>
                  </label>
                  
                  <label for="rate10">
                    <input type="radio" name="rating" id="rate10" value="Really great">
                    <span style="font-weight: 400"> Really <br> great </span>
                  </label>
                </div>
              </div>

              <div class="modal-footer border-0">
                <div class="container">
                  <div class="col text-center">
                    <button type="submit" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submits" data-bs-dismiss="modal"
                    style="background-color: #00DFC7; color: white; border: 0px;border-radius: 8px;">CONTINUE</button>
                  </div>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>

      @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
          <script>
          $(function() {
            $('#successModal').modal('show');
          });
        </script>
      @endif

      <!-- Modal Success -->
      <div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered">
          <div class="modal-content" style="border-radius: 10px;">

            <div class="modal-header border-0">
              {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
            </div>

            <div class="modal-body">
              <img class="mb-3" src="/image/success.png" alt="mood-check" style="width:25%; display: block; margin-left: auto; margin-right: auto;">
              <p style="font-size: 20px; text-align: center;">Your today's task is successfully submitted!</p>
            </div>

            <div class="modal-footer border-0">
              <div class="container">
                <div class="col text-center">
                  <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submits" data-bs-dismiss="modal"
                  style="background-color: #00DFC7; color: white; border: 0px;border-radius: 8px;">CLOSE</button>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      <!-- The Modal -->
      {{-- <div id="myModal" class="modal">
        <div class="modal-content">
          <span class="close">&times;</span>
          <img class="mb-3" src="/image/mood-check.png" alt="mood-check" style="align-self: center; width:15%;">
          <p style="font-size: 20px; align-self: center!important;">How do you feel today?</p>
          
          <form method="POST" id="moodForm">
            @csrf
            <input type="hidden" name="date" id="date" value="{{ request()->get('date') }}">
            <div class="radiogroupAboveLabel">
              <label for="rate1"> 
                <input type="radio" name="rating" id="rate1" value="Couldn't be worse">
                <span style="font-weight: 400"> Couldn't be <br> worse </span>
              </label>
              
              <label for="rate2">
                <input type="radio" name="rating" id="rate2" value="Sick">
                <span style="font-weight: 400"> Sick </span>
              </label>
              
              <label for="rate3">
                <input type="radio" name="rating" id="rate3" value="Unwell">
                <span style="font-weight: 400"> Unwell </span>
              </label>
              
              <label for="rate4">
                <input type="radio" name="rating" id="rate4" value="Meh">
                <span style="font-weight: 400"> Meh </span>
              </label>

              <label for="rate5"> 
                <input type="radio" name="rating" id="rate5" value="So-so">
                <span style="font-weight: 400"> So-so </span>
              </label>
              
              <label for="rate6">
                <input type="radio" name="rating" id="rate6" value="Okay">
                <span style="font-weight: 400"> Okay </span>
              </label>
              
              <label for="rate7">
                <input type="radio" name="rating" id="rate7" value="Good">
                <span style="font-weight: 400"> Good </span>
              </label>
              
              <label for="rate8">
                <input type="radio" name="rating" id="rate8" value="Very good">
                <span style="font-weight: 400"> Very good </span>
              </label>

              <label for="rate9">
                <input type="radio" name="rating" id="rate9" value="Great">
                <span style="font-weight: 400"> Great </span>
              </label>
              
              <label for="rate10">
                <input type="radio" name="rating" id="rate10" value="Really great">
                <span style="font-weight: 400"> Really <br> great </span>
              </label>
            </div>

            <div class="col justify-content-end">
              <button type="submit" id="myBtn2" class="btn mb-3 pl-4 pr-4 pt-3 pb-3 submit" style="margin-top: 20px; margin-left: 385px;" 
                data-toggle="modal" data-target="#myModal2" data-dismiss="myModal" class="close">CONTINUE</button>
            </div>
          </form>
        </div>
      </div> --}}

      {{-- <div id="myModal2" class="modal">
        <div class="modal-content" id="successModal">
          <span class="close">&times;</span>
          <img class="mb-3" src="/image/success.png" alt="mood-check" style="align-self: center; width:15%;">
          <p style="font-size: 20px; align-self: center!important;">Your today's task is successfully submitted!</p>
          
          <div class="col justify-content-end">
            <button onClick="window.location.reload();" id="myBtn3" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submit" 
            style="margin-top: 20px; margin-left: 385px;">CLOSE</button>
          </div>
        </div>
      </div> --}}

    </div>
    @endif

    @if($countReminders > 0 && request()->get('date') != null)
      <div class="container" >
        <div class="row" style="">
          <div class="card task" style="background: #FCA0101A; box-shadow:none; color:white; border-radius: 16px;">
              <div class="card-body" style="display: flex; border-color: #FCA010;">
                <div class="col-sm-5">
                  <p style="font-weight: 400; color: #FCA010; margin-top: 10px;"><span><img src="/image/reminder.png" alt="">&nbsp;</span>REMINDER</p>
                  <p style="color: black;">You still have <strong> {{ $countReminders }} task(s) In Progress</strong> yesterday</p>
                </div>
                <div class="col" style="top: 25px;">
                  {{-- <a href="#ReminderModal" id="ReminderButton" class="btn mb-5 pl-4 pr-4 pt-3 pb-3 mr-3 viewtask" style="float: right;margin-left: 25px;">VIEW TASK</a> --}}
                  <button type="button" class="btn mb-5 pl-4 pr-4 pt-3 pb-3 mr-3 viewtask" style="float: right;margin-left: 25px;" 
                  data-bs-toggle="modal" data-bs-target="#exampleModal">VIEW TASK</button>
                  @php
                    // dd($Reminders);
                    foreach($Reminders as $key) {
                      $Date = $key->created_at;
                    }
                  @endphp
                  @if($Reminders->isEmpty() != true) 
                    <form action="/dashboard/task/dismiss" method="POST"> @csrf
                      <input type="hidden" name="date" value="{{ $Date }}">
                      <button type="submit" class="btn mb-5 pl-4 pr-4 pt-3 pb-3 dismiss" style="float: right;">DISMISS</button>
                    </form>
                  @endif
                </div>
              </div>
          </div>
        </div>
      </div>
    @endif

    <!-- Modal Reminder -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl modal-dialog-centered">
          <div class="modal-content" style="border-radius: 10px;">
            
            <div class="modal-header border-0">
              <img class="mb-3" src="/image/inprogress.png" alt="" style="width:50px;">
              <h5 style="position: absolute; margin-left: 65px; margin-top: -3%;">Yesterday’s tasks in progress</h5>
              <h6 style="font-weight: 300; margin-left: 18px; margin-top: 3%;">{{ $countReminders }} task(s)</h6>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
              <div class="row scrollDashboard">
                <div class="col-12">
                  <table id="dtOrderExample" class="table table-borderless">
                    <thead>
                      <tr style="color: #A8A8A8;">
                        <th scope="col" style="font-weight: 400;">No</th>
                        <th scope="col" style="font-weight: 400;">Task name</th>
                        <th scope="col" style="font-weight: 400;">Project ID</th>
                        <th scope="col" style="font-weight: 400;">Site name</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 1;
                      @endphp
                      @foreach($Reminders as $reminder)
                        <tr>
                          <th scope="row">{{ $no++ }}</th>
                          <td><strong>{{ $reminder->name }}</strong></td>
                          <td>{{ $reminder->project_id }}</td>
                          <td>{{ $reminder->site_id }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div class="modal-footer border-0">
              <div class="container">
                <div class="col text-center">
                  <button type="button" class="btn btn-primary mb-3 pl-4 pr-4 pt-3 pb-3 submits" data-bs-dismiss="modal" 
                  style="background-color: #00DFC7; color: white; border: 0px;border-radius: 8px;">CLOSE</button>
                </div>
              </div>
            </div>

          </div>
      </div>
    </div>

    <!-- ModalViewTask -->
    <div id="ReminderModal" class="modal" style="background-color: white;">
      <div class="modalBox" style="display: flex; justify-content: center; margin-top: 5%;">
        <div class="modal-content2">
          <span class="close" style="display: flex; justify-content: flex-end;">&times;</span>
          <img class="mb-3" src="/image/inprogress.png" alt="" style="width:50px;">
          <h5 style="position: absolute; left: 100px; top: 60px;">Yesterday’s tasks in progress</h5>
          <h6 style="font-weight: 300;margin-left: 65px;margin-top: -35px;margin-bottom: 25px;">2 task(s)</h6>

          <div class="row scrollDashboard">
            <div class="col-12">
              <table id="dtOrderExample" class="table table-borderless">
                <thead>
                  <tr style="color: #A8A8A8;">
                    <th scope="col" style="font-weight: 400;">No</th>
                    <th scope="col" style="font-weight: 400;">Task name</th>
                    <th scope="col" style="font-weight: 400;">Project ID</th>
                    <th scope="col" style="font-weight: 400;">Site name</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td><strong>Making Presentation</strong></td>
                    <td>Shell Retail</td>
                    <td>Shell NTI xxx</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>

    @foreach ($tasks as $task)
      @if ($task->absence_id == null)
        <a href="/dashboard/task/{{ $task->id }}" style="text-decoration: none;">
          <div class="container" >
            <div class="row mt-1">
        
              <div class="card task" style="background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%); box-shadow:none; color:white; border-radius: 16px; border: none;">
                  <div class="card-body" style="display: flex;">
                    
                    <div class="col-sm-5">
                      @if ($task->daily_task_id == 0)
                        <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->daily_task_remark }}</p>
                      @else
                        <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->name }}</p>
                      @endif
                      @php
                        $start = \Carbon\Carbon::parse($task->start_time)->format('H:i');
                        $finish = \Carbon\Carbon::parse($task->finish_time)->format('H:i');
                      @endphp
                      <p style="margin-top: -15px; font-weight: 300;"><em>{{ $task->created_at->format('d F Y') }}, {{ $start }} - {{ $finish }}</em></p>
                    </div>
        
                    <div class="col" style="text-align: center;">
                      @if ($task->task_status == 'Completed')
                      <div class="card p-2" style="border: none; box-shadow: none; background-color: #FFFFFF; color: #42A2D0; margin-top: 25px; margin-left: 240px; width: 28%;">
                      {{ $task->task_status }}
                      </div>
                      @else
                      <div class="card p-2" style="border: none; box-shadow: none; background-color: #FFFFFF; color: #FCA010; margin-top: 25px; margin-left: 240px; width: 28%;">
                      {{ $task->task_status }}
                      </div>
                      @endif

                      {{-- Approval Status --}}
                      @if ($task->approval_status != null)
                        @if ($task->approval_status == "Not Approved")
                          <div class="card p-2" style="font-size: 13px;border: none; box-shadow: none; background-color: #FFFFFF; 
                          color: #FE6060; margin-top: 25px; margin-left: 240px; width: 28%; ">
                          {{ $task->approval_status }}
                        </div>
                        @else
                          <div class="card p-2" style="font-size: 13px;border: none; box-shadow: none; background-color: #FFFFFF; 
                            color: #42A2D0; margin-top: 25px; margin-left: 240px; width: 28%; ">
                            {{ $task->approval_status }}
                          </div>
                        @endif
                      @endif
                      {{-- Approval Status --}}
                    </div>

                    <div class="col-sm-4" style="margin-right: -205px;">
                      <p style="margin-top: 12px; margin-bottom: -3%; font-weight: 200;">Total time</p>
                      <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ Carbon\Carbon::parse($task->duration)->format('H') }}h {{ Carbon\Carbon::parse($task->duration)->format('i') }}min</p>
                    </div>
                    
                    @if ($task->approval_status == "Not Approved" || $task->approval_status == "Rejected")
                      <div class="col-sm-1" style="margin-top: 20px;">
                        <form action="/dashboard/task/delete" method="post">
                          @method('delete')
                          @csrf
                          <input type="hidden" name="date" value="{{ request()->get('date') }}">
                          <input type="hidden" name="id" value="{{ $task->id }}">
                          <button button type="submit" class="btn" onclick="return confirm('Are you sure to delete?')" style="background-color: none; border: none; color: #ffffff;">
                            <img src="/image/delete.png" alt="search">
                          </button>
                        </form>
                      </div>
                    @endif
                    
                  </div>
              </div>
        
            </div>
          </div>
        </a>
      @else
        <a href="/dashboard/task/off/{{ $task->id }}" style="text-decoration: none;">
          <div class="container" >
            <div class="row" style="margin-top: 2%;">

              <div class="card task" style="background: linear-gradient(270deg, #FE6060 0%, #EF929E 100%); box-shadow:none; color:white; border-radius: 16px; border: none;">
                  <div class="card-body" style="display: flex;">

                    <div class="col-sm-5">
                      @if ($task->absence_id == 4)
                        <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->absence_remark }}</p>
                      @else
                        <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ $task->name }}</p>
                      @endif
                      <p style="margin-top: -15px; font-weight: 200;"><em>{{ $task->created_at->format('d F Y') }}</em></p>
                    </div>

                    {{-- Approval Status --}}
                    <div class="col-sm-3" style="text-align: center;">
                      @if ($task->approval_status != null)
                        @if ($task->approval_status == "Not Approved")
                          <div class="card p-2" style="font-size: 14px;border: none; box-shadow: none; background-color: #FFFFFF; 
                            color: #FE6060; margin-top: 25px; width: 40%; margin-left:240px;">
                            {{ $task->approval_status }}
                          </div>
                        @else
                          <div class="card p-2" style="font-size: 14px;border: none; box-shadow: none; background-color: #FFFFFF; 
                            color: #42A2D0; margin-top: 25px; width: 40%; margin-left:240px;">
                            {{ $task->approval_status }}
                          </div>
                        @endif
                      @endif
                    </div>
                    {{-- Approval Status --}}

                    <div class="col-sm-4" style="margin-right: -210px; margin-left: 150px;">
                      <p style="margin-top: 12px; margin-bottom: -3%; font-weight: 200;">Total time</p>
                      <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">OFF</p>
                    </div>

                    @if ($task->approval_status == "Not Approved" || $task->approval_status == "Rejected")
                      <div class="col-sm-1" style="margin-top: 20px;">
                        <form action="/dashboard/task/delete" method="post">
                          @method('delete')
                          @csrf
                          <input type="hidden" name="date" value="{{ request()->get('date') }}">
                          <input type="hidden" name="id" value="{{ $task->id }}">
                          <button button type="submit" class="btn" style="background-color: none; border: none; color: #ffffff;">
                            <img src="/image/delete.png" alt="search">
                          </button>
                        </form>
                      </div>
                    @endif
                    
                  </div>
              </div>

            </div>
          </div>
        </a>
      @endif
    @endforeach

<meta name="user-id" content="{{ auth()->user()->id }}">

<script>
  var modal = document.getElementById("ReminderModal");
  var btn = document.getElementById("ReminderButton");
  var span = document.getElementsByClassName("close")[0];
  btn.onclick = function() {
    modal.style.display = "block";
  }
  span.onclick = function() {
    modal.style.display = "none";
  }

  /* $( document ).ready(function() {
    $('#moodForm').submit(function (e) {
      e.preventDefault();
      let formData = $(this).serializeArray();

      console.log(formData);

      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });

      $.ajax({
        method: "POST",
        url: "{{ route('mood.create') }}",
        data: {
          _token:$("input[name=_token]").val(),
          value: formData[2]['value'],
          date: formData[1]['value']
        },
        success: (event) => {
          console.log(event)
        }
      });
    });
  }); */
</script>

            {{-- @if ($task->absence_id != null)
            <div class="col-sm-5">
              <p style="font-weight: 400; font-size: 36px; color: white; margin-top: 10px;">{{ \App\Models\Absence::where('id', $task->id)->first()->name }}</p>
              <p style="margin-top: -15px; font-weight: 200;"><em>Saved</em></p>
            </div>
            @endif --}}
<!-- <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Tasks</h1>
</div>

@if (session()->has('success'))
<div class="alert alert-success" role="alert">
  {{ session('success') }}
</div>
@endif

<div class="table-responsive col-lg-12">
        @if (request()->get('date') == date('Y-m-d') || request()->get('date') == now()->subDay(1)->format('Y-m-d'))
        <a href="/dashboard/task/create" class="btn btn-primary mb-3">Add New Task</a>
        @endif
        
            
        <table id="dt" class="table table-striped table-sm">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Title</th>
              <th scope="col">Task Status</th>
              <th scope="col">Created Date</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($tasks as $task)
            <tr>
              <td>{{ $loop->iteration}}</td>
              <td>{{ $task->name }}</td>
              <td>{{ $task->task_status }}</td>
              <td>{{ $task->created_at->format('d F Y') }}</td>
              <td>
                <a href="/dashboard/task/{{ $task->id }}" class=" btn btn-info"><i class="fas fa-eye text-white"></i></a>
                <a href="/dashboard/task/{{ $task->id }}" class=" btn btn-warning"><i class="fas fa-edit text-white"></i></span></a>
                <a href="/dashboard/task/{{ $task->id }}" class=" btn btn-danger"><i class="fas fa-times"></i></span></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div> -->
@endsection