@extends('layouts.main')

@section('content')

{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

<style>
    .form-control{
        border: none!important;
        background-color: #F7F7F7;
        border-radius: 8px;
    }
    .select2-container--default .select2-selection--single{
        border: none;
        background-color: #F7F7F7;
        border-radius: 8px;
    }

    .maxChar {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 700px;
    }
</style>


<!-- Main content -->
<div class="content">
    <div class="container p-5">
        <div class="card card-default" style="margin-left: 160px; width: 70%; border-radius: 16px; border: none; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1);">
            <div class="card-body">

                @foreach ($tasks as $task)
                    @if ($task->project_id == 0)
                        <h1 class="ml-5 maxChar" style="color: #4107BB; margin-top: 20px;">{{ $task->daily_task_remark }}</h1>
                    @else
                        <h1 class="ml-5" style="color: #4107BB; margin-top: 20px;">{{ $task->name }}</h1>
                    @endif
                    
                    <h5 class="ml-5" style="color: #A8A8A8; margin-top: 10px; font-weight: 200;"><em>{{ $task->created_at->format('d F Y') }}</em></h5>
                @endforeach

                <div class="container">
        
                    @foreach ($tasks as $task)
                        <div class="row mt-4" style="margin-left: 3%;">
                            <div class="col" style="margin-right:-7%;">
                                <form>
                                    <fieldset disabled>
                                        @php
                                            $start = \Carbon\Carbon::parse($task->start_time)->format('H:i');
                                            $finish = \Carbon\Carbon::parse($task->finish_time)->format('H:i');
                                        @endphp

                                        <div class="row" style="margin-bottom: 2%;">
                                            <div class="col-sm-4">
                                                <label for="" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Start time</label>
                                            <input type="time" id="disabledTextInput" class="form-control" value="{{ $start }}">
                                            </div>
                                
                                            <div class="col-sm-4" style="margin-left: 23.5%;">
                                                <label for="" style="font-weight:200; font-size: 12px; color: #A8A8A8;">Finish time</label>
                                                <input type="time" id="disabledTextInput" class="form-control" value="{{ $finish }}">
                                            </div>
                                        </div>
                                        

                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Project Name</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $project }}">
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Project Remark</label>
                                            <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->project_remark }}</textarea>
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Task Name</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $task->name }}">
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Task Remarks</label>
                                            <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->daily_task_remark }}</textarea>
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Aditional Note</label>
                                            <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->note }}</textarea>
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Type of Work</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $type_of_work }}">
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Transportation</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $transportation }}">
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Site</label>
                                            <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $site }}">
                                        </div>
                                        <div class="form-group" style="width: 90%;">
                                            <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Site Remark</label>
                                            <textarea type="text" id="disabledTextInput" class="form-control" style="color: #868686;">{{ $task->site_remark }}</textarea>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Task Completion Status</label>
                                                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $task->task_status }}">
                                            </div>
                                
                                            <div class="col-sm-4" style="margin-left: 23.5%;">
                                                <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Approval Status</label>
                                                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{ $approval }}">
                                            </div>
                                        </div>

                                    </fieldset>
                                </form> <br><br>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

@endsection