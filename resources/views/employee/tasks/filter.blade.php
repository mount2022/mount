@extends('layouts-w-sidebar.main')

@section('content')
<div class="content-header p-5">
  <div class="container flex-column">
    <div class="row">
      <div class="col-sm-1 mr-3 mt-2">
        <h4 style="color: #4107BB;">Select</h4>
      </div>
      <div class="col-sm-2">
    <form action="/dashboard/task" method="get" >
    {{-- <form action="/dashboard/filter" method="get" > --}}
        @csrf
        <div class="form-group">
          <select id="month" name="month" class="form-control select2" style="width: 100%;">
            <option selected disabled>Month</option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">Sertember</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">Desember</option>
          </select>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <select id="year" name="year" class="form-control select2" style="width: 100%;">
            <option selected disabled>Year</option>
            <?php 
            $year = date('Y');
            $min = $year - 10;
            $max = $year;
            for( $i=$max; $i>=$min; $i-- ) {
            echo '<option value='.$i.'>'.$i.'</option>';
            }?>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <button type="submit" class="btn" style="background-color: #4107BB; color: #ffffff;">
          <img src="/image/search.png" alt="search">
        </button>
      </div>
    </form>
      <div class="col-sm-3 mt-2 justify-content-end" style="margin-left: 10%; margin-right: -15%;">
        <h4 style="color: #4107BB;"></h4>
      </div>
    </div>
  </div>
</div>

@endsection