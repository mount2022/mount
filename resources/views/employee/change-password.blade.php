@extends('layouts.main')

@section('content')

<style>
    .picture-container {
        position: relative;
        cursor: pointer;
        text-align: center;
    }
    .picture {
        width: 106px;
        height: 106px;
        background-color: #999999;
        border: 4px solid #CCCCCC;
        color: #FFFFFF;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        transition: all 0.2s;
        -webkit-transition: all 0.2s;
    }
    .picture:hover {
    border-color: #2ca8ff;
    }
    .content.ct-wizard-green .picture:hover {
        border-color: #05ae0e;
    }
    .content.ct-wizard-blue .picture:hover {
        border-color: #3472f7;
    }
    .content.ct-wizard-orange .picture:hover {
        border-color: #ff9500;
    }
    .content.ct-wizard-red .picture:hover {
        border-color: #ff3b30;
    }
    .picture input[type="file"] {
        cursor: pointer;
        display: block;
        height: 100%;
        left: 0;
        opacity: 0 !important;
        position: absolute;
        top: 0;
        width: 100%;
    }
    .picture-src {
        width: 100%;

    }

    .dropdown-item{
        color: #A8A8A8;
    }
    .dropdown-item:hover{
        background-color: #F7F9FF;
        color: #4107BB;
    }
    img.icon:hover, img.icon:focus{
        filter: invert(11%) sepia(98%) saturate(5938%) hue-rotate(263deg) brightness(70%) contrast(115%);
    }
</style>

<div class="row">
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:35%; height:100%; left:30%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <br>
                <!-- profile picture -->
                <div class="container">
                    <div class="picture-container">                        
                        <div class="picture">
                            @php
                                $path = url('user/images/' . auth()->user()->picture);
                            @endphp
                            <img src="{{ $path }}" class="picture-src" id="wizardPicturePreview" title="">
                            <input type="file" id="wizard_picture" name="wizard_picture" class="">
                        </div>
                    </div>
                </div>
                <!-- profile picture -->


                <br>
                <!-- sidebar button -->
                <div class="row">
                    <a class="nav-link" href="/dashboard/profile">
                        <img class="icon" src="/image/user.png" width="10%" alt=""> &nbsp; My Profile
                    </a>
                    <a class="nav-link {{ Request::is('dashboard/profile/change-password') ? 'active' : '' }}" href="/dashboard/profile/change-password"">
                        <img class="icon" src="/image/lock.png" width="10%" alt=""> &nbsp; Change Password
                    </a>
                    <a class="nav-link" href="/dashboard/profile/notification">
                        <img class="icon" src="/image/notif.png" width="10%" alt=""> &nbsp; Notification
                    </a>
                    <a class="nav-link" href="/dashboard/profile/kpi">
                        <img class="icon" src="/image/kpi.png" width="10%" alt=""> &nbsp; Key Performance Indicator
                    </a>
                    <a class="nav-link" href="/dashboard/profile/jobdesk">
                        <img class="icon" src="/image/jobdesc.png" width="10%" alt=""> &nbsp; Job Description
                    </a>
                    <a class="nav-link" href="/dashboard/profile/training">
                        <img class="icon" src="/image/training.png" width="10%" alt=""> &nbsp; Training Plan
                    </a>
                </div>
                <div class="row" style="margin-left: 5%; margin-top: 100%;">
                    <form action="/logout" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-sm profile-logout">LOG OUT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- change password -->
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:100%; height:100%; left:-35%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                @if (session()->has('success'))
                    <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                    </div>
                @endif
                <h4>Change Password</h4>
                <form method="POST" action="{{ route('user.password.update') }}">
                    @method('patch')
                    @csrf
                <div class="form-group" style="margin-top: 5%; width:50%;">
                    <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Your current password</label>
                    <input type="password" class="form-control @error('current_password') is-invalid @enderror" id="current_password" name="current_password" required placeholder="Current Password">
                        @error('current_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="form-group" style="margin-top: 5%; width:50%;">
                    <label for="password" style="font-weight:200; font-size: 12px; color: #1F1F1F;">New password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" required placeholder="New Password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group" style="margin-top: 5%; width:50%;">
                    <label for="" style="font-weight:200; font-size: 12px; color: #1F1F1F;">Confirm password</label>
                    <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required placeholder="Confirm Password">
                </div>

                <div class="row justify-content-end" style="margin-left: -20%;">
                    <div class="col-sm-3"> 
                        <button type="submit" class="btn" onclick="return confirm('Are you sure to change your password?')" style="background-color: #ffffff; color: #00DFC7; border-color: #00DFC7; margin-top: 25%;">APPLY CHANGES</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

@endsection


<!-- js profile picture -->
<!-- $(document).ready(function(){
// Prepare the preview for profile picture
    $("#wizard-picture").change(function(){
        readURL(this);
    });
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
} -->