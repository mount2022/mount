@extends('layouts.main')

@section('content')

<style>
    .picture-container {
        position: relative;
        cursor: pointer;
        text-align: center;
    }
    .picture {
        width: 106px;
        height: 106px;
        background-color: #999999;
        border: 4px solid #CCCCCC;
        color: #FFFFFF;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        transition: all 0.2s;
        -webkit-transition: all 0.2s;
    }
    .picture:hover {
    border-color: #2ca8ff;
    }
    .content.ct-wizard-green .picture:hover {
        border-color: #05ae0e;
    }
    .content.ct-wizard-blue .picture:hover {
        border-color: #3472f7;
    }
    .content.ct-wizard-orange .picture:hover {
        border-color: #ff9500;
    }
    .content.ct-wizard-red .picture:hover {
        border-color: #ff3b30;
    }
    .picture input[type="file"] {
        cursor: pointer;
        display: block;
        height: 100%;
        left: 0;
        opacity: 0 !important;
        position: absolute;
        top: 0;
        width: 100%;
    }
    .picture-src {
        width: 100%;

    }

    .dropdown-item{
        color: #A8A8A8;
    }
    .dropdown-item:hover{
        background-color: #F7F9FF;
        color: #4107BB;
    }
    img.icon:hover, img.icon:focus{
        filter: invert(11%) sepia(98%) saturate(5938%) hue-rotate(263deg) brightness(70%) contrast(115%);
    }
    .column-title{
        background: linear-gradient(270deg, #42A2D0 0%, #00DFC7 100%), linear-gradient(0deg, #A8A8A8, #A8A8A8);
        color: white;
        height: 3.2rem;
    }
    th.column {
        font-size:12px;
        text-align: center;
        color: white;
        background: #A8A8A8;
    }
</style>

<div class="row" style="padding-bottom: 20%;">
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:35%; height:100%; left:30%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <br>
                <!-- profile picture -->
                <div class="container">
                    <div class="picture-container">                        
                        <div class="picture">
                            @php
                                $path = url('user/images/' . auth()->user()->picture);
                            @endphp
                            <img src="{{ $path }}" class="picture-src" id="wizardPicturePreview" title="">
                            <input type="file" id="wizard_picture" name="wizard_picture" class="">
                        </div>
                    </div>
                </div>
                <!-- profile picture -->


                <br>
                <!-- sidebar button -->
                <div class="row">
                    <a class="nav-link" href="/dashboard/profile">
                        <img class="icon" src="/image/user.png" width="10%" alt=""> &nbsp; My Profile
                    </a>
                    <a class="nav-link" href="/dashboard/profile/change-password">
                        <img class="icon" src="/image/lock.png" width="10%" alt=""> &nbsp; Change Password
                    </a>
                    <a class="nav-link" href="/dashboard/profile/notification">
                        <img class="icon" src="/image/notif.png" width="10%" alt=""> &nbsp; Notification
                    </a>
                    <a class="nav-link" href="/dashboard/profile/kpi">
                        <img class="icon" src="/image/kpi.png" width="10%" alt=""> &nbsp; Key Performance Indicator
                    </a>
                    <a class="nav-link" href="/dashboard/profile/jobdesk">
                        <img class="icon" src="/image/jobdesc.png" width="10%" alt=""> &nbsp; Job Description
                    </a>
                    <a class="nav-link active" href="/dashboard/profile/training">
                        <img class="icon" src="/image/training.png" width="10%" alt=""> &nbsp; Training Plan
                    </a>
                </div>
                <div class="row" style="margin-left: 5%; margin-top: 100%;">
                    <form action="/logout" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-sm profile-logout">LOG OUT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- KPI  -->
    <div class="col">
        <div class="card card-default border-0" style="background: white; color:#1f1f1f; box-shadow: 0px 5px 80px rgba(0, 0, 0, 0.1); border-radius: 16px;
        width:100%; height:100%; left:-35%; top:10%;">
            <div class="card-body" style="padding: 5%;">
                <h4>Training plan</h4>
                &nbsp;
                @foreach($TrainingTitle as $value)
                    <div class="row">
                        <div class="col-sm-3">
                            <p style="color: #A8A8A8; font-weight: 400;">Title</p>
                        </div>
                        <div class="col">
                            <p style="font-weight: 400;">{{ $value->training_type }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <p style="color: #A8A8A8; font-weight: 400;">Period</p>
                        </div>
                        <div class="col">
                            <p style="font-weight: 400;">{{ $value->when }}</p>
                        </div>
                    </div>
                @endforeach

                @if($Trainings->isEmpty() != true)
                    <table class="table table-bordered" style="margin-top: 20px;">
                        <thead>
                            <tr>
                            <th scope="col" class="column align-middle">NO</th>
                            <th scope="col" class="column align-middle">TRAINING TYPE</th>
                            <th scope="col" class="column align-middle">WHEN</th>
                            <th scope="col" class="column align-middle">OBJECTIVES</th>
                            <th scope="col" class="column align-middle">PARTICIPANTS</th>
                            <th scope="col" class="column align-middle">TRAINING REQUIREMENT</th>
                            <th scope="col" class="column align-middle">NOTES</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $number = 1;
                            @endphp
                            @foreach($Trainings as $training)
                                <tr>
                                    <td style="text-align: center;">{{ $number++ }}</td>
                                    <td>{{ $training->training_type }}</td>
                                    <td style="text-align: center;">{{ $training->when }}</td>
                                    <td>{{ $training->objective }}</td>
                                    <td style="text-align: center;">{{ $training->participant }}</td>
                                    <td>{{ $training->requirement }}</td>
                                    <td style="text-align: center;">{{ $training->note }}</td>
                                    <td style="display: none;"><input type="hidden" class="tabledit-input" name="user_id" value="{{ request()->route('id') }}"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="row">
                        <p style="color: #A8A8A8; font-weight: 400; text-align: center;">No Data</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<meta name="user-id" content="{{ auth()->user()->id }}">

@endsection