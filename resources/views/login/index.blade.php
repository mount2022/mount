@extends('layouts.main')

@section('content')

<div class="container" style="width: 60%; padding-top: 7%;">
    @if(session()->has('loginError'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('loginError') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    @endif

    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card" style="border-radius: 16px; border: none; box-shadow: 0px 4px 200px rgba(0, 0, 0, 0.15);">
          <img src="/image/mount-logo.png" width="50%" alt="logo" class="card-title pl-5 mt-5">
          <div class="card-body p-4 p-sm-5">
            <p class="card-title text-center mb-4 fw-bold fs-5 login-title" style="font-weight: 300;">Log In</p>
            <form action="/login" method="post">
                @csrf
              <div class="form-outline mb-3" >
                <input style="border-radius: 8px;" type="email" name="email" class="form-control p-4 @error('email') is-invalid @enderror" id="email" placeholder="Email" autofocus required value="{{ old('email') }}">
                @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
              </div>
              <div class="form-outline mb-3">
                <input style="border-radius: 8px;" type="password" name="password" class="form-control p-4" id="password" placeholder="Password" required>
              </div>

              {{-- <div class="form-check mb-3">
                <input class="form-check-input" type="checkbox" value="" id="rememberPasswordCheck">
                <label class="form-check-label login-label" for="rememberPasswordCheck">
                  Keep me signed in
                </label>
              </div> --}}
              <div class="d-grid">
                <button class="btn btn-login text-uppercase fw-bold" type="submit" style="width: 100%; background: linear-gradient(90deg, #00DFC7 27.29%, #42A2D0 72.86%); font-weight: 600; color: white; border-radius: 8px">LOG IN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection