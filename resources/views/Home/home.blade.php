@extends('layouts.main')

@section('content')

<div class="container" style="width: 85%; padding-top: 10%; background-image: url({{asset('../image/bg-triangle.png')}})">

	<div class=row>
		<div class="col" style="text-align:right;">
			<img src="/image/waving-hand.png" width="380px" alt="illustration" style="margin-right: 15%;">
		</div>
		
		<div class="col">
			<img src="/image/mount-logo.png" width="140px" alt="logo" style="margin-bottom: 9%">
			<h1 style="font-weight: 600"> Hi, </h1>
			<h1 style="font-weight: 400; margin-top: -4px; margin-bottom: 3%"> welcome to <a style="color: #4107BB">Mount</a> ! </h1>
			<a>An application to integrate manhour</a><br>
			<a>and task reports in your company.</a>
			<div>
				<a href="/login">
					<button class="btn text-uppercase" type="submit" style="width: 190px; height: 48px; background: linear-gradient(90deg, #00DFC7 27.29%, #42A2D0 72.86%); font-weight: 500; color: white; margin-top:9%; border-radius: 8px"> LOG ME IN </button>
				</a>
			</div>
		</div>
	</div>

</div>

@endsection