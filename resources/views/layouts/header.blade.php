<!-- Navbar -->
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
	<div class="container">
	  <a href="#" class="navbar-brand">
	    <img src="https://www.southcharlottefamilycounseling.com/wp-content/uploads/2015/10/cropped-logo-dummy.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
	    <span class="brand-text font-weight-light">Mount</span>
	  </a>

	  <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

	  <div class="collapse navbar-collapse navbar-nav navbar-no-expand ml-auto" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
			<li><a href="/dashboard" class="dropdown-item">Overview</a></li>
			<li><a href="/dashboard/tasks" class="dropdown-item">Task Sheet</a></li>
        </ul>
    </div>

    <!-- Right navbar links -->
	  <ul class="navbar-nav navbar-no-expand ml-auto">
      	<li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <div class="media media-profile">
            <img src="{{url('plugins/dist/img/user2-160x160.jpg')}}" alt="User Avatar" class="img-size-32 mr-3 img-circle">
            <div class="media-body">
              <h3 class="dropdown-item-title">Adrian Putra</h3>
            </div>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="user-header">
          	<img src="{{url('plugins/dist/img/user2-160x160.jpg')}}" alt="User Avatar" class="profile-user-img img-fluid img-circle">
      		<h5>Adrian Putra</h5>
            <p class="position">Architect</p>
            <p class="company">PT. Meinhardt EPCM Indonesia</p>
          </div>
          <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See Profile</a>
          <div class="user-footer">
          	<div class="float-left">
                <a href="javascript:;" class="btn btn-warning btn-flat change-pass d-xs-none">Change Password</a>
                <a href="javascript:;" class="btn btn-warning btn-default btn-flat change-pass d-none d-xs-block">Change Password</a>
            </div>
            <div class="float-right">
                <form action="/logout" method="post">
                  @csrf
                  <button type="submit" class="dropdown-item"><i class="bi bi-box-arrow-right"></i> Logout</button>
                </form>
            </div>
          </div>
        </div>
      </li>
	  </ul>
	</div>
</nav>
<!-- /.navbar -->