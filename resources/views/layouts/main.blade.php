<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Mount</title>

  <!-- Favicons -->
  <!-- <link href="{{url('sites/img/favicon.png')}}" rel="icon">
  <link href="{{url('sites/img/apple-touch-icon.png')}}" rel="apple-touch-icon"> -->

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{url('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{url('css/styles.css')}}">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <!-- Poppins Font -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <!-- ijaboCropTool -->
  <link rel="stylesheet" href="{{ url('plugins/ijaboCropTool/ijaboCropTool.min.css') }}">

  <!--- For CSS --->
  <?php
    if ( isset($site['css']) ){
        foreach($site['css'] as $css){
            $exp = explode(",", $css);
            echo "<link type=\"{$exp[0]}\" rel=\"{$exp[1]}\" href=\"{$exp[2]}\" />";
        }
      }
  ?>
  <link rel="stylesheet" href="{{ url('/css/old-styles.css') }}">

  <!-- Datepicker -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>

<body class="hold-transition layout-top-nav">
<div class="wrapper">
  @auth
    @include('layouts.navbar')
  @endauth

  <div class="content-wrapper" style="background-color: white;">
    @yield('content')
  </div>

  @include('layouts.footer')
</div>

<!-- JavaScript Libraries -->
<script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{url('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('plugins/dist/js/adminlte.min.js')}}"></script>
<script src="{{ url('plugins/ijaboCropTool/ijaboCropTool.min.js') }}"></script>

<!-- Pusher -->
<script src="/js/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script>

<!--- For JS --->
<?php
    if ( isset($site['js']) ){
        foreach($site['js'] as $js)
          echo "<script src=\"{$js}\"></script>";     
    }
?>

</body>
</html>
