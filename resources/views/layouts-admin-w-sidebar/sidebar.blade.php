<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style="padding-bottom: 35px; padding-top: 22px; margin-left: 7px;">
        <img src="/image/mount-mht-logo.png" alt="AdminLTE Logo" class="brand-image" style="width: 70%; margin-left: 13%;">
    </a>

    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            
                @foreach ($dates as $date)    
                    <li class="nav-item">
                        @if ($date->format('Y-m-d') == session('date') && request()->get('id') == null)
                        <a href="/dashboard/task-detail?date={{ $date->format('Y-m-d') }}" class="nav-link" style="background: #F7F9FF; color: #4107BB;">
                        @else
                        <a href="/dashboard/task-detail?date={{ $date->format('Y-m-d') }}" class="nav-link">
                        @endif
                            
                            @if ($date->format('d F Y') == $today->format('d F Y'))
                                <!-- today -->
                                <img src="/image/taskstatus-today.png" alt="" style="width: 10%;">
                                <p style="color: #4107BB; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                <p style="color: #4107BB; margin-left:18%; margin-top:0%; font-size: 10px;">Today</p>
                            @elseif ($date < $today)
                                @if ($date->format('d F Y') == $today->yesterday()->format('d F Y'))
                                    <!-- yesterday -->
                                    <img src="/image/taskstatus-yesterday.png" alt="" width="10%">
                                    <p style="color: #FCA010; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                    <p style="color: #FCA010; margin-left:18%; margin-top:0%; font-size: 10px;">Yesterday</p> 
                                @else

                                    @php
                                        date_default_timezone_set('Asia/Jakarta');
                                        if(request()->get('id') != null){
                                            $id = request()->get('id');
                                        } else {
                                            $id = session('user_id');
                                        }

                                        $unlock = \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', $id)->count();
                                        if($unlock != 0) {
                                            $end = \App\Models\Unlock::whereDate('date_unlock', request()->get('date'))->where('user_id', $id)->latest()->first()->end;
                                        } else {
                                            $end = null;
                                        }
                                    @endphp
                                    {{-- @if (request()->get('id') != null) --}}
                                        @if (\App\Models\Task::whereDate('created_at', $date)->where('user_id', $id)->count() > 0 || $end != null)
                                            @if (date('Y-m-d H:i:s') <= $end && date('Y-m-d H:i:s') >= $end)
                                                <!-- Unlock -->
                                                <img src="/image/taskstatus-filled.png" alt="" width="10%">
                                                <p style="color: #1F1F1F; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #1F1F1F; margin-left:18%; margin-top:0%; font-size: 10px;">Unlock</p>
                                            @elseif (\App\Models\Task::whereDate('created_at', $date)->where('user_id', $id)->count() > 0 
                                            && date('Y-m-d H:i:s') <= $end)
                                                <!-- filled -->
                                                <img src="/image/taskstatus-filled.png" alt="" width="10%">
                                                <p style="color: #1F1F1F; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #1F1F1F; margin-left:18%; margin-top:0%; font-size: 10px;">Filled</p>
                                            @elseif (\App\Models\Task::whereDate('created_at', $date)->where('user_id', $id)->count() > 0)
                                                <!-- filled -->
                                                <img src="/image/taskstatus-filled.png" alt="" width="10%">
                                                <p style="color: #1F1F1F; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #1F1F1F; margin-left:18%; margin-top:0%; font-size: 10px;">Filled</p>
                                            @else
                                                <!-- missed/null -->
                                                <img src="/image/taskstatus-missed.png" alt="" width="10%">
                                                <p style="color: #FF6060; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #FF6060; margin-left:18%; margin-top:0%; font-size: 10px;">Missed</p>
                                            @endif      
                                        @else
                                            <!-- missed/null -->
                                            <img src="/image/taskstatus-missed.png" alt="" width="10%">
                                            <p style="color: #FF6060; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                            <p style="color: #FF6060; margin-left:18%; margin-top:0%; font-size: 10px;">Missed</p>
                                        @endif

                                    {{-- @else
                                        @if (\App\Models\Task::whereDate('created_at', $date)->where('user_id', session('user_id'))->count() > 0 
                                        || \App\Models\Unlock::whereDate('date_unlock', $date)->where('user_id', session('user_id'))->count() > 0)
                                            @if (date('Y-m-d H:i:s') <= \App\Models\Unlock::whereDate('date_unlock', $date)->where('user_id', session('user_id'))->latest()->first()->end)
                                                <!-- Unlock -->
                                                <img src="/image/taskstatus-filled.png" alt="" width="10%">
                                                <p style="color: #1F1F1F; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #1F1F1F; margin-left:18%; margin-top:0%; font-size: 10px;">Unlock</p>
                                            @elseif (\App\Models\Task::whereDate('created_at', $date)->where('user_id', session('user_id'))->count() > 0 
                                            && date('Y-m-d H:i:s') <= \App\Models\Unlock::whereDate('date_unlock', $date)->where('user_id', session('user_id'))->latest()->first()->end)
                                                <!-- filled -->
                                                <img src="/image/taskstatus-filled.png" alt="" width="10%">
                                                <p style="color: #1F1F1F; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #1F1F1F; margin-left:18%; margin-top:0%; font-size: 10px;">Filled</p>
                                            @elseif (\App\Models\Task::whereDate('created_at', $date)->where('user_id', session('user_id'))->count() > 0)
                                                <!-- filled -->
                                                <img src="/image/taskstatus-filled.png" alt="" width="10%">
                                                <p style="color: #1F1F1F; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #1F1F1F; margin-left:18%; margin-top:0%; font-size: 10px;">Filled</p>
                                            @else
                                                <!-- missed/null -->
                                                <img src="/image/taskstatus-missed.png" alt="" width="10%">
                                                <p style="color: #FF6060; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                                <p style="color: #FF6060; margin-left:18%; margin-top:0%; font-size: 10px;">Missed</p>
                                            @endif   
                                        @else
                                            <!-- missed/null -->
                                            <img src="/image/taskstatus-missed.png" alt="" width="10%">
                                            <p style="color: #FF6060; margin-left:5%;">{{ $date->format('D, d M Y') }}</p> </br>
                                            <p style="color: #FF6060; margin-left:18%; margin-top:0%; font-size: 10px;">Missed</p>
                                        @endif
                                    @endif --}}
                                @endif
                            @else
                                <!-- tomorrow -->
                                <img src="/image/taskstatus-tomorrow.png" alt="" style="width: 10%;">
                                <p style="color: #A8A8A8; margin-left:5%;">{{ $date->format('D, d M Y') }}</p>
                            @endif
                        </a>
                    </li>
                @endforeach

            </ul>
        </nav>
    </div>
</aside>