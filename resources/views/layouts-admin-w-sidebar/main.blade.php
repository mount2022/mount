<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Mount</title>
  <!-- Favicons -->
  <!-- <link href="{{url('sites/img/favicon.png')}}" rel="icon">
  <link href="{{url('sites/img/apple-touch-icon.png')}}" rel="apple-touch-icon"> -->

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{url('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <link rel="stylesheet" href="{{url('plugins/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{url('css/styles.css')}}">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <!-- Poppins Font -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <!--- For CSS --->
  <?php
    if ( isset($mainapp['css']) ){
        foreach($mainapp['css'] as $css){
            $exp = explode(",", $css);
            echo "<link type=\"{$exp[0]}\" rel=\"{$exp[1]}\" href=\"{$exp[2]}\" />";
        }
      }
  ?>
  <link rel="stylesheet" href="/css/old-styles.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<!-- Site wrapper -->
<div class="wrapper">
  @include('layouts-admin-w-sidebar.navbar')

  @include('layouts-admin-w-sidebar.sidebar')

  <div class="content-wrapper">
    <section class="content">
      @yield('content')
    </section>
  </div>

  @include('layouts-admin-w-sidebar.footer')

  <aside class="control-sidebar control-sidebar-light">
    <!-- Control sidebar content goes here -->
  </aside>
</div>

<!-- JavaScript Libraries -->
<script src="{{url('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{url('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<script src="{{url('plugins/dist/js/adminlte.min.js')}}"></script>

<!-- Pusher -->
<script src="/js/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script>

<!--- FOr JS --->
<?php
    if ( isset($mainapp['js']) ){
        foreach($mainapp['js'] as $js)
          echo "<script src=\"{$js}\"></script>";     
    }
?>
</body>
</html>
