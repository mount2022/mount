<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KPISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('default_kpis')->insert([
            'user_id' => '0',
            'kpi' => 'No Data',
            'value' => 'No Data',
            'target' => 'No Data',
            'metric' => 'No Data',
            'realization' => 'No Data',
            'score' => 'No Data',
            'final_score' => 'No Data',
        ]);
    }
}
