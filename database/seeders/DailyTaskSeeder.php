<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DailyTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //semua department
        //department 1
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Training'
        ]);

        //department 2
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'Training'
        ]);

        //department 3
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'Training'
        ]);

        //department 4
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Training'
        ]);

        //department 5
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Training'
        ]);

        //department 6
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Training'
        ]);

        //department 7
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Training'
        ]);

        //department 8
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Training'
        ]);

        //department 9        
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Training'
        ]);

        //department 10
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Training'
        ]);

        //department 11
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Internal Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'External Meeting & Coordination'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Adhoc/Urgent Request from anyone'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Training'
        ]);






        //Department 1
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Detail Drawing'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Layouting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Permit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Site Assesment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Site Survey (NTI)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Site Survey (NTS)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Site Survey (Franchise Prog)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Modular Building'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Safe access maintenance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Solar Panel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Eco Pavement'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - GEN5 Lowcost'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Single skin canopy'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - TCC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Nitrogen House'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Prince Project'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - DAR 1'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - DAR 2'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Permit Drawing'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - Permit to PTSP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Architect - FUS'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Drafter - DED'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Drafter - Supporting Drawing request'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Drafter - Permit Drawing'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Drafter - Standard Drawing'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Drafter - Family Revit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Drafter - Site Survey'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Site Report Assessment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Permit Inrit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Checking for soil test'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Calculation Soakwel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Calculation Drainage'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Calculation land Development'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Calculation retaining wall'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Civil - Site Survey'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - Calculation for foundation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - Calculation for structure clumn'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - Steel Structure Calculation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - TCC Canopy'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - Calculation UGT'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - Checking Soaring Report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - Checking report assessment for soil test'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Structure - Site Survey'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Calculation biofilter capacity'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Calculation UGT capacity'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Calculation rainwater capacity'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Calculation Drainage Capacity'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Clean Water Supply Diagram'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Waste Water Treatment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Calculation for SIPA'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Calculation Gutter'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Allignment with civil work'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Canopy Rrepair'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Mechanical - Site Survey'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Calculation for electrical diagram'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - FAT'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Connecting with PLN'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Calculation Kwh meter'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Single line Diagram'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Grounding repair'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Panel Instrument'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - CO2 Reduction'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Solar Panel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - EV Charger'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '1',
            'name' => 'Electrical - Site Survey'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '1',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 2
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'PMC - NTI - Design layouting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'PMC - NTI - Design review'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'PMC - NTI - FSY Tracker'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'PMC - NTI - Quotation review'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'PMC - NTI - Site Visit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'PMC - NTI - Site Visit - Tank Sinking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'PMC - NTI - Site Visit - Maiden Delivery'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - RBSAM - CRR arrangement'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - RBSAM - Quotation Review'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - RBSAM - Site Visit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - RBSAM - Site Visit - OTP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - Dispenser Replacement - CRR arrangement'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - Dispenser Replacement - Quotation Review'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - Dispenser Replacement - Site Visit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'FMC - Dispenser Replacement - Site Visit - OTP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'On Board Training - Training material arrangement'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '2',
            'name' => 'On Board Training - Training session'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '2',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 3
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Pre Project - Preliminary Survey Report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Pre Project - Conduct the land handover'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Pre Project - Arrange the NTC (Prior the project)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Initiation stage - Review and approve DAR-2'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Initiation stage - finalize the CRR document (F2F)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Initiation stage - Review and approve the CRR document'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Initiation stage - Submit the DAR-2 to the client'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Initiation stage - Submitted the CRR document to the client'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Initiation stage - Project Kickoff Meeting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - UST installation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Maiden Delivery'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - TERA (Metrology)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Dry Run (ensure of sales)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - COI (SERTCO/MIGAS)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Select Readiness (Handover Select Shop)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Conduct of HSSE Audit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Conduct of TNT'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - HSSE documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - CQC documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Asset register document'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - MOC documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - As-built drawing preparation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Tank Sinking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Post-buried LOB test'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - Maiden Delivery'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Subsequent stage - COI preparation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Final stage - TERA (Metrology)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Final stage - COI (SERTCO/MIGAS)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Final stage - Final handover and OTP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Final stage - Project documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Final stage - HSSE documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'PM - Post Project - Maintain of communication with L2 & Retailer'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Pre Project - Preliminary Survey Report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Pre Project - Join with the PCC to carry out the NTC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Initiation stage - Develop and compile the DAR-2'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Initiation stage - Followed up the CRR document with L3'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Initiation stage - Complete all concerns during meeting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Initiation stage - Project Kickoff Meeting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Civil works'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Fuel system works'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Electrical works'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Mechanical works'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Utility works'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Commissioning works'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Conduct of HSSE Audit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Conduct of TNT'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - HSSE documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - CQC documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Asset register document'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - MOC documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - As-built drawing preparation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Tank Sinking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Post-buried LOB test'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - Maiden Delivery'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Subsequent stage - COI preparation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Final stage - TERA (Metrology)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Final stage - COI (SERTCO/MIGAS)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Final stage - Final handover and OTP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Final stage - Project documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Final stage - HSSE documents (nmpi, t&t, hsse audit)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => "SE - Post Project - Followed up each any WO's from the CBRE"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '3',
            'name' => 'SE - Post Project - Conduct LOPC audit after 1 month OTP'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '3',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 4
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Human Performance - PH / PI Training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Human Performance - HSSE On Boarding Training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Human Performance - Prepare & Conduct HSSE Meeting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Human Performance - Prepare & Conduct HSSE Socialization'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Human Performance - Monitor L2 and L3 HSSE Performance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Review CRR & BCP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Review Lifting Plan & Any Emergency Docs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Review MOC related to critical HSSE items'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Review HSSE Hand Over + HSSE KPI'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Making HSSE Documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Prepare & review HSSE Docs for LOD-1 & CF Audit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Support MHT HSSE Global Team'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Review Greenbanding for newly L3 or exist. L3'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Monitor Lucidity within the L2 and L3'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Provide Risk Register as part of DAR-2 Doc'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Initative Program (Tl Study NTI Cipendawa)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Analysis L3 HSSE KPI Q2 Lucidity System'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - E-PTW review'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Submission HSSE Reporting in lucidity system'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Project Tech & Docs - Monthly NMPI Analysis'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Operational - HSSE Visit and Conduct inspection for several items'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Operational - HSSE Coaching/Training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Operational - HSSE Campaign'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Operational - Mandatory visit (Tank sinking, Maiden Delivery, or etc)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '4',
            'name' => 'Operational - Conduct Incident Investigation'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '4',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 5
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Scheduler - Weekly report NTI'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report VA's"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report AI's"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report SVR & DPR"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report RBSAM"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report CBRR"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report GEI"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report Prince-1"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report Prince-2"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Weekly report Prince-3"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Baseline Schedule and Updated DAR-1"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Baseline Schedule and Updated DAR-2"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Baseline Schedule and Updated CRR"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Scheduler - Update Ship Data Compile Report"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - Monitoring Project ongoing thru MoM Daily'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - Provide Design Assurance Review (DAR-2)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - Discipline Control & Assurance Framework'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - FUS'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - DAR 1'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - DAR 2'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - CRR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - Asset Register'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - Summary for  COI Alignment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - Site Delivery Plan'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - POA for PLN'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - POA for PDAM'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - POA for TELKOM'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - POA for LandHandOver'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => 'Technical Asst. - KPI Tracker'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Technical Asst. - DLP - WO's MHT (including SoW)"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check & Review Handover docs from L3's"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Email reminder to all L3's for submit handover docs"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check availability & Review Handover Docs from all L3's"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Prepare Handover docs to CBRE"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check Commissioning & Training Docs from L3's"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check Pre handover & Final Handover Docs"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check Snaglist Document"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check Softcopy As Built Drawing Docs from L3's"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check Declare DLP Period & LOPC Audit"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check for Asset Register and email submission to CBRE"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Deliver hardcopy of As Built Drawing to Shell & CBRE"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Deliver hardcopy of As Built Drawing to Retailer"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Upload Handover docs in OneDrive & Share to CBRE"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Prepare Project Close Out docs to Shell"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check permitting Document and put into folder permit"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check IP approved Document"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check Approval IP approved Document"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check DAR 1, DAR 2 & CRR (Included the approval)"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Request & Check availability financial document"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check availability HSSE Docs"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check & Review Weekly Meeting & Report (MOM)"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check availability Site photos from all of L3's"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check availability As Built Drawing (CAD & PDF)"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Check COI, Tera Certificate & SLO Document"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Provide Tracker for Close Out Document to Shell"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Upload Project Close Out docs in Share Point & Share"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Update DED Drawing approved in server"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Update MOC request & save in server"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Arrange Permit Form to L3's & update to Tracker"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Provide Letter request & save in server"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Update Permit docs in server & share to team"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Update COI, SLO & Tera Certificate"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Update Tracker Project Close out Doc Submission"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Follow up & respone any request from email"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Update General docs in SharePoint"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '5',
            'name' => "Doc Cont - Prepare Audit (if any)"
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '5',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 6
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - IDD'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Vendor Registration'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - ABC Memo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Greenbanding doc'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Contract HSSE Plan'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - RFQ new L3'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - RFQ Comparison PCC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - RFQ Comparison Non PCC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Nego'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Contract Draft'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Final contract to Shell'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Outline Agreement (OA)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - New L3 Tracker'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Collect CV from L3'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - EnC Refreshment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Contract Clause Workshop - Slide Pack'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Contract Clause Workshop - Presentation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Contract - Compile Worker welfare Mgm Plan'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - GCCB 1'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - GCCB 2'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - BOQ Initial for Tender'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Bid Comparison vs TNT (Initial)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - BOQ Initial (Term-1)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Final Sync DED - Civil'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Final Sync DED -  ME'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - BOQ Sync vs TNT (SOR exercise)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Permit'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Soil Test / Topo / GPR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Dispenser'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Tanks'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - RVIe Mat'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Genset'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Stabilizer'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Lamps (LUCI, PJU, Indoor Lamp)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Ibank'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - IT Equipment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Antivirus'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - FS Material'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - FS Installation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - RVIe Install'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Select Shop / Happy Box (Non PMT)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - AC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - CCTV'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Coffee Machine'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Lube Bay Equipment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - APAR APAB'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Safety Box'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Sticker (Lube Bay, Store, HSSE, etc)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Winix'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Envoy'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Solar Cell'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - EV Charger'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Cost Accuracy'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Provide VE refer to the site HO (NTI)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Saving Reinvested'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - RAB for Permitting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'Cost Eng - Sudden Request'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C Manager - Contracting Strategy & GCCB-1'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C Manager - Contracting Strategy & GCCB-2'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C Manager - Tender Score'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C Manager - PR PO Instruction'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C Manager - Budget Monitoring'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C Manager - Value Eng Workshop'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C Manager - Review all task'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - ManagerTender Invitation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - Submission from L3'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - Bid Comparison'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - Notif Letter'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - Sync Nego'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - Progress Report Sync'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - Tender Score'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - Notif email'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - GCCB-1'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '6',
            'name' => 'C&C - GCCB-2'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '6',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 7
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'PR/PO Focal - PR Creation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'PR/PO Focal - Request PO issuance to Shell'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'PR/PO Focal - Email PO to L3'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'PR/PO Focal - PO Rev/Del - Email to CP Focal'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'PR/PO Focal - PR/PO Tracker'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'PR/PO Focal - PO Issues Resolution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'PR/PO Focal - Budget Monitoring'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Invoice Processing Focal - Proforma Invoice Checking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Invoice Processing Focal - SE/GR Creation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Invoice Processing Focal - Invoice Issues Escalation & Resolution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Invoice Processing Focal - Invoice Tracker'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Invoice Processing Focal - Account Summary Follow-Up'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Invoice Processing Focal - Invoice Refresher Workshop'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Asset Creation Focal - CAPEX Accrual & Reaccrual'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Asset Creation Focal - Cost Compilation (per project)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Asset Creation Focal - Cost to Asset Classification (per project)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Asset Creation Focal - Asset Classification vs Asset Register Checking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Asset Creation Focal - Asset Creation Form'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Asset Creation Focal - Asset Creation Recon'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Asset Creation Focal - Asset Write-Off Form'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Delivery Scheduling'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Delivery Follow-Up'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: MSA'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: FS & Dispenser'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: IT Equipment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: RVI'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: UST'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: Lighting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: Genset & Stabilizer'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: APAR & Safe Deposit Box'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Update: Other'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Stock Tracker Report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - FS Material Delivery Checking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Account Summary Reminder & Compilation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Logistic Focal - Account Summary Follow-Up'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Delivery Scheduling'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Opname: MSA'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Update & Swap: FS & Dispenser'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Update & Swap: IT Equipment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Update & Swap: RVI'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Update & Swap: UST'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Update & Swap: Lighting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Update & Swap: Genset & Stabilizer'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Update & Swap: Other'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock Tracker'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Material Demand Forecasting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Warehouse Reporting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - RVI Shipping Documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Stock & Delivery Issues Resolution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Ass Manager - Vendor Management'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - AUC Reporting (Monthly & Quarterly)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - CAPEX Accrual & Reaccrual'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Cost Compilation (per project)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Cost to Asset Classification (per project)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Asset Classification vs Asset Register Checking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Asset Creation Form'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Asset Creation Recon)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Asset Write-Off Form'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Vendor Management'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - PO Issues Resolution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Invoice Issues Resolution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Asset Issues Resolution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '7',
            'name' => 'Proc Manager - Logistic Issues Resolution'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '7',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 8
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Collecting, interpreting, and reviewing financial information'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Predicting future financial trends'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Reporting to management and stakeholders'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Producing financial reports related to budgets'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Producing financial reports related to AP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Producing financial reports related to AR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Producing financial reports related to expense'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Consolidation tax documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'PPH 21 from HR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'PPH 23 from AP & AR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'PPH 4(2) from AP & AR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'VAT from AP & AR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Monthly & Yearly Tax Report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Review & adjustment journals'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Monthly closing report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Budgeting & cash flow'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Request tax payment to AR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Liase with external auditors & Tax consultant'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Prepare PV'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Prepare PO'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Received and handle all document from vendor and contractor'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Review document from AR & AP'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Input data to Accurate'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Completed the document and file'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Handle Petty Cash'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Assist to update cashflow from budget projection until actual'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Received and handle document reimburse'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Input to software'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Completed and file all document'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Installing and configuring computer hardware, software, systems, network, printer'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Providing technical support across the company (via phone or in person)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Diagnose and troubleshooting technical hardware and software issues'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Identify and escalate situations requiring urgent attention'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '8',
        //     'name' => 'Plan, build and maintain application based on business need, provide ready solution for user based on business need, support in house application and ensure business requirement for IT documented properly'
        // ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Assist GA to give recommendation on laptop purchasing and other technological devices'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Manage software upgrade, changes and updates'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Prepare activity report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Preparing and replacing equipment as necessary'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Maintain records of assets especially laptop, pc, mouse and etc'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Calculate salary and generate Tax in Talenta.'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Calculate Extra Hour Allowance & Inlieu'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Update Calculation BPJS K & TK'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Calculate THR (Include Tax)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Calculate Bonus (Include Tax)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Payslip recheck and send'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Update excel database (additional & Change) - BPJS TK'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Update excel database (additional & Change) - BPJS K'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Update excel database (additional & Change) - Lippo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Process BPJS K & TK & Insurance registration'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Process BPJS K & TK & Insurance deactivation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Create PV for Salary'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Create PV for Social Burden (BPJSK, BPJS TK), Insurance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Create PV for THR or Bonus'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Compile hard copy PV and Salary details'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Compile hard copy PV for Social Burden (BPJSK, BPJS TK), Insurance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Compile hard copy PV for THR or Bonus'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Create Compensation and Benefit Policy'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Flow chart of Salary (additional & Changes)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Explain cases C&B (based on FAQ)'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '8',
        //     'name' => 'Create flow chart & Explain Compensation for contract employee and  permanent employee (UU Cipta Kerja)'
        // ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '8',
        //     'name' => 'Create flow chart & Explain benefit for contract employee and permanent employee (UU Cipta Kerja)'
        // ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Calculate Saverance payment & termination cost based on Fee from Client'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Involve in the process of job evaluation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Involve in the process of job evaluation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Employee data base'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Compensable Factor Dictionary'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Salary survey'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Grade salary structure'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Actual salary data base'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Point calculation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Point factor ideal'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Indvidual Report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Compile to all staff report'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Review and update Job Evaluation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'analyze extra hour allowance / month'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Insurance sosialisation (New hire)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Gain or Loss calculation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Analyze the staff cost based on invoice'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Update cost center for project'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Adjustment history employee'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'compile adjustment letter'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Forecast staff remuneration 2022'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Handle and respond to discrepancies and queries relating to payroll'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '8',
            'name' => 'Handle and respond to HR team related to Attendance, BPJS, Insurance'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '8',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        //Department 9
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Maintain Database in Excel for public sharing weekly'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Maintain weekly complete database for internal HRGA'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Analyze pivot weekly (per dept, per age, per join date, per location, per gender)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update database in Talenta (new hire, exit, transfer)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update Total Headcount'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update report for pak Tito or Pak Vinesh'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and indentify daily task all department'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create HRGA Daily task delegation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'plan and prepare for man hours system'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review guidance for staff related system'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Socialize for staff related system'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update Job description for all staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and review KPI'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and review training plan'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and review talenta database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Transfer knowledge related Attendance system to HRGA'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with external parties related HRGA System'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for staff related system'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coaching indirect and direct to HRGA staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Consulting with staffs and give advice'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Export Time attendance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review time attendance, extra hours and additional leave'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Approve time attendance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review warning letter for late in and early checkout'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Analysis and update extra hours allowance nominal'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Analysis and summarize KPI result'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update info and review monthly payroll'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review monthly extra hours allowance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update info on registration and deactivation both BPJS'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update info on Lippo registration and deactivation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Calculate KPI Bonus (if any)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and review THR calculatione'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Iniate and update Compensation calculation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create letter of compensation calculation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update compesation payment calendar'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Assist and review comben Memo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create HR Memo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and review GA Memo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update peraturan perusahaan'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Socialize E&C, PP, HRGA Memo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Iniate job evaluation and review'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update salary survey or benchmarking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Approve and review PR for HR Expense'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Approve and review travel cost and other cost'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update transaction BCA'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review and reconcile GER'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create HRGA Budget and distribute'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create draft concept of HRGA program'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and prepare HRGA program'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review and implement HRGA program'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Recap latest antigen / PCR Database for Business Travel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for Antigen / PCR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review and approve HRGA budget'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Lead event opening of HRGA Program'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Collect and compile training plan'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with vendors for soft skill training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review and revise all HRGA Documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and prepare for document related dispute'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with relevant parties for dispute process'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Input new hire data into Talenta'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Remove resign staffs (if any) from Talenta'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update Position and Status Change (Transfer) in Talenta'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update End Date for all staffs in Talenta'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Get 3 Potential Candidates for one position monthly'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Phonescreen for Talent Pool'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update talent pool database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Analyze by pivot availability of potential candidate'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send report and result of talent pool'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update Job description for all staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update recruitment progress 2021 in excel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send recruitment progress'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Complete SRF and ask approval'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Post Job Ads in Jobstreet, linkedin, other platform'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Shortlist CV for recruitment needs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Phonescreen prior to interview'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send interview invitation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Interview potential Candidate'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Conduct DISC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update DISC file for all staff including new hire'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Scoring and Analyze DISC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Conduct reference check'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Assist technical test'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Recap summary of interview result'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Collect and compile applicant form and interview form'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Scan New Hire Documents for SRF Approval'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Conduct verbal offering'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send email offering to selected candidate'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create New Hire Contract and ask signature'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Scan and compile new hire documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Providing access card to new hire'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Register finger print'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Request and purchase ID Card'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Request and process PPE'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Renew and select PPE tools'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Rekap PPE Database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send MCU invitation to new hire'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Process MCU payment invoice and create PR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send MCU Result to staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Rekap MCU database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Provide Merchandise to new hire'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Provide new hire staffs contract'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Introduce new hire to all staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send PP, E&C, HRGA Memo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Explain attendance system to new hire'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Announce new hire in WA Group'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send handover documents and exit interview to staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create acceptance of resignation and ask signature'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create verification of Employment and ask signature'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create notification letter and ask aspproval'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Check remaining leave and ask confirmation to resignee'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange and conduct Exit Interview'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Followup clear handover from staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange KPI / Technical competence session'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Follow up the progress of KPI / technical Session to head of dept'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'create Gform for survey and evaluation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Export Time attendance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Check empty attendance and make it annual leave'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Maintain and recap staff annual leave (weekly)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Analyze late attendance monthly and record in excel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Prepare warning letter for late attendance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and check extra hours for inlieu and allowance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send extra hours allowance per dept'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Kitas, SKTT, STN, LKK, Domisili'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Process Kitas: RPTKA, Notifikasi Inta, Inta, Vitas'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '9',
        //     'name' => 'Process Visa: Sponsor Letter (Company), FU Chamber of Country, Request Itineary, Accomodaion & Transportation'
        // ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange Repatriation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Prepare Travel Cost and Seek approval overseas travel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Maintain list attendance of training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update New Hire Induction training database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update Onboarding training database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update Ethic and Compliance training database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and register lucidity training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Invite training to participant and trainer'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange E&C Refresher'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange training for DDC'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Purchase snack and gift for training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR/PV/GER for training related needs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Follow up trainer availability for any training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate and monitor training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send report of E&C Refresher'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send New Hire Induction Training Invitation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with trainer on induction training schedule'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Prepare for Onboarding Training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Prepare for New Hire Induction training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Follow up to vendor for any training'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Socialize and get signature of all staffs in E&C'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => "Recap e&c and reminder staffs who haven't submit yet"
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Iniate ISO Program and coordinate with vendors'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and prepare documents for ISO (140001, 450001, 90001)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for ISO'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with all head of dept for ISO implementation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create checklist progress for ISO'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Review, Revise and Maintain (Main/Branch) Office Contract'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Prepare payment for monthly electricity and internet'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '9',
        //     'name' => 'Maintain Database for (Main/Branch) Office Expenses (incl office rental, electricity, phone bill, biznet, maintenance, etc)'
        // ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for Office rental and other office expenses'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Find new potential office (If any)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Make comparison report for new office'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Follow-up Office Fit-out, Fixture / Furnitures (if any)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create poster or sign and put in proper place'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update seating arrangement and office layout'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Routine Maintenance for Office utilities'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Purchase sundries, Amenities, Pantries Supply'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Purchase printing, papers and stationaries'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Meeting and coordination with pic building and working space'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with OB for office cleanliness and neatness'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange Disinfectant time and send announcement to all staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update job description for office boy'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange schedule for meeting room'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Monitor the room or meeting room in case overloaded'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR and GER for Office & Maintenance'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create, update and maintain Corporate Documents checklist'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Follow up and review corporate documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR/PV/GER for legal documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Purchasing Assets/ Equipment (CAPEX) and Routine Local Purchasing (OPEX)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Follow up and coordination SKA /IPTB'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create GER SKA/IPTB or other certificate'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Maintain asset database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR/PV/GER for Capex (IT)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update asset registration'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send excel data of actual travel expense (hotel, meal allowance, flight) per dept to HRGA Lead'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange flight/train/bus booking for business travel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange hotel booking for business travel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange rental car for business travel'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '9',
        //     'name' => 'To provide costing for individual traveling or business trip, relocation, crew change, and transfer personnel from site to site'
        // ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '9',
        //     'name' => 'Make report actual expense of hotel and create in bar graph annually (can coordinate with Pak Andri) per dept'
        // ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '9',
        //     'name' => 'Make report actual expense of Flight and create in bar graph annually (can coordinate with Pak Andri) per dept'
        // ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Make report actual expense of meal allowance and create in bar graph annually per dept'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Make report actual expense of fuel and create in bar graph annually'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with driver and user for shuttle car Schedule'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update and maintain contract of all cars'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update shuttle route'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Find new car based on health protocol'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Find and recruit new drivers'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Find rental vendor for jabodetabek area (for alternative)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send performnace review of driver and OB'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Hand over and exit interview for driver and OB'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Rekap JMP into excel car schedule'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send shuttle car schedule to driver and user'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update staff mobilization Memo'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Collect and remind travel request'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Rekap travel request in excel sheet'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send meal allowance info to user'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for voucher Taxi'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Purchase voucher taxi'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Make report voucher taxi every month'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Compile all vouchers evident and put in one folder'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and prepare for Friday Refreshment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create GER for Friday Refreshment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create budget (PR & GER) for merchandise'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and Prepare for annual end year dinner'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and Prepare for social activity (CSR)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and Prepare for Outing'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Conduct Friday Refreshment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Annual end year dinner execution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Staff farewell execution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Social activity Execution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Outing Execution'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Monthly refreshment execution (if any)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create rundown and activity for HR Program (outing, dinner, bonding)'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send New Hire Announcement'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Send birthday greetings in WA Groups'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create and send public holiday poster'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate and prepare for unexpected event'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Maintain IG & Linkedin for employer branding'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update roster for all staffs in excel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Update Covid-19 tracker'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange Antigen / PCR for tracking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Arrange Antigen / PCR for Business Travel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Recap latest antigen / PCR Database for tracking'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Recap latest antigen / PCR Database for Business Travel'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for Antigen / PCR'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Collect and review reimbursement Document'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Record all reimbursement prior to 15th'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Scan reimbursement documents'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for HR Expense'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create PR for other dept Expense'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Process purchase HR Expense'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Process purchase other dept Expense'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create GER HR Expense and collect evidence'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create GER for other dept Expense and collect evidence'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Recap educational background for job evaluation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Recap total work experience for job evaluation'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Coordinate with campus for Job posting'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'conduct PAPI assessment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'conduct BARS Assessment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Export BARS result of evalutaion'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Scoring and recap PAPI Assessment'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Rekap DISC Database'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Assist for training administration'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'English Training for Aldi & Indra'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Training softskill for targeted staffs'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Plan and prepare HR Activities'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Create poster for employer branding'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '9',
            'name' => 'Maintain IG and Linkedin for employer Branding'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '9',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        // Department 10
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Manage Cost and contract related'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Manage procurement related'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Manage HRGA related'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Manage Finance related'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '10',
            'name' => 'Manage business development related'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '10',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);

        // Department 11
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Manage project delivery related'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Manage hsse related'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Manage fuel system  related'
        ]);
        DB::table('daily_tasks')->insert([
            'project_id' => '1',
            'department_id' => '11',
            'name' => 'Manage Project Control related'
        ]);
        // DB::table('daily_tasks')->insert([
        //     'project_id' => '1',
        //     'department_id' => '11',
        //     'name' => 'Other, Please mention in Task Remark'
        // ]);
    }
}
