<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeOfWorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_of_works')->insert([
            'name' => 'Office'
        ]);
        DB::table('type_of_works')->insert([
            'name' => 'Home'
        ]);
        DB::table('type_of_works')->insert([
            'name' => 'Site'
        ]);
        DB::table('type_of_works')->insert([
            'name' => "Business Travel"
        ]);
    }
}
