<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            'name' => 'Shell Retail'
        ]);
        DB::table('projects')->insert([
            'name' => 'Shell Real Estate'
        ]);
        DB::table('projects')->insert([
            'name' => 'Ahabe (Tuban Port)'
        ]);
        DB::table('projects')->insert([
            'name' => 'AGB (Cargill)'
        ]);
    }
}
