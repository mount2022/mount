<?php

namespace Database\Seeders;

use App\Models\DailyTask;
use App\Models\Department;
use App\Models\Project;
use App\Models\Site;
use App\Models\Task;
use App\Models\Transportation;
use App\Models\TypeOfWork;
use App\Models\User;
use App\Models\Training;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
        $this->call([
            DailyTaskSeeder::class,
            DepartmentSeeder::class,
            ProjectSeeder::class,
            SiteSeeder::class,
            TransportationSeeder::class,
            TypeOfWorkSeeder::class,
            AbsenceSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            TrainingSeeder::class,
            KPISeeder::class
        ]);
    }
}
