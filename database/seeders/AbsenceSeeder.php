<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AbsenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('absences')->insert([
            'name' => 'Tanggal Merah'
        ]);
        DB::table('absences')->insert([
            'name' => 'Cuti'
        ]);
        DB::table('absences')->insert([
            'name' => 'Sakit'
        ]);
        DB::table('absences')->insert([
            'name' => 'Other, Please mention in Absence Remark'
        ]);
    }
}
