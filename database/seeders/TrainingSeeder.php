<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('default_trainings')->insert([
            'user_id' => '0',
            'training_type' => 'No Data',
            'when' => 'No Data',
            'objective' => 'No Data',
            'participant' => 'No Data',
            'requirement' => 'No Data',
            'note' => 'DEFAULT'
        ]);

        // DB::table('default_trainings')->insert([
        //     'user_id' => '0',
        //     'training_type' => 'TRAIING NEED ANALYSIS (HSSE)',
        //     'when' => '2022-2033',
        //     'objective' => '',
        //     'participant' => '',
        //     'requirement' => '',
        //     'note' => 'Title'
        // ]);
    }
}
