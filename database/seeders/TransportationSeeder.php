<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransportationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transportations')->insert([
            'name' => 'Office Shuttle'
        ]);
        DB::table('transportations')->insert([
            'name' => 'Taxi'
        ]);
        DB::table('transportations')->insert([
            'name' => 'Airplane'
        ]);
        DB::table('transportations')->insert([
            'name' => 'Bus/Travel'
        ]);
        DB::table('transportations')->insert([
            'name' => 'Train'
        ]);
        DB::table('transportations')->insert([
            'name' => 'Private Vehicle'
        ]);
        DB::table('transportations')->insert([
            'name' => 'Online Transport'
		]);
		DB::table('transportations')->insert([
            'name' => 'No Transportation'
        ]);
    }
}
