<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name' => 'Design'
        ]);
        DB::table('departments')->insert([
            'name' => 'Fuel System'
        ]);
        DB::table('departments')->insert([
            'name' => 'Project Delivery'
        ]);
        DB::table('departments')->insert([
            'name' => 'HSSE'
        ]);
        DB::table('departments')->insert([
            'name' => 'Project Control'
        ]);
        DB::table('departments')->insert([
            'name' => 'Cost & Contract'
        ]);
        DB::table('departments')->insert([
            'name' => 'Procurement'
        ]);
        DB::table('departments')->insert([
            'name' => 'Finance'
        ]);
        DB::table('departments')->insert([
            'name' => 'HR&GA'
        ]);
        DB::table('departments')->insert([
            'name' => 'CPM'
        ]);
        DB::table('departments')->insert([
            'name' => 'Deputy CPM'
        ]); 
    }
}
