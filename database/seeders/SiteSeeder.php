<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL GARDEN CITY, EAST JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL MERR RUNGKUT-1 SURABAYA"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL RAYA PARUNG-2 CINANGKA"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL HASYIM ASHARI-1 TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL SALEMBA-1 CENTRAL JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL VETERAN-1 GRESIK"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL BINTARO U-TOWN SOUTH TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL CIPENDAWA BR-1, BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL PETUKANGAN"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL MERUYUNG, DEPOK"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL DUPAK-1, SURABAYA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 CO NTI'S - SHELL GUNUNG PUTRI-1 BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL HUSEIN SASTRA, TANGERANG CITY"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL PONDOK GEDE"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL WIBAWA MUKTI-1, BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL SILIWANGI, DEPOK"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL SETIA BUDI-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SISINGAMANGARAJA-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL WAHIDIN-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - SHELL CIKARANG-1 (LEMAHABANG), BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CO NTI'S 2021 - MEINHARDT EPCM FEES"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR AI'S 2021 - FS INTEGRITY SOEPOMO"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR AI'S 2021 - PIK"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR AI'S 2021 - SUPRAPTO"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR AI'S 2021 - RECONSTRUCTION OF RETAINING WALL IMPROVEMENT OF LATUMENTENG-2"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2020 AI'S - BSD3 RETAINING WALL"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR VA'S-NFR 2021 - BUNDLING LUBEBAY SHOC+ BRANDING 2021 (7 8 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR VA'S-NFR 2021 - FRANCHISE FITTING & VA COLOCATOR JAGORAWI"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR VA'EVC 2021 - PILOT EV CHARGER LAUNCHING 2021 (1 2 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CM'S 2021 - DISPENSER & SHEAR VALVE UNIT REPLACEMENT 2021 (13 12 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CM'S 2021 - RBSAM COMPLIANCE UPGRADE 2021 (8 9 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CM'S 2021 - CANOPY AND BUILDING ROOF REPLACEMENT 2021 (4 15 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR CM'S 2021 - GROUNDING AND ELECTRICAL IMPROVEMENT 2021 (6 11 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "PIPELINE VA'S-NFR 2021 - BUNDLING LUBEBAY UPGRADE 2021 (2 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "PIPELINE VA'S-NFR 2021 - BUNDLING LUBEBAY UPGRADE 2019 (6 10 SITES)"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL BASUKI RACHMAT-1 JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL SERANG-BALARAJA, TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL SOLEH ISKANDAR-2, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL RAYA PANTURA-1, BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL CIKANDE-1, SERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL RAYA PERJUANGAN-1, BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL CEGER RAYA-1 SOUTH TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL CIBARUSAH-1, BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 TO 2022 CO NTI'S - SHELL IMAM BONJOL, BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL DELTA MAS BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL SISINGAMANGARAJA-2, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL YOS SUDARSO-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - CO NTI SHELL ADAM MALIK-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL PANCING-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL GAJAH MADA-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL BRIGJEN KATAMSO-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL KAHFI-1 SOUTH JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL CADAS KUKUN-1, TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL YASMIN-1 BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL RAYA SERANG, TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL TENTARA PELAJAR-1 SOUTH JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL PAJAJARAN-2 BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL SUPRAPTO-2 CENTRAL JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL AERO8-1 JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL FATMAWATI-2"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL MANGONSIDI-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL SEPATAN-1, TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL M. SURYA DHARMA-1 TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL LETDA NASIR-1, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL PALMERAH BARAT, WEST JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL LENTENG AGUNG-1 SOUTH JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL PERINTIS KEMERDEKAAN-1 EAST JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL A. YANI KWG, KARAWANG"
        ]);
        DB::table('sites')->insert([
            'name' => "FULL BLOWN 2021 - SHELL PERMATA HIJAU, SOUTH JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL DRAMAGA-1, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL PAHLAWAN, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL DR. SEMERU-1, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL MINANGKABAU, SOUTH JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL ANTAPANI-1 BANDUNG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL TEGAR BERIMAN-1, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL ASIA AFRIKA -1, BANDUNG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL MABES HANKAM-1, EAST JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL KEBON KAWUNG-1, BANDUNG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL RAYA MUCHTAR, DEPOK"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL BINONG-1, TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL RAYA CISAUK-1, TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI UJUNG MENTENG-1 JKT"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI SAWANGAN, DEPOK"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI CILEUNGSI"
        ]);
        DB::table('sites')->insert([
            'name' => "JGC"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI MARELAN"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI NGURAH RAI"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI MARSEKAL SURYA DARMA"
        ]);
        DB::table('sites')->insert([
            'name' => "CO NTI SHELL PEMUDA CENTRAL-1 SBY"
        ]);
        DB::table('sites')->insert([
            'name' => "INRIT SAWANGAN & PARUNG 2"
        ]);
        DB::table('sites')->insert([
            'name' => "PKDR SOEPOMO"
        ]);
        DB::table('sites')->insert([
            'name' => "INRIT KENJERAN"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL KEBAYORAN BARU-1, SOUTH JAKARTA"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL BOJONG GEDE, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL DAAN MOGOT-3, TANGERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - CO NTI SHELL CIAWI, BOGOR"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL CEMARA-1, MEDAN"
        ]);
        DB::table('sites')->insert([
            'name' => "CARRY OVER 2021 TO 2022 CO NTI'S - SHELL VIDA-1, BEKASI"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR VA'S-NFR 2021 - FRANCHISE FITTING & VA COLOCATOR RAYA HANKAM (BK)"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR VA'S-NFR 2021 - FRANCHISE FITTING & VA COLOCATOR BINTARO U-TOWN (BK)"
        ]);
        DB::table('sites')->insert([
            'name' => "TnR VA'S-NFR 2021 - FRANCHISE FITTING & VA COLOCATOR GRAHA RAYA (Kopi Tuku)"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI MAYOR OKING"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI CIASUK"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI WR SUPRATMAN"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI Shell Diponegoro Tambun"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI Shell Cut Mutia-2 Bekasi"
        ]);
        DB::table('sites')->insert([
            'name' => "KS Tubun-1, Jkt"
        ]);
        DB::table('sites')->insert([
            'name' => "SHELL NTI CIKUPA SERANG"
        ]);
        DB::table('sites')->insert([
            'name' => "SHELL KERTAJAYA"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI SOEPRAPTO 2"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI KENJERAN"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI KEMANG RAYA"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI GUNUNG SAHARI"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI Trituran Medan site"
        ]);
        DB::table('sites')->insert([
            'name' => "NTI Amir Machmud"
        ]);
        // DB::table('sites')->insert([
        //     'name' => "Other, Please mention in Site Remark"
        // ]);
    }
}
