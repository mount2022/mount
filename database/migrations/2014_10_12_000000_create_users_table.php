<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('department_id');
            $table->foreignId('role_id');
            $table->string('employee_id');
            $table->string('name');
            $table->string('phone');
            $table->date('date_of_birth');
            $table->string('place_of_birth');
            $table->text('address');
            $table->string('gender');
            $table->string('position');
            $table->string('job_level');
            $table->string('approval_line');
            $table->string('employment_status');
            $table->date('join_date');
            $table->date('end_date')->nullable();;
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('picture')->default('default.jpg');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
