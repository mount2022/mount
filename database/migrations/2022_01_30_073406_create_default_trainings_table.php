<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefaultTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_trainings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->text('training_type')->nullable();
            $table->text('when')->nullable();
            $table->text('objective')->nullable();
            $table->text('participant')->nullable();
            $table->text('requirement')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_trainings');
    }
}
