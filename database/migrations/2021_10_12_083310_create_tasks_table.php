<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('project_id')->nullable();
            $table->foreignId('daily_task_id')->nullable();
            $table->foreignId('type_of_work_id')->nullable();
            $table->foreignId('transportation_id')->nullable();
            $table->foreignId('site_id')->nullable();
            $table->foreignId('absence_id')->nullable();
            $table->string('name')->nullable();
            $table->string('project_remark')->nullable();
            $table->time('start_time')->nullable();
            $table->time('finish_time')->nullable();
            $table->time('duration')->nullable();
            $table->text('daily_task_remark')->nullable();
            $table->text('site_remark')->nullable();
            $table->text('note')->nullable();
            $table->text('absence_remark')->nullable();
            $table->text('absence_description')->nullable();
            $table->string('task_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
